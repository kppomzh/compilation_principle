package pubdir.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * 记录产生式
 */
public class Cell {
    public static Cell epsilon=new Cell("ε");
    private final List<Cell> cells;
    private final String cellName;
    private String[] attributes;

    public Cell(String cellName){
        this.cellName=cellName;
        cells=new ArrayList<>();
    }

    public void setAttribute(String[] attributes){
        this.attributes = attributes;
    }

    public String getFirstMark() {
        if(length()!=0) {
            Cell tCell = cells.get(0);
            return tCell.getFirstMark();
        }
        else
            return getCellName();
    }


    public void addCell(Cell c){
        cells.add(c);
    }

    public List<Cell> getCell() {
        return cells;
    }


    public String getCellName() {
        return cellName;
    }

    public int length() {
        return cells.size();
    }

    public String[] getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        if(cells.isEmpty())
            return cellName;
        else {
            StringBuilder sb = new StringBuilder();
            cells.forEach(sb::append);
            return sb.toString();
        }
    }
}
