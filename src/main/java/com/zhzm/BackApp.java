package com.zhzm;

import com.zhzm.Exception.BuildException;
import com.zhzm.Exception.SemanticException;
import com.zhzm.analyzer.Parser;
import com.zhzm.visitormode.TreeVisitor;
import oth.Util.FiniteAutoSegment;
import oth.Util.PrintGrammarObject;
import oth.Util.SerialUtil;
import pubdir.structure.GrammarTable;
import pubdir.structure.Node;
import pubdir.structure.Word;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.Map;

public class BackApp {
    static Map<String, String> map = Map.of(
            "Caculator.grammar", "(3-5+7*12)/-2*1.3",
            "assign.grammar", "function int abc { return def;  }"
    );

    public static void main(String[] args) throws BuildException, IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, InstantiationException, SemanticException {
        String grammarFileName = "Caculator.grammar";
        GrammarTable table = (GrammarTable) new SerialUtil().readSerObj(grammarFileName);
//        PrintGrammarObject.printGrammarTable(table);
        // 词法分析
        String expression = map.get(grammarFileName);
        // String expression = "(3-5+7*12)/-2";
        // String expression = "(3-5+7*12)/-2+sin(2)";
        FiniteAutoSegment segment = new FiniteAutoSegment();
        LinkedList<Word> words = segment.stateMachine(expression);
        // 进行语法检查
        Parser parser = new Parser(table);
        parser.setDebug(true);
        Node<Word> root = parser.start(words);
        PrintGrammarObject.printTree(root);

        System.out.println(TreeVisitor.startVisit(root));

//        TreeBuilder builder = new TreeBuilder(table, "com.zhzm.grammarclass");
//        ParserTreeNode treeRoot = builder.start(words);
//        System.out.println(treeRoot.getSubStance(null));
    }
}
