package com.zhzm.interpretermode;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/7 20:35
 */
public class ExpressionTest {
    public static void main(String[] ar) {


        Expression<Number, Number> exp = new NumberUnaryOperator().of("sqrt", 100);
        System.out.println(exp.interpret());
        exp = new NumberUnaryOperator().of("sin", 3.14d);
        System.out.println(exp.interpret());
        exp = new NumberBinaryOperator().of(10, "/", 3.14d);
        System.out.println(exp.interpret());
        Expression<Boolean, Object> boi = new Compare().of(10, 3.14d);
        System.out.println(boi.interpret());
        boi = new Compare().of("abd", "abd");
        System.out.println(boi.interpret());
        Expression<Number, String> nsi = new ToNumber().append("1").append(".").append("4").append("1").append("4");
        System.out.println(nsi.interpret());
    }
}
