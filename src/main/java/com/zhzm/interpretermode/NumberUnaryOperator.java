package com.zhzm.interpretermode;

import com.zhzm.languagefunctions.MathUnaryFunction;

public class NumberUnaryOperator implements Expression<Number, Number> {
    private MathUnaryFunction mu;

    private Number num;

    @Override
    public Number interpret() {
        return mu.calculate(num);
    }

    @Override
    public NumberUnaryOperator of(Object... objs) {
        this.num = (Number) objs[1];
        this.mu = MathUnaryFunction.valueOf((String) objs[0]);
        return this;
    }
}
