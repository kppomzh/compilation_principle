package com.zhzm.interpretermode;

public interface Expression<T, K> {
    T interpret();

    default Expression<T, K> of(Object... objs) {
        return this;
    }
    default Expression<T, K> append(K k) {
        return this;
    }
}
