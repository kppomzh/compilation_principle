package com.zhzm.interpretermode;

import java.util.Objects;

public class Compare implements Expression<Boolean, Object> {

    private Object b1;

    private Object b2;

    public Compare of(Object... objs) {
        this.b1 = objs[0];
        this.b2 = objs[1];
        return this;
    }


    @Override
    public Boolean interpret() {
        return Objects.equals(b1,b2);
    }
}
