package com.zhzm.interpretermode;

import java.util.ArrayList;
import java.util.List;

public class ToNumber implements Expression<Number, String> {

    @Override
    public ToNumber append(String str) {
        this.contextArr.add(str);
        return this;
    }

    List<String> contextArr = new ArrayList<>();

    @Override
    public Number interpret() {
        StringBuilder sb = new StringBuilder();
        boolean hasPoint = false;
        for(String str : contextArr) {
            sb.append(str);
            hasPoint = hasPoint || str.contains(".");
        }
        return hasPoint ? Double.parseDouble(sb.toString()) : Integer.parseInt(sb.toString());
    }


}
