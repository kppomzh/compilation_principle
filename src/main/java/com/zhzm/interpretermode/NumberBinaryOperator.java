package com.zhzm.interpretermode;

import com.zhzm.languagefunctions.MathExtion;

public class NumberBinaryOperator implements Expression<Number, Number> {
    private MathExtion me;

    private Number left, right;

    @Override
    public Number interpret() {
        return me.calculate(left, right);
    }

    @Override
    public NumberBinaryOperator of(Object... objs) {
        this.me = MathExtion.getExtion((String) objs[1]);
        this.right = (Number) objs[2];
        this.left = (Number) objs[0];
        return this;
    }
}
