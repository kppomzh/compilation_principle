package com.zhzm.visitormode;

import com.zhzm.Exception.GrammarException;
import com.zhzm.interpretermode.Expression;
import com.zhzm.interpretermode.NumberBinaryOperator;
import com.zhzm.interpretermode.ToNumber;
import pubdir.structure.Node;
import pubdir.structure.Word;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public abstract class TreeVisitor<V> {

    public static String startVisit(Node<Word> treeNode) {
        return new SVisitor().visit(treeNode);
    }

    protected abstract V visit(Node<Word> treeNode);

    static class SVisitor extends TreeVisitor<String> {
        @Override
        public String visit(Node<Word> treeNode) {
            StringBuilder sb = new StringBuilder();
            treeNode.getChild().forEach(item -> sb.append(new EVisitor().visit(item)));
            return sb.toString();
        }
    }

    static class EVisitor extends TreeVisitor<Number> {

        @Override
        public Number visit(Node<Word> treeNode) {
            Expression<Number, Number> e = new NumberBinaryOperator();
            Deque<Object> queue = new ArrayDeque<>();
            for (Node<Word> item : treeNode.getChild()) {
                switch (item.getContent().getSubstance()) {
                    case "t":
                        queue.add(new TVisitor().visit(item));
                        break;
                    case "ep":
                        queue.addAll(new EPVisitor().visit(item));
                        break;
                    default:
                        throw new GrammarException(item.getContent());
                }
            }
            while(queue.size()>1) {
                e.of(queue.poll(), queue.poll(), queue.poll());
                queue.addFirst(e.interpret());
            }
            return (Number) queue.poll();
        }
    }

    static class EPVisitor extends TreeVisitor<List<?>> {
        @Override
        public List<?> visit(Node<Word> treeNode) {
            List<Object> list = new ArrayList<>();
            treeNode.getChild().forEach(item -> {
                if(!item.getContent().getName().equals("nonterminal")) {
                    list.add(item.getContent().getSubstance());
                    return;
                }
                switch (item.getContent().getSubstance()) {
                    case "t":
                        list.add(new TVisitor().visit(item));
                        break;
                    case "ep":
                        list.addAll(new EPVisitor().visit(item));
                        break;
                    default:
                        throw new GrammarException(item.getContent());
                }
            });
            return list;
        }
    }


    static class TVisitor extends TreeVisitor<Number> {
        @Override
        public Number visit(Node<Word> treeNode) {
            Expression<Number, Number> e = new NumberBinaryOperator();
            Deque<Object> queue = new ArrayDeque<>();
            for (Node<Word> item : treeNode.getChild()) {
                switch (item.getContent().getSubstance()) {
                    case "f":
                        queue.add(new FVisitor().visit(item));
                        break;
                    case "tp":
                        queue.addAll(new TPVisitor().visit(item)) ;
                        break;
                    default:
                        throw new GrammarException(item.getContent());
                }
            }
            while(queue.size()>1) {
                e.of(queue.poll(), queue.poll(), queue.poll());
                queue.addFirst(e.interpret());
            }
            return (Number) queue.poll();
        }
    }

    static class TPVisitor extends TreeVisitor<List<?>> {

        @Override
        protected List<?> visit(Node<Word> treeNode) {
            List<Object> list = new ArrayList<>();
            treeNode.getChild().forEach(item -> {
                if(!item.getContent().getName().equals("nonterminal")) {
                    list.add(item.getContent().getSubstance());
                    return;
                }
                switch (item.getContent().getSubstance()) {
                    case "f":
                        list.add(new FVisitor().visit(item));
                        break;
                    case "tp":
                        list.addAll(new TPVisitor().visit(item));
                        break;
                    default:
                        throw new GrammarException(item.getContent());
                }
            });
            return list;
        }
    }

    static class FVisitor extends TreeVisitor<Number> {

        @Override
        protected Number visit(Node<Word> treeNode) {
            Number num = null;
            for (Node<Word> item : treeNode.getChild()) {
                switch (item.getContent().getSubstance()) {
                    case "number":
                        num = new NumberVisitor().visit(item);
                        break;
                    case "e":
                        num = new EVisitor().visit(item);
                        break;
                    case "(":
                    case ")":
                        continue;
                    default:
                        throw new GrammarException(item.getContent());
                }
            }
            return num;
        }
    }

    static class NumberVisitor extends TreeVisitor<Number> {

        @Override
        protected Number visit(Node<Word> treeNode) {
            Expression<Number, String> toNumber = new ToNumber();
            treeNode.getChild().forEach(item -> {
                switch (item.getContent().getSubstance()) {
                    case "intpart":
                        toNumber.append(new IntPartVisitor().visit(item));
                        break;
                    case "doublepart":
                        toNumber.append(new DoublePartVisitor().visit(item));
                        break;
                    default:
                        throw new GrammarException(item.getContent());
                }
            });
            return toNumber.interpret();
        }
    }

    static class IntPartVisitor extends TreeVisitor<String> {

        @Override
        protected String visit(Node<Word> treeNode) {
            StringBuilder sb = new StringBuilder();
            treeNode.getChild().forEach(item -> sb.append(item.getContent().getSubstance()));
            return sb.toString();
        }
    }

    static class DoublePartVisitor extends TreeVisitor<String> {

        @Override
        protected String visit(Node<Word> treeNode) {
            StringBuilder sb = new StringBuilder();
            treeNode.getChild().forEach(item -> sb.append(item.getContent().getSubstance()));
            return sb.toString();
        }
    }
}
