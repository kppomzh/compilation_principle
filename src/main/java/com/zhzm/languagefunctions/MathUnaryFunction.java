package com.zhzm.languagefunctions;

public enum MathUnaryFunction implements Cloneable {
    sin("sin") {
        @Override
        public Number calculate(Number var1) {
            return Math.sin(var1.doubleValue());
        }
    },
    cos("cos") {
        @Override
        public Number calculate(Number var1) {
            return Math.cos(var1.doubleValue());
        }
    },
    tan("tan") {
        @Override
        public Number calculate(Number var1) {
            return Math.tan(var1.doubleValue());
        }
    },
    abs("abs") {
        @Override
        public Number calculate(Number var1) {
            return Math.abs(var1.doubleValue());
        }
    },
    sqrt("sqrt") {
        @Override
        public Number calculate(Number var1) {
            return Math.sqrt(var1.doubleValue());
        }
    };

    private final String symbol;
    private MathUnaryFunction(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }

    public abstract Number calculate(Number var1);
}
