package com.zhzm.languagefunctions;

/**
 * 重载运算符的方法
 * 由于运算符的数量是有限的，重载时对应的意义是明确的，因此适合用枚举来实现这个操作
 * 此处仅仅用枚举方式举例，其他实现方式也是有的
 */
public enum MathExtion {
    plus("+") {
        public Number calculate(Number var1, Number var2) {
            if (var1.getClass().getSimpleName().equals(Integer.class.getSimpleName()) && var2.getClass().getSimpleName().equals(Integer.class.getSimpleName())) {
                return var1.intValue() + var2.intValue();
            } else {
                return var1.doubleValue() + var2.doubleValue();
            }
        }
    },
    minus("-") {
        public Number calculate(Number var1, Number var2) {
            if (var1.getClass().getSimpleName().equals(Integer.class.getSimpleName()) && var2.getClass().getSimpleName().equals(Integer.class.getSimpleName())) {
                return var1.intValue() - var2.intValue();
            } else {
                return var1.doubleValue() - var2.doubleValue();
            }
        }
    },
    multi("*") {
        public Number calculate(Number var1, Number var2) {
            if (var1.getClass().getSimpleName().equals(Integer.class.getSimpleName()) && var2.getClass().getSimpleName().equals(Integer.class.getSimpleName())) {
                return var1.intValue() * var2.intValue();
            } else {
                return var1.doubleValue() * var2.doubleValue();
            }
        }
    },
    div("/") {
        public Number calculate(Number var1, Number var2) {
            if (var1.getClass().getSimpleName().equals(Integer.class.getSimpleName()) && var2.getClass().getSimpleName().equals(Integer.class.getSimpleName())) {
                return var1.intValue() / var2.intValue();
            } else {
                return var1.doubleValue() / var2.doubleValue();
            }
        }
    };

    private final String symbol;

    private MathExtion(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }

    public abstract Number calculate(Number var1, Number var2);

    public static MathExtion getExtion(String mark) {
        for(MathExtion me : MathExtion.values()) {
            if(me.toString().equals(mark)) {
                return me;
            }
        }
        return null;
    }
}
