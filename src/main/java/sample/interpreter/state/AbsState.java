package sample.interpreter.state;

import org.apache.beam.sdk.values.KV;
import sample.interpreter.facility.AbsFacility;

public interface AbsState {

    void open(AbsFacility absFacility);

    void awake(AbsFacility absFacility);

    void doAction(AbsFacility absFacility, KV<String, String> action);

    default void close(AbsFacility absFacility) {
        absFacility.writeInfoLog( "{} 已关闭", absFacility.getFurnitureName());
        absFacility.init();
    }
}
