package sample.interpreter.state;

import org.apache.beam.sdk.values.KV;
import sample.interpreter.facility.AbsFacility;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/20 18:43
 */
public class CloseState implements AbsState {

    @Override
    public String toString() {
        return " 已关闭";
    }

    @Override
    public void open(AbsFacility absFacility) {
        absFacility.setState(new OpenState());
        absFacility.writeInfoLog("{} 已启动", absFacility.getFurnitureName());
    }

    @Override
    public void awake(AbsFacility absFacility) {
        absFacility.writeErrorLog( "{} 已关闭，无法唤醒", absFacility.getFurnitureName());
    }

    @Override
    public void doAction(AbsFacility absFacility, KV<String, String> action) {
        absFacility.writeErrorLog( "{} 已关闭，无法操作", absFacility.getFurnitureName());
    }
}
