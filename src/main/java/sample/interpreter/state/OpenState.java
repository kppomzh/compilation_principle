package sample.interpreter.state;

import org.apache.beam.sdk.values.KV;
import sample.interpreter.facility.AbsFacility;

public class OpenState implements AbsState {
    @Override
    public String toString() {
        return " 已启动";
    }

    @Override
    public void open(AbsFacility absFacility) {

    }

    @Override
    public void awake(AbsFacility absFacility) {
        absFacility.setState(new AwakeState());
        absFacility.writeInfoLog("{} 已唤醒，可以设置工作模式", absFacility.getFurnitureName());
    }

    @Override
    public void doAction(AbsFacility absFacility, KV<String, String> action) {
        awake(absFacility);
        // 自动唤醒
        absFacility.doAction(action);
    }
}
