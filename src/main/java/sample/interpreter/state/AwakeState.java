package sample.interpreter.state;

import org.apache.beam.sdk.values.KV;
import sample.interpreter.facility.AbsFacility;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/20 18:44
 */
public class AwakeState implements AbsState {
    private String action;
    private String state;

    @Override
    public String toString() {
        return action + " 为 " + state;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public void open(AbsFacility absFacility) {

    }

    @Override
    public void awake(AbsFacility absFacility) {

    }

    @Override
    public void doAction(AbsFacility absFacility, KV<String, String> act2opt) {
        String actValue = absFacility.getLegitActValue(act2opt.getKey());
        if(null == actValue) {
            absFacility.writeErrorLog("{} 设备不支持该操作", absFacility.getFurnitureName());
        }
        else {
            absFacility.writeInfoLog("{} 设置 {} 为 {}{}", absFacility.getFurnitureName(),
                    act2opt.getKey(), act2opt.getValue(), actValue);
            action = act2opt.getKey();
            state = act2opt.getValue();
        }
    }
}
