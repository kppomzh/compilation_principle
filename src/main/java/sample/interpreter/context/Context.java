package sample.interpreter.context;

import sample.interpreter.config.RegFacilityConf;
import sample.interpreter.expression.AllCommandExpression;
import sample.interpreter.facility.AbsFacility;

public class Context {
    private AllCommandExpression commandStart;

    // 初始化
    public Context(AbsFacility... states) {
        RegFacilityConf.getInstence().register(states);
        commandStart = new AllCommandExpression();
    }


    public void run(String s) {
        commandStart.setAllCommands(s).interpreter();
    }
}
