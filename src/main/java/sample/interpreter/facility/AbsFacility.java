package sample.interpreter.facility;

import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sample.interpreter.state.AbsState;
import sample.interpreter.state.CloseState;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/18 23:12
 */
public abstract class AbsFacility {
    private final AbsState closeState;
    private final Logger log;
    private final String furnitureType;
    private String furnitureName;
    // 当前设备状态对象
    private AbsState state;

    // 设备合法操作登记表
    protected Map<String, String> legitAct;
    // 当前设备状态
    protected Map<String, String> stateMap;

    protected AbsFacility(String furnitureType, Class<? extends AbsFacility> clazz) {
        legitAct = new HashMap<>();
        stateMap = new HashMap<>();
        this.furnitureType = furnitureType;
        log = LoggerFactory.getLogger(clazz);
        Random random = new SecureRandom();
        furnitureName = this.furnitureType + '_' + Integer.toHexString((random.nextInt(69632)-4096) % 65536);
        closeState = new CloseState();
        state = closeState;
    }

    /**
     * 抽象启动设备
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
    */
    public void open() {
        state.open(this);
    }

    /**
     * 抽象关闭设备
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public void close() {
        state.close(this);
    }

    /**
     * 抽象设置设备
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public void doAction(KV<String,String> act2opt) {
        state.doAction(this, act2opt);
    }

    /**
     * 输出设备状态
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public String toString() {
        return furnitureType + ':' + furnitureName + ' ' + state.toString();
    }

    /**
     * 初始化设备，由state类调用
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public void init() {
        state = closeState;
        stateMap.clear();
    }

    /**
     * 由state类调用，更改状态
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public void setState(AbsState state) {
        this.state = state;
    }

    /**
     * 获取设备名
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public String getFurnitureName() {
        return furnitureName;
    }

    /**
     * 获取设备类型
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public String getFurnitureType() {
        return furnitureType;
    }

    /**
     * 查询设备操作表
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public String getLegitActValue(String key) {
        return legitAct.get(key);
    }

    public void writeInfoLog(String format, Object... arguments) {
        log.info(format, arguments);
    }

    public void writeErrorLog(String format, Object... arguments) {
        log.error(format, arguments);
    }

    /**
     * 抽象获取设备默认工作设置
     *
     * @author zhongziming
     * @date 2024/1/21 14:41
     * @version 1.0
     */
    public abstract String getDefaultMode();
}
