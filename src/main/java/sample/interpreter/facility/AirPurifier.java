package sample.interpreter.facility;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/20 19:22
 */
public class AirPurifier extends AbsFacility {
    public AirPurifier() {
        super("空气净化器", AirPurifier.class);
        legitAct.put("风速", "");
    }

    @Override
    public String getDefaultMode() {
        return "空气净化器 设置 风速 中";
    }
}
