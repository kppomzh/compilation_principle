package sample.interpreter.facility;

public class Calorifier extends AbsFacility {

    public Calorifier() {
        super("热水器", Calorifier.class);
        legitAct.put("摄氏度", "℃");
        legitAct.put("华氏度", "℉");
    }

    @Override
    public String getDefaultMode() {
        return "热水器 设置 摄氏度 45";
    }
}
