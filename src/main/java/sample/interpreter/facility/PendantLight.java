package sample.interpreter.facility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PendantLight extends AbsFacility {
    private static final Logger LOG = LoggerFactory.getLogger(PendantLight.class);

    public PendantLight() {
        super("吊顶灯", PendantLight.class);
        legitAct.put("光通量", "流明");
        legitAct.put("色温", "K");
    }

    @Override
    public String getDefaultMode() {
        return "吊顶灯 设置 色温 6500";
    }
}
