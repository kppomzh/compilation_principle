package sample.interpreter.config;

import sample.interpreter.facility.AbsFacility;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/21 11:15
 */
public class RegFacilityConf {
    private static final RegFacilityConf regConf = new RegFacilityConf();

    // 被控端注册列表
    private Map<String, AbsFacility> registerStateMap;

    public static RegFacilityConf getInstence() {
        return regConf;
    }

    public void register(AbsFacility... states) {
        registerStateMap = Stream.of(states).
                collect(Collectors.toMap(AbsFacility::getFurnitureType, item->item, (v1, v2)->v2));
    }

    public AbsFacility getFacility(String furnitureType) {
        return registerStateMap.get(furnitureType);
    }

    public Collection<String> getKeys() {
        return registerStateMap.keySet();
    }
}
