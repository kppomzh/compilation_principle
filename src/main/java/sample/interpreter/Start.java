package sample.interpreter;

import sample.interpreter.context.Context;
import sample.interpreter.facility.AirPurifier;
import sample.interpreter.facility.Calorifier;
import sample.interpreter.facility.PendantLight;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/18 22:30
 */
public class Start {

    public static void main(String[] args) {
        Context context = new Context(new Calorifier(), new PendantLight(), new AirPurifier());
        context.run("吊顶灯 设置 色温 6500;" +
                "正常模式;" +
                "热水器 设置 摄氏度 40;" +
                "吊顶灯 设置 光通量 100;" +
                "空气净化器 设置 亮度 100;" +
                "离开模式;" +
                "空气净化器 设置 风速 强;");
    }
}
