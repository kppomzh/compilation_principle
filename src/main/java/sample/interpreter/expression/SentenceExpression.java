package sample.interpreter.expression;

import sample.interpreter.config.RegFacilityConf;
import sample.interpreter.facility.AbsFacility;

import java.util.List;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/18 22:42
 */
public class SentenceExpression extends NonTerminalExpression<String> {
    private List<String> subCommands;
    private AbsFacility facility;
    private ActExpression actExpression;

    public SentenceExpression(String item) {
        subCommands = List.of(item.split(" "));
        facility = RegFacilityConf.getInstence().getFacility(subCommands.get(0));
        actExpression = new ActExpression(subCommands.get(1));
        if(subCommands.size() > 2) {
            actExpression.setAction(subCommands.get(2), subCommands.get(3));
        }
    }

    @Override
    public String interpreter() {
        actExpression.setFacility(facility);
        return actExpression.interpreter();
    }
}
