package sample.interpreter.expression;

import sample.interpreter.config.RegFacilityConf;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/20 21:34
 */
public class ModeExpression extends NonTerminalExpression<String> {
    private String mode;

    public ModeExpression(String item) {
        super();
        mode = item;
    }

    private List<SentenceExpression> sleepMode() {
        List<SentenceExpression> expressions = new ArrayList<>();
        RegFacilityConf regConf = RegFacilityConf.getInstence();
        regConf.getKeys().forEach(item -> {
        });
        return expressions;
    }

    private List<SentenceExpression> normalMode() {
        List<SentenceExpression> expressions = new ArrayList<>();
        RegFacilityConf regConf = RegFacilityConf.getInstence();
        regConf.getKeys().forEach(item -> {
            String open = regConf.getFacility(item).getFurnitureType() + " 启动";
            String doAction = regConf.getFacility(item).getDefaultMode();
            expressions.add(new SentenceExpression(open));
            expressions.add(new SentenceExpression(doAction));
        });
        return expressions;
    }

    private List<SentenceExpression> closeMode() {
        List<SentenceExpression> expressions = new ArrayList<>();
        RegFacilityConf regConf = RegFacilityConf.getInstence();
        regConf.getKeys().forEach(item -> {
            String close = regConf.getFacility(item).getFurnitureType() + " 关闭";
            expressions.add(new SentenceExpression(close));
        });
        return expressions;
    }

    @Override
    public String interpreter() {
        List<SentenceExpression> expressions = switch (mode) {
            case "离开模式" -> closeMode();
            // case "睡眠模式" -> sleepMode();
            case "正常模式" -> normalMode();
            default -> List.of();
        };
        expressions.forEach(SentenceExpression::interpreter);
        return null;
    }
}
