package sample.interpreter.expression;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/20 21:43
 */
public class AllCommandExpression extends NonTerminalExpression<String> {

    private List<String> commands = new ArrayList<>();
    private List<NonTerminalExpression> expressions = new ArrayList<>();

    public AbsExpression<String> setAllCommands(String allCommands) {
        commands = List.of(allCommands.split(";"));
        commands.forEach(item -> {
            if(item.contains("模式")) {
                expressions.add(new ModeExpression(item));
            } else {
                expressions.add(new SentenceExpression(item));
            }
        });
        return this;
    }

    @Override
    public String interpreter() {
        expressions.forEach(AbsExpression::interpreter);
        return null;
    }
}
