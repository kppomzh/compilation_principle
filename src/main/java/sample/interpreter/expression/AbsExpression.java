package sample.interpreter.expression;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/18 22:32
 */
public interface AbsExpression<T> {
    T interpreter();
}
