package sample.interpreter.expression;

import org.apache.beam.sdk.values.KV;
import sample.interpreter.facility.AbsFacility;

/**
 * @author zhongziming
 * @version 1.0
 * @date 2024/1/18 22:43
 */
public class ActExpression extends TerminalExpression {
    private final String state;
    private KV<String, String> act2opt;
    private AbsFacility facility;

    public ActExpression(String action) {
        state = action;
    }

    public void setAction(String act, String opt) {
        act2opt = KV.of(act, opt);
    }

    public void setFacility(AbsFacility facility) {
        this.facility = facility;
    }

    @Override
    public String interpreter() {
        switch (state) {
            case "启动":
                facility.open();
                break;
            case "关闭":
                facility.close();
                break;
            case "设置":
                facility.doAction(act2opt);
                break;
        }
        return facility.toString();
    }
}
