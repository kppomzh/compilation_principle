package com.test;

import java.security.SecureRandom;
import java.util.Random;
import java.util.stream.IntStream;

public class CpuTest {

    private int height;
    private int width;
    private int loop;

    public static void main(String[] args) {
        CpuTest ct = new CpuTest();
        int height = 6400;
        int width = 768;
        int loop = 131072;
        ct.init(height, width, loop);
        ct.nowTest();
    }

    private int[][] upperBoard;
    private int[][] board;
    private static final int[][] lifeKernel = new int[][]{{1,1,1}, {1,0,1}, {1,1,1}};

    public void init(int height, int width, int loop) {
        this.height = height;
        this.width = width;
        this.loop = loop;
        buildRandomBoard();
        upperBoard = new int[height+2][width+2];
    }

    private void buildRandomBoard() {
        board = new int[height][width];
        Random random = new SecureRandom();
        IntStream.range(0, height).parallel().forEach(i -> {
            IntStream.range(0, width).parallel().forEach(j -> {
                board[i][j] = random.nextBoolean() ? 1 : 0;
            });
        });
    }

    public int[][] nowTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            gameOfLife(board);
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("time :" + time + " ms");
        System.out.println("score :" + getScore(time));
        return board;
    }

    private double getScore(long time) {
        double oneTime = (double) time / loop;
        return 1000 / Math.exp(oneTime);
    }

    private void gameOfLife(int[][] board) {
        int height = board.length;
        int width = board[0].length;
        IntStream.range(0, height).parallel().forEach(i -> {
            System.arraycopy(board[i], 0, upperBoard[i+1], 1, width);
        });
        IntStream.range(0, height).parallel().forEach(i -> {
            for (int j = 0; j < width; j++) {
                int count = 0;
                for (int ci = 0; ci < lifeKernel.length; ci++) {
                    for (int cj = 0; cj < lifeKernel.length; cj++) {
                        count += upperBoard[i + ci][j + cj] * lifeKernel[ci][cj];
                    }
                }
                switch (count) {
                    case 2:
                        break;
                    case 3:
                        board[i][j] = 1;
                        break;
                    default:
                        board[i][j] = 0;
                        break;
                }
            }
        });
    }
}
