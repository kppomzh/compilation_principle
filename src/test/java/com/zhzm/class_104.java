package com.zhzm;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class class_104 {
    public int maxDepth(TreeNode root) {
        if(root==null)
            return 0;

        int res=0;
        List<TreeNode> floor1=new LinkedList<>(),floor2=new LinkedList<>();
        Stack<TreeNode> stack=new Stack<>();
        floor1.add(root);
        stack.push(root);

        while(!floor1.isEmpty()){
            res++;
            while(!floor1.isEmpty()){
                TreeNode node=floor1.remove(0);
                if(node.left!=null){
                    floor2.add(node.left);
                }
                if(node.right!=null){
                    floor2.add(node.right);
                }
            }
            floor1=floor2;
            floor2=new LinkedList<>();
        }

        while(stack.peek().left!=null || stack.peek().right!=null){
            stack.push(stack.peek().left);
        }

        return res;
    }

    public int maxDepth2(TreeNode root) {
        return root == null?0:Math.max(maxDepth(root.left),maxDepth(root.right))+1;
    }
}
