package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class class_633 {

    @Test
    public void test() {
        System.out.println(judgeSquareSum(9));
    }

    public boolean judgeSquareSum(int c) {
        if(c > 0) {
            while ((c & 1) == 0) {
                c >>= 1;
            }
        }
        for (int i = 3; i <= Math.sqrt(c); i += 2) {
            if(c % i != 0 || i % 4 != 3)
                continue;
            int exp = 0;
            while(c % i == 0) {
                c /= i;
                exp++;
            }
            if ((exp & 1) == 1) {
                return false;
            }
        }
        return c % 4 != 3 ;
    }

    /**
     * 获取一个整数的所有质因数
     *
     * @param number 待分解的整数
     * @return 质因数列表
     */
    public Set<Integer> getPrimeFactors(int number) {
        Set<Integer> factors = new HashSet<>();
        // 用2去除number，直到不能整除为止
        while ((number & 1) == 1) {
            factors.add(2);
            number >>= 1;
        }
        // 用奇数去除number，直到不能整除为止
        for (int i = 3; i <= Math.sqrt(number); i += 2) {
            while (number % i == 0) {
                factors.add(i);
                number /= i;
            }
        }
        // 如果number仍然是一个大于2的质数，则加入到列表中
        if (number > 2) {
            factors.add(number);
        }
        return factors;
    }


    public boolean judgeSquareSum2(int c) {
        if(c == 0)
            return true;
        while((c & 1) == 1) {
            c >>= 1;
        }
        return c % 4 == 1 ;
    }

    public boolean judgeSquareSum1(int c) {
        Set<Integer> set = new HashSet<>();
        int i = -1;
        int pow = -1;
        while(pow <= c) {
            i++;
            pow = (int) Math.pow(i, 2);
            set.add(pow);
            if(set.contains(c - pow)) {
                return true;
            }
        }
        return false;
    }
}
