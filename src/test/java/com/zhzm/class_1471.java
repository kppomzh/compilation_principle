package com.zhzm;

import java.util.Arrays;
import java.util.Comparator;

public class class_1471 {
    public static void main(String[] args) {
        class_1471 class1471=new class_1471();
        print(class1471.getStrongest(new int[]{-7,22,17,3},2));
        print(class1471.getStrongest(new int[]{-1,-2,-3,-4,-5,-6,-7,-8,-9},3));
        print(class1471.getStrongest(new int[]{-9,-8,-7,-6,-5,1,2,3,4},3));
        print(class1471.getStrongest(new int[]{-9,-8,-7,-6,1,2,3,4,5},3));
        print(class1471.getStrongest(new int[]{6,7,11,7,6,8},3));
        print(class1471.getStrongest(new int[]{6,-3,7,2,11},3));
    }

    private static void print(int[] res){
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i]);
            System.out.print(' ');
        }
        System.out.println();
    }

    public int[] getStrongest(int[] arr, int k) {
        int[] res = new int[k];
        Arrays.sort(arr);

        int middle=arr[(arr.length-1)/2]<<1;

        int left=0,right=arr.length-1;
        for(int i=0;i<k;i++){
            if(arr[left]+arr[right]>=middle){
                res[i]=arr[right];
                right--;
            } else {
                res[i]=arr[left];
                left++;
            }
        }
        return res;
    }

    public int[] getStrongest2(int[] arr, int k) {
        int[] res = new int[k];
        Arrays.sort(arr);

        int middle=arr[(arr.length-1)/2];

        int left=0,right=arr.length-1;
        for(int i=0;i<k;i++){
            if(Math.abs(arr[right]-middle)>=Math.abs(arr[left]-middle)){
                res[i]=arr[right];
                right--;
            } else {
                res[i]=arr[left];
                left++;
            }
        }
        return res;
    }
}
