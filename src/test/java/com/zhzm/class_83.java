package com.zhzm;

import com.zhzm.structure.object.ListNode;

// 删除排序链表中的重复元素
public class class_83 {

    public ListNode deleteDuplicates(ListNode head) {
        if(head!=null){
            ListNode cursor=head;
            while(cursor.next!=null){
                if(cursor.val==cursor.next.val){
                    cursor.next=cursor.next.next;
                } else {
                    cursor=cursor.next;
                }
            }
        }
        return head;
    }
}
