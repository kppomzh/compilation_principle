package com.zhzm;

public class class_2347 {
    public String bestHand(int[] ranks, char[] suits) {
        boolean flush=suits[3]==suits[4];
        for(int i=0;i<3&&flush;i++){
            flush = suits[i] == suits[3];
        }
        if(flush)
            return "Flush";

        int[] rankMap=new int[14];
        int max=0;
        String res;
        for (int i = 0; i < ranks.length; i++) {
            rankMap[ranks[i]]++;
            max=Math.max(max,rankMap[ranks[i]]);
        }
        if(max>=3)
            res="Three of a Kind";
        else if(max==2)
            res="Pair";
        else
            res="High Card";

        return res;
    }
}
