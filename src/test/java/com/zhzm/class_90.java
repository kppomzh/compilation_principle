package com.zhzm;

import org.junit.Test;

import java.util.*;

public class class_90 {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Map<Integer, Integer> numMap = new HashMap<>();
        for (int num : nums) {
            if (numMap.containsKey(num)) {
                numMap.put(num, numMap.get(num) + 1);
            } else {
                numMap.put(num, 1);
            }
        }

        res.add(Collections.emptyList());
        numMap.forEach((key, value) -> {
            int loopMax = res.size();
            for (int j = 0; j < value; j++) {
                Integer[] valueArr = new Integer[j+1];
                Arrays.fill(valueArr, key);
                for (int i = 0; i < loopMax; i++) {
                    List<Integer> temp = new ArrayList<>(res.get(i));
                    temp.addAll(List.of(valueArr));
                    res.add(temp);
                }
            }
        });
        return res;
    }

    @Test
    public void test() {
        System.out.println(subsetsWithDup1(new int[]{1,3,2,2,4,3,4}));
    }

    public List<List<Integer>> subsetsWithDup1(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        int lastNum = Integer.MIN_VALUE;
        Queue<Integer> lastList = new ArrayDeque<>();
        res.add(Collections.emptyList());
        for (int num : nums) {
            if (num != lastNum) {
                extracted(lastList, res);
            }
            lastNum = num;
            lastList.add(num);
        }
        extracted(lastList, res);
        return res;
    }

    private static void extracted(Queue<Integer> lastList, List<List<Integer>> res) {
        int loopMax = res.size();
        while(!lastList.isEmpty()){
            for (int i = 0; i < loopMax; i++) {
                List<Integer> temp = new ArrayList<>(res.get(i));
                temp.addAll(lastList);
                res.add(temp);
            }
            lastList.poll();
        }
    }
}
