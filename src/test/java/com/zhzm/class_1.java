package com.zhzm;

import java.util.HashMap;

public class class_1 {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer,Integer> hs=new HashMap<>();

        for(int loop=0;loop<nums.length;loop++){
            if(hs.containsKey(target-nums[loop])){
                return new int[]{loop,hs.get(target-nums[loop])};
            }
            hs.put(nums[loop],loop);
        }
        return new int[]{0,0};
    }
}
