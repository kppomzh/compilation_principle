package com.zhzm;

import com.zhzm.structure.object.ListNode;

public class class_21 {
    public ListNode merge2list(ListNode l1, ListNode l2){
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        ListNode res=new ListNode(),cur=res;
        while(true){
            if(l1.val> l2.val) {
                cur.next = l2;
                if(l2.next==null){
                    cur.next.next=l1;
                    break;
                } else {
                    l2=l2.next;
                }
            } else {
                cur.next = l1;
                if(l1.next==null){
                    cur.next.next=l2;
                    break;
                } else {
                    l1=l1.next;
                }
            }
            cur=cur.next;
        }

        return res.next;
    }
}

