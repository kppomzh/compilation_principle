package com.zhzm;

import java.util.HashMap;

public class class_677 {
    public void test(){
        MapSum mapSum=new MapSum();
        mapSum.insert("a",3);
        mapSum.insert("b",2);
        System.out.println(mapSum.sum("a"));
        System.out.println(mapSum.sum("apple"));
        mapSum.insert("app",2);
        System.out.println(mapSum.sum("ap"));
        mapSum.insert("apple",2);
//        System.out.println(mapSum.sum("ap"));
        System.out.println(mapSum.sum("ap"));
        System.out.println(mapSum.sum("app"));
        System.out.println(mapSum.sum("l"));
        System.out.println(mapSum.sum("apl"));

//        mapSum=new MapSum();
//        mapSum.insert("appled",2);
//        System.out.println(mapSum.sum("ap"));
//        mapSum.insert("apple",3);
//        System.out.println(mapSum.sum("a"));
//        mapSum.insert("apple",2);
//        System.out.println(mapSum.sum("a"));
    }
}

class MapSum {
    private Trie root;

    public MapSum() {
        root=new Trie();
    }

    public void insert(String key, int val) {
        root.insert(key,0,val);
    }

    public int sum(String prefix) {
        return root.sum(prefix,0);
    }
}


class Trie{
    Trie[] childs;
    int valueSum, value;
    boolean notEmpty;

    HashMap<Character,Trie> nodeHashMap;

    public Trie(){
        valueSum =0;
        value =0;
        childs = new Trie[26];
        notEmpty=false;
        nodeHashMap=new HashMap<>();
    }

    public int addTrie(String s,int idx,int cont){
        int res=0;
        if(idx<s.length()) {
            int local=s.charAt(idx)-97;
            if (childs[local] == null) {
                childs[local] = new Trie();
            }
            res=childs[local].addTrie(s,idx+1,cont);
            valueSum = valueSum - res + cont;
            notEmpty=true;
        } else {
            res= value;
            this.valueSum +=cont;
            if(!notEmpty){
                this.valueSum -= value;
            }
            value =cont;
        }
        return res;
    }

    public int insert(String str, int index, int val) {
        Trie child;
        int res;
        if (index < str.length()) {
            if (nodeHashMap.containsKey(str.charAt(index))) {
                child = nodeHashMap.get(str.charAt(index));
            } else {
                child = new Trie();
                nodeHashMap.put(str.charAt(index), child);
            }

            res = child.insert(str, index + 1, val);
            valueSum = valueSum - res + val;
        } else {
            res = value;
            value = val;
            valueSum += val;
        }

        return res;
    }
    public int getCont(String prefix,int idx){
        if(idx==prefix.length()){
            return valueSum;
        } else {
            return childs[prefix.charAt(idx)-97]==null?0:childs[prefix.charAt(idx)-97].getCont(prefix, idx+1);
        }
    }

    public int sum(String str, int index) {
        if (index == str.length())
            return valueSum;
        else if (nodeHashMap.containsKey(str.charAt(index))) {
            return nodeHashMap.get(str.charAt(index)).sum(str, index + 1);
        } else
            return 0;
    }
}