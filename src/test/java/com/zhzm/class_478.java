package com.zhzm;

import org.junit.Test;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

import static java.lang.Math.TAU;

public class class_478 {
    @Test
    public void test1() {
        Solution(4.0, 0.0, 0.0);
        for (int i = 0; i < 5; i++) {
            System.out.println(Arrays.toString(this.randPoint()));
        }
    }

    private double radius;
    private double x_center;
    private double y_center;

    private Random random;

    public void Solution(double radius, double x_center, double y_center) {
        this.radius = radius;
        this.x_center = x_center;
        this.y_center = y_center;
        random = new SecureRandom();
    }

    public double[] randPoint() {
        double angle = random.nextDouble(TAU);
        System.out.println(angle);
        double length = Math.sqrt(random.nextDouble(1d)) * radius;
        double deltaX = length * Math.cos(angle);
        double deltaY = length * Math.sin(angle);
        return new double[]{deltaX + x_center, deltaY + y_center};
    }
}
