package com.zhzm;

public class class_1828 {
    public int[] countPoints(int[][] points, int[][] queries) {
        int[] res = new int[queries.length];
        for(int i = 0; i < res.length; i++) {
            int nres = 0;
            int dis = queries[i][2] * queries[i][2];
            for(int j = 0; j < points.length; j++) {
                int deltaX = Math.abs(points[j][0] - queries[i][0]);
                int deltaY = Math.abs(points[j][1] - queries[i][1]);
                if(deltaY * deltaY + deltaX * deltaX <= dis)
                    nres++;
            }
            res[i] = nres;
        }
        return res;
    }
}
