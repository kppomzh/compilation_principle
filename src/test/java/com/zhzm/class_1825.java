package com.zhzm;

public class class_1825 {
    public static void main(String[] args) {
        MKAverage mkAverage=new MKAverage(3,1);
        mkAverage.addElement(58916);
        mkAverage.addElement(61899);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(85406);
        mkAverage.addElement(49757);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(27520);
        mkAverage.addElement(12303);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(63945);
        System.out.println();

        mkAverage=new MKAverage(3,1);
        mkAverage.addElement(3);
        mkAverage.addElement(1);
        mkAverage.addElement(10);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(5);
        mkAverage.addElement(5);
        mkAverage.addElement(5);
        System.out.println(mkAverage.calculateMKAverage());
        System.out.println();

        mkAverage=new MKAverage(10,2);
        mkAverage.addElement(3);
        mkAverage.addElement(1);
        mkAverage.addElement(10);
        mkAverage.addElement(5);
        mkAverage.addElement(10);
        mkAverage.addElement(73);
        mkAverage.addElement(7);
        mkAverage.addElement(27);
        mkAverage.addElement(9);
        mkAverage.addElement(5);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(50);
        mkAverage.addElement(21);
        mkAverage.addElement(15);
        mkAverage.addElement(58);
        mkAverage.addElement(80);
        System.out.println(mkAverage.calculateMKAverage());
        mkAverage.addElement(65);
        mkAverage.addElement(717);
        mkAverage.addElement(500);
        System.out.println(mkAverage.calculateMKAverage());
    }
}
