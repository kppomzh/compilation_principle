package com.zhzm;

import java.util.ArrayList;
import java.util.List;

public class LCP_04 {
    public static void main(String[] args) {
        LCP_04 lcp04=new LCP_04();
        System.out.println(lcp04.domino(2,3,new int[][]{{1,0},{1,1}}));
    }
    public int domino(int n, int m, int[][] broken) {
        if(broken.length==0){
            return (m*n)/2;
        }

        int[][] map=new int[n][m];

//        {
//            map[0][0]=2;
//            map[n-1][0]=2;
//            map[0][m-1]=2;
//            map[n-1][m-1]=2;
//        }
//        for (int i = 1; i < n - 1; i++) {
//            map[i][0]=3;
//            map[i][m-1]=3;
//            for (int j = 1; j < m - 1; j++) {
//                map[i][j]=4;
//            }
//        }
//        for (int i = 1; i < m - 1; i++) {
//            map[0][i]=3;
//            map[n-1][i]=3;
//        }

        for (int i = 0; i < broken.length; i++) {
            int[] broke=broken[i];
            map[broke[0]][broke[1]]=1;
        }

        int[] adjList=new int[n*m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(map[i][j]!=1) {
                    int cur = i * m + j + 1;

                    if (j + 1 < m && map[i][j + 1] == 0) {
                        int next = i * m + j + 1;
                        adjList[cur]=next;
                        adjList[next]=cur;
                    }
                    if (i + 1 < n && map[i + 1][j] == 0){
                        int next = (i+1) * m + j;
                        adjList[cur]=next;
                        adjList[next]=cur;
                    }
                }
            }
        }
        return 0;
    }

    private int hungarain(int[][] adjList){
        int res=0;
        return res;
    }
}
