package com.zhzm;

public class class_2383 {
    public int minNumberOfHours(int initialEnergy, int initialExperience, int[] energy, int[] experience) {
        int[] prefixexperience=new int[experience.length+1];
        prefixexperience[0]=initialExperience;

        int beforeExp=0;
        int beforeEng=0,needEng=0;
        for (int i = 0; i < experience.length; i++) {
            prefixexperience[i+1]=prefixexperience[i]+experience[i];
            if(prefixexperience[i]<=experience[i]){
                int tmp=experience[i]-prefixexperience[i]+1;
                beforeExp+=tmp;
                prefixexperience[i+1]+=tmp;
            }
            needEng+=energy[i];
        }

        if(initialEnergy<=needEng){
            beforeEng=needEng-initialEnergy+1;
        }
        return beforeEng+beforeExp;
    }
}
