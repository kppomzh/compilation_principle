package com.zhzm;

public class class_1139 {
    public static void main(String[] args) {
        class_1139 class1139 = new class_1139();

        long time=System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            class1139.largest1BorderedSquare(new int[][]{{1, 1, 1}, {0, 1, 0}, {0, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}});
            class1139.largest1BorderedSquare(new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1, 1},{0, 1, 1},{0, 1, 1}});
        }
        System.out.println((System.currentTimeMillis()-time)/1000d);
        time=System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            class1139.largest1BorderedSquare2(new int[][]{{1, 1, 1}, {0, 1, 0}, {0, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}});
            class1139.largest1BorderedSquare2(new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1, 1},{0, 1, 1},{0, 1, 1}});
        }
        System.out.println((System.currentTimeMillis()-time)/1000d);
    }

    int[][] dgrid, rgrid;

    int ylength, xlength;

    public int largest1BorderedSquare(int[][] grid) {
        ylength = grid.length;
        xlength = grid[0].length;
        dgrid = new int[ylength][xlength];
        rgrid = new int[ylength][xlength];

        int ylocal, xlocal;
        int res = 0;
        for (ylocal = 0; ylocal < ylength - res; ylocal++) {
            for (xlocal = 0; xlocal < xlength - res; xlocal++) {
                if (grid[ylocal][xlocal] == 1) {
                    res = Math.max(res, transfer(grid, ylocal, xlocal));
                }
            }
        }
        return res * res;
    }

    private int transfer(int[][] grid, int ylocal, int xlocal) {
        int dlength = getMaxDownLength2(grid, ylocal, xlocal);
        int rlength = getMaxRightLength2(grid, ylocal, xlocal);
        int min = Math.min(dlength, rlength);

        while (min > 1) {
            int rdlength = getMaxRightLength2(grid, ylocal + min - 1, xlocal);
            int drlength = getMaxDownLength2(grid, ylocal, xlocal + min - 1);
            if (rdlength >= min && drlength >= min)
                break;
            else
                min--;
        }
        return min;
    }

    /**
     * 从上级函数传入的[x,y]位置应当为1,递归传入不算
     *
     * @param grid
     * @param y
     * @param x
     * @return
     */
    private int getMaxDownLength2(int[][] grid, int y, int x) {
        if (grid[y][x] == 1 && dgrid[y][x] == 0) {
            if (y + 1 < ylength)
                dgrid[y][x] = getMaxDownLength2(grid, y + 1, x) + 1;
            else
                dgrid[y][x] = 1;
        }

        return dgrid[y][x];
    }

    /**
     * 从上级函数传入的[x,y]位置应当为1,递归传入不算
     *
     * @param grid
     * @param y
     * @param x
     * @return
     */
    private int getMaxRightLength2(int[][] grid, int y, int x) {
        if (grid[y][x] == 1 && rgrid[y][x] == 0) {
            if (x + 1 < xlength)
                rgrid[y][x] = getMaxRightLength2(grid, y, x + 1) + 1;
            else
                rgrid[y][x] = 1;
        }

        return rgrid[y][x];
    }

    public int largest1BorderedSquare2(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[][][] dp = new int[2][m+1][n+1];
        for (int i=0;i<m;++i) {
            for (int j=0;j<n;++j) {
                if (grid[i][j]==1) {
                    dp[0][i+1][j+1] = dp[0][i+1][j] + 1;
                    dp[1][i+1][j+1] = dp[1][i][j+1] + 1;
                }
            }
        }

        int ans = 0;

        for (int i=1;i<=m;++i) {
            for (int j=1;j<=n;++j) {
                int r = Math.min(dp[0][i][j], dp[1][i][j]);
                for (;r>ans;r--) {
                    if (dp[0][i-r+1][j]>=r && dp[1][i][j-r+1]>=r) {
                        ans = r;
                        break;
                    }
                }
            }
        }
        return ans * ans;
    }
}
