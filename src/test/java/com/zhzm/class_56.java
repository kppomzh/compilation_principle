package com.zhzm;

import java.util.*;

public class class_56 {
    public void test(){
        int[][] res=new class_56().merge(new int[][]{{1,4}});
    }

    public int[][] merge(int[][] intervals) {
        List<int[]> lres=new ArrayList<>();

        PriorityQueue<int[]> queue=new PriorityQueue<>((o1, o2) -> o1[0]-o2[0]);
        for(int i=0;i<intervals.length;i++){
            queue.offer(intervals[i]);
        }

        int[] tmpRange=queue.poll();
        while(!queue.isEmpty()){
            int[] interval=queue.poll();
            if(interval[0]<=tmpRange[1]){
                tmpRange[1]=Math.max(tmpRange[1],interval[1]);
            }
            else {
                lres.add(tmpRange);
                tmpRange=interval;
            }
        }
        lres.add(tmpRange);

        int[][] res=new int[lres.size()][];
        for(int i=0;i<lres.size();i++){
            res[i]=lres.get(i);
        }
        return res;
    }

    public int[][] merge2(int[][] intervals) {
        List<int[]> lres=new ArrayList<>();

        Arrays.sort(intervals, (o1, o2) -> o1[0]-o2[0]);

        int[] tmpRange=intervals[0];
        for (int i = 1; i < intervals.length; i++) {
            int[] interval=intervals[i];
            if(interval[0]<=tmpRange[1]){
                tmpRange[1]=Math.max(tmpRange[1],interval[1]);
            }
            else {
                lres.add(tmpRange);
                tmpRange=interval;
            }
        }
        lres.add(tmpRange);

        int[][] res=new int[lres.size()][];
        for(int i=0;i<lres.size();i++){
            res[i]=lres.get(i);
        }
        return res;
    }
}
