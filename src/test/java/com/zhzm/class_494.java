package com.zhzm;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class class_494 {
    /*
     * 生成函数解法
     * 假设我们用形如(x^-n+x^n)的公式来表达每一项的数取正还是取反的话，我们将可以以多项式幂次和系数的方式来指明每个加和结果到底有几种方式可以得到
     * 需要注意的是，所有的结果是关于0镜像对称的，这一点将会在接下来的解题过程中让读者觉得比较“显然”
     */
    @Test
    public void test() {
        int[] nums = new int[]{1, 1, 1, 1, 1};
        int target = 3;
        System.out.println(findTargetSumWays(nums, target));
        Assert.assertEquals(5, findTargetSumWays(nums, target));
    }

    @Test
    public void test1() {
        int[] nums = new int[]{1, 2, 3, 4, 5};
        int target = 1;
        System.out.println(findTargetSumWays(nums, target));
        Assert.assertEquals(3, findTargetSumWays(nums, target));
    }

    @Test
    public void test2() {
        int[] nums = new int[]{1, 0, 0};
        int target = 1;
        System.out.println(findTargetSumWays(nums, target));
        Assert.assertEquals(4, findTargetSumWays(nums, target));
    }

    @Test
    public void test3() {
        int[] nums = new int[]{1, 2, 1};
        int target = 0;
        System.out.println(findTargetSumWays(nums, target));
        Assert.assertEquals(2, findTargetSumWays(nums, target));
    }

    @Test
    public void test4() {
        int[] nums = new int[]{9,7,0,3,9,8,6,5,7,6};
        int target = 2;
        System.out.println(findTargetSumWays1(nums, target));
        Assert.assertEquals(40, findTargetSumWays1(nums, target));
    }

    @Test
    public void test5() {
        int[] nums = new int[]{9,7,3};
        int target = 1;
        System.out.println(findTargetSumWays(nums, target));
    }

    public int findTargetSumWays(int[] nums, int target) {
        target = Math.abs(target);
        if (nums.length == 1)
            return nums[0] == target ? 1 : 0;
        int res = 0;
        int sum = Arrays.stream(nums).parallel().sum();
        Arrays.sort(nums);
        Queue<Integer> queue = new LinkedList<>();
        queue.add(nums[nums.length - 1]);
        sum -= queue.peek();

        for (int i = nums.length - 2; i >= 0 && !queue.isEmpty(); i--) {
            int tempSize = queue.size();
            for (int j = 0; j < tempSize; j++) {
                int node = queue.poll();
                if (node + sum == target || node - sum == target) {
                    res += (node > 0 ? 1 : 2);
                } else if (node + sum > target && node - sum < target) {
                    queue.add(node - nums[i]);
                    queue.add(node + nums[i]);
                } else if (node + sum == -target || node - sum == -target) {
                    res += 2;
                }
            }
            sum -= nums[i];
        }
        int zeroNum = target == 0 ? 1 : 0;
        for (int num : nums) {
            if (num == 0)
                zeroNum++;
            else
                break;
        }
        return (int) (res * Math.pow(2, zeroNum));
    }

    public int findTargetSumWays1(int[] nums, int target) {
        target = Math.abs(target);
        if (nums.length == 1)
            return nums[0] == target ? 1 : 0;
        int res = 0;
        int sum = Arrays.stream(nums).parallel().sum();
        Arrays.sort(nums);
        Queue<Integer> queue = new LinkedList<>();
        queue.add(nums[nums.length - 1]);
        int sumP = sum - queue.peek();
        for (int i = nums.length - 2; i >= 0 && !queue.isEmpty(); i--) {
            int tempSize = queue.size();
            for (int j = 0; j < tempSize; j++) {
                int node = queue.poll();
                if (node + sumP == target || node - sumP == target) {
                    res++;
                } else if (node + sumP > target && node - sumP < target) {
                    queue.add(node - nums[i]);
                    queue.add(node + nums[i]);
                }
            }
            sumP -= nums[i];
        }
        queue.clear();
        queue.add(-nums[nums.length - 1]);
        int sumM = sum + queue.peek();
        for (int i = nums.length - 2; i >= 0 && !queue.isEmpty(); i--) {
            int tempSize = queue.size();
            for (int j = 0; j < tempSize; j++) {
                int node = queue.poll();
                if (node + sumM == target || node - sumM == target) {
                    res++;
                } else if (node + sumM > target && node - sumM < target) {
                    queue.add(node - nums[i]);
                    queue.add(node + nums[i]);
                }
            }
            sumM -= nums[i];
        }

        int zeroNum = target == 0 ? 1 : 0;
        for (int num : nums) {
            if (num == 0)
                zeroNum++;
            else
                break;
        }
        return (int) (res * Math.pow(2, zeroNum));
    }
}
