package com.zhzm;

import org.junit.Test;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class class_380 {
    @Test
    public void test() {
        RandomizedSet randomizedSet = new RandomizedSet();
        randomizedSet.insert(1); // 向集合中插入 1 。返回 true 表示 1 被成功地插入。
        randomizedSet.remove(2); // 返回 false ，表示集合中不存在 2 。
        randomizedSet.insert(2); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(3); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(7); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(5); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(8); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(4); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(9); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        randomizedSet.insert(12); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        System.out.println(randomizedSet.getRandom()); // getRandom 应随机返回 1 或 2 。
        randomizedSet.remove(1); // 从集合中移除 1 ，返回 true 。集合现在包含 [2] 。
        randomizedSet.insert(2); // 2 已在集合中，所以返回 false 。
        System.out.println(randomizedSet.getRandom()); // 由于 2 是集合中唯一的数字，getRandom 总是返回 2 。
    }

    class RandomizedSet {
        Set<Integer> randomMap;
        Random random = new Random();

        public RandomizedSet() {
            randomMap = new HashSet<>();
        }

        public boolean insert(int val) {
            if(randomMap.contains(val))
                return false;
            randomMap.add(val);
            return true;
        }

        public boolean remove(int val) {
            if (!randomMap.contains(val))
                return false;
            randomMap.remove(val);
            return true;
        }

        public Integer getRandom() {
            return randomMap.toArray(new Integer[0])[random.nextInt(randomMap.size())];
        }
    }
}
