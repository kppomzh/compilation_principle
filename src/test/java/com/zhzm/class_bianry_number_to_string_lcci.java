package com.zhzm;

public class class_bianry_number_to_string_lcci {
    public static void main(String[] args) {
        class_bianry_number_to_string_lcci lcci=new class_bianry_number_to_string_lcci();
        System.out.println(lcci.printBin(0.625));
    }
    public String printBin(double num) {
        StringBuilder sb=new StringBuilder();
        double approach=1d,step=0.5,res=0d;
        boolean trans=false;
        sb.append("0.");
        for(int i=0;i<30;i++){
            approach*=step;
            if(res+approach<num){
                res+=approach;
                sb.append('1');
            } else if(res+approach==num){
                res+=approach;
                sb.append('1');
                trans=true;
                break;
            } else {
                sb.append('0');
            }
        }
        if(trans)
            return sb.toString();
        else
            return "ERROR";
    }
}
