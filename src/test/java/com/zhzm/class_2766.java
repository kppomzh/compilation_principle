package com.zhzm;

import org.junit.Test;

import java.util.*;

public class class_2766 {
    @Test
    public void test() {
        int[] nums = new int[]{5,13,22,23,23,33};
        int[] moveFrom = new int[]{13,6,5,12};
        int[] moveTo = new int[]{1,12,12,13};
        List<Integer> res = relocateMarbles(nums, moveFrom, moveTo);
        System.out.println(res);
    }

    public List<Integer> relocateMarbles(int[] nums, int[] moveFrom, int[] moveTo) {
        Set<Integer> set = new HashSet<>();
        Map<Integer, Integer> preload = preload(moveFrom, moveTo);
        for(int num : nums) {
            set.add(preload.getOrDefault(num, num));
        }
        return set.stream().sorted().toList();
    }


    private Map<Integer, Integer> preload(int[] moveFrom, int[] moveTo) {
        Map<Integer, Integer> res = new HashMap<>();
        for (int i = 0; i < moveFrom.length; i++) {
            boolean exists = false;
            for (int j = i+1; j < moveFrom.length; j++) {
                if(moveTo[i] == moveFrom[j]) {
                    res.put(moveFrom[i], moveTo[j]);
                    exists = true;
                }
            }
            if(!exists)
                res.put(moveFrom[i], moveTo[i]);
        }
        return res;
    }
}
