package com.zhzm;

public class class_1802 {
    public static void main(String[] ar){
        class_1802 c1802=new class_1802();
        System.out.println(c1802.maxValue2(1,0,24));
        System.out.println(c1802.maxValue2(5,0,28));
        System.out.println(c1802.maxValue2(6,1,10));
        System.out.println(c1802.maxValue2(8,7,14));
        System.out.println(c1802.maxValue2(7,1,17));
    }

    public int maxValue1(int n, int index, int maxSum) {
        // int[] array=new int[n];
        // int a=(n-index)*(array[index]+array[index]-(n-1-index))/2;
        // int b=(index+1)*(array[index]+array[index]-(index))/2;
        // a+b-array[index]<=maxSum;
        //设array[index]为x
        // ((n-index)*(2*x-(n-1-index))+(index+1)*(2*x-(index)))/2-x<=maxSum;
        // (n*2*x-(n-index)*(n-1-index)-(index+1)*index)/2<=maxSum;
        // n*2*x<=2*maxSum+(n-index)*(n-1-index)+(index+1)*index;
        return (int)(2*maxSum+(n-index)*(n-1-index)+(index+1)*index)/(2*n);
    }

    public int maxValue2(int n, int index, int maxSum) {
        double left = Math.min(index,n-index-1);
        double right = Math.max(index,n-index-1);

        double upper = ((left + 1) * (left + 1) - 3 * (left + 1)) / 2 + left + 1 + (left + 1) + ((left + 1) * (left + 1) - 3 * (left + 1)) / 2 + right + 1;
        if (upper >= maxSum) {
            double c = left + right + 2 - maxSum;
            return (int) Math.floor((2 + Math.sqrt(4 - 4 * c)) / 2);
        }

               upper = (2 * (right + 1) - left - 1) * left / 2 + (right + 1) + ((right + 1) * (right + 1) - 3 * (right + 1)) / 2 + right + 1;
        if (upper >= maxSum) {
            double b = left + 1 - 3.0 / 2;
            double c = right + 1 + (-left - 1) * left / 2 - maxSum;
            return (int) Math.floor((-b + Math.sqrt(b * b - 2 * c)) );
        } else {
            double a = left + right + 1;;
            double b = (-left * left - left - right * right - right) / 2 - maxSum;
            return (int) Math.floor(-b / a);
        }
    }

    public int maxValue(int n, int index, int maxSum) {
        int tSum;
        if(maxSum==0)
            return 0;
        int res=1;

        if(maxSum>n){
            int firstArea=Math.min(n-index,index);
            int secondArea=Math.max(n-index,index);
            tSum=n;

            int i=firstArea==0?1:0;
            for(;i<firstArea;i++){
                int add=2*i+1;
                tSum+=add;
                if(tSum>maxSum){
                    return res;
                }
                res++;
            }
            for(;i<secondArea;i++){
                tSum+=firstArea;
                tSum+=i;
                if(tSum>maxSum){
                    return res;
                }
                res++;
            }
            while(true){
                tSum+=n;
                res++;
                if(tSum>maxSum){
                    res--;
                    break;
                } else if(tSum==maxSum) {
                    break;
                }
            }
        }
        return res;
    }
}
