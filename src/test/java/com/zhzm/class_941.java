package com.zhzm;

// 有效的山脉数组
public class class_941 {
    public boolean validMountainArray(int[] arr) {
        int left = 0, right = arr.length - 1;
        while(left < arr.length - 1 && arr[left] < arr[left + 1])
            left++;
        while(right > 0 && arr[right] < arr[right - 1])
            right--;
        return left == right && left != 0 && right != arr.length - 1;
    }
}
