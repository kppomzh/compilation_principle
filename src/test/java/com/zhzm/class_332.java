package com.zhzm;

import com.zhzm.structure.object.MaxHeap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class class_332 {
    private static final String startStation = "JFK";

    public List<String> findItinerary(List<List<String>> tickets) {
        List<String> res = new ArrayList<>();
        Map<String, MaxHeap<String>> fromToMap = new HashMap<>();
        Map<String, Integer> inDegree = new HashMap<>();
        for(List<String> ticket : tickets) {
            MaxHeap<String> fromTo = fromToMap.computeIfAbsent(ticket.get(0), k->new MaxHeap<>());
            fromTo.add(ticket.get(1));
            int inDegVal = inDegree.getOrDefault(ticket.get(1), 0) + 1;
            inDegree.put(ticket.get(1), inDegVal);
        }
        String station = startStation;
        while(true) {
            res.add(station);
            if(fromToMap.isEmpty()) {
                break;
            }
            String nextStation;
            // 入度为1，出度为2
            if(inDegree.get(station) == 1 && fromToMap.get(station).size() == 2) {
                nextStation = findEndNode(station, fromToMap, inDegree);
            } else {
                nextStation = fromToMap.get(station).removeTop();
            }
            int inDegVal = inDegree.get(nextStation) - 1;
            inDegree.put(nextStation, inDegVal);
            station = nextStation;
        }
        return res.stream().sorted().collect(Collectors.toList());
    }

    private String findEndNode(String station,
                                Map<String, MaxHeap<String>> fromToMap,
                                Map<String, Integer> inDegree) {
        if(inDegree.get(station) == 1 && fromToMap.get(station).size() == 2) {

        } else if(inDegree.get(station) == 1 && fromToMap.get(station).size() == 1) {

        } else {

        }
        String littleSta = fromToMap.get(station).removeTop();
        String moveTo = fromToMap.get(station).removeTop();
        fromToMap.get(station).add(littleSta);
        return moveTo;
    }
}
