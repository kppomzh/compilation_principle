package com.zhzm;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class class_94 {
    public void test(){
        class_94 c94=new class_94();
        TreeNode root = new TreeNode(1), left = new TreeNode(2),right = new TreeNode(3);
        root.left = left;
        root.right = right;
//        left.left = right;
        right.right=new TreeNode(5);
        right.left=new TreeNode(4);
        System.out.println(c94.inorderTraversal(root));
    }

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res=new LinkedList<>();

        if(root!=null){
            Stack<TreeNode> stack=new Stack<>();

            TreeNode cursor = root;
            while(!stack.isEmpty()||cursor!=null){
                if(cursor==null)
                    cursor = stack.pop();
                else while (cursor.left != null) {
                    stack.push(cursor);
                    cursor = cursor.left;
                }

                res.add(cursor.val);
                if (cursor.right != null) {
                    cursor = cursor.right;
                } else {
                    cursor=null;
                }
            }
        }

        return res;
    }
}
