package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class class_228 {
    @Test
    public void test() {
        System.out.println(summaryRanges1(new int[]{0,2,3,4,6,8,9}));
    }

    public List<String> summaryRanges(int[] nums) {
        if(nums.length == 0)
            return Collections.emptyList();
        List<String> res = new ArrayList<>();
        int start = nums[0];
        int end = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if(nums[i] == end+1) {
                end = nums[i];
                continue;
            } else if(start == end) {
                res.add(String.valueOf(start));
            } else {
                res.add(String.format("%d->%d", start, end));
            }
            start = nums[i];
            end = nums[i];
        }
        if(start == end) {
            res.add(String.valueOf(start));
        } else {
            res.add(start + "->" + end);
        }
        return res;
    }

    public List<String> summaryRanges1(int[] nums) {
        return subRanges(nums, 0 , nums.length-1);
    }

    private List<String> subRanges(int[] nums, int start, int end) {
        if(end < start)
            return Collections.emptyList();
        else if(start == end) {
            return List.of(String.valueOf(nums[start]));
        } else if(end - start == nums[end] - nums[start]) {
            return List.of(String.format("%d->%d", nums[start], nums[end]));
        } else {
            List<String> res = new ArrayList<>();
            for (int i = end; i > start ; i--) {
                if(nums[i] - nums[i-1] != 1) {
                    res.addAll(subRanges(nums, start, i-1));
                    res.addAll(subRanges(nums, i, end));
                    break;
                }
            }
            return res;
        }
    }

    public List<String> summaryRanges3(int[] nums) {
        List<Integer> list = new ArrayList<>();
        List<String> res = new ArrayList<>();
        if (nums.length > 0) {        //在数组非空时才需要处理
            for (int i : nums) {
                if (list.isEmpty()) {
                    list.add(i);
                } else {
                    if (i - list.get(list.size() - 1) != 1) {
                        if (list.size() == 1) {
                            res.add(String.valueOf(list.get(0)));
                            list.clear();
                            list.add(i);
                        } else {
                            res.add(list.get(0) + "->" + list.get(list.size() - 1));
                            list.clear();
                            list.add(i);
                        }
                    } else {
                        list.add(i);
                    }
                }
            }
            if (list.size() > 1) {
                res.add(list.get(0) + "->" + list.get(list.size() - 1));
            }else{
                res.add(String.valueOf(list.get(0)));
            }
        }
        return res;
    }

    public List<String> summaryRanges4(int[] nums) {
        List<Integer> list = new ArrayList<>();
        List<String> res = new ArrayList<>();
        if (nums.length > 0) {        //在数组非空时才需要处理
            for (int i : nums) {
                if (list.isEmpty()) {
                    list.add(i);
                } else {
                    if (i - list.get(list.size() - 1) != 1) {
                        if (list.size() == 1) {
                            res.add(String.valueOf(list.get(0)));
                            list.clear();
                            list.add(i);
                        } else {
                            res.add(list.get(0) + "->" + list.get(list.size() - 1));
                            list.clear();
                            list.add(i);
                        }
                    } else {
                        list.add(i);
                    }
                }
            }
            if (list.size() > 1) {
                res.add(list.get(0) + "->" + list.get(list.size() - 1));
            } else {
                res.add(String.valueOf(list.get(0)));
            }
        }
        return res;
    }
}
