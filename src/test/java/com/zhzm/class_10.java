package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class class_10 {
    @Test
    public void test(){
        System.out.println(isMatch("aabbbaaccccca"
                ,"a.*ac*cccca"));
        System.out.println(isMatch("aac","a.*c"));
        System.out.println(isMatch("aac","c*aac"));
        System.out.println(isMatch("a","a.*"));
        System.out.println(isMatch("ca","c.*a"));
        System.out.println(isMatch("a",".*..a"));
    }

    public boolean isMatch(String s, String p) {
        parser ps=new parser(p);
        return ps.match(s,0,0);
    }

    class parser{
        List<Integer> strParts;
        public parser(String p){
            strParts=new ArrayList<>();
            for (int i = 0,index=0; i < p.length(); i++) {
                if(p.charAt(i)=='*'){
                    strParts.set(index-1,strParts.get(index-1)+128);
                } else if(p.charAt(i)=='+'){
                    index++;
                    strParts.add((int) p.charAt(i));
                    strParts.add(p.charAt(i)+128);
                } else {
                    strParts.add((int) p.charAt(i));
                    index++;
                }
            }
        }

        public boolean match(String s,int index,int partLocal){
            if(index==s.length()&&partLocal==strParts.size())
                return true;
            if(partLocal>=strParts.size())
                return false;
            if(index>=s.length()){
                for (int i = partLocal; i < strParts.size(); i++) {
                    if (strParts.get(i) < 128 && strParts.get(i) != '.') {
                        return false;
                    }
                }
                return true;
            }

            Integer part=strParts.get(partLocal);
            if(part<128){
                if(part=='.' || part==s.charAt(index)){
                    return match(s,index+1,partLocal+1);
                }
                return false;
            } else {
                if(part=='.'+128 || part==s.charAt(index)+128){
                    if(match(s,index,partLocal+1))
                        return true;
                    if(match(s,index+1,partLocal)){
                        return true;
                    }
                    else {
                        return match(s,index+1,partLocal+1);
                    }
                }
                return match(s,index,partLocal+1);
            }
        }
    }
}
