package com.zhzm;

import org.junit.Test;

// 反转单词前缀
public class class_2000 {
    @Test
    public void test() {
        String word = "xyxzxe";
        char ch = 'z';
        System.out.println(reversePrefix(word, ch));
    }
    public String reversePrefix(String word, char ch) {
        char[] res = word.toCharArray();
        int idx = 0;
        boolean find = false;
        for (; idx < word.length(); idx++) {
            res[word.length() - idx - 1] = word.charAt(idx);
            if(ch == word.charAt(idx)) {
                find = true;
                idx++;
                char[] temp = new char[word.length()];
                System.arraycopy(res, word.length() - idx, temp, 0, idx);
                res = temp;
                break;
            }
        }
        if(idx <= word.length()) {
            System.arraycopy(word.toCharArray(), idx, res, idx, word.length() - idx);
        }
        return find ? new String(res) : word;
    }
}
