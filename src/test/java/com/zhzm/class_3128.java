package com.zhzm;

import org.junit.Test;

// 直角三角形
public class class_3128 {
    @Test
    public void test() {
        int[][] grid = new int[][]{
                {1, 0, 1}, // 2
                {1, 0, 0}, // 1
                {1, 0, 0}, // 1
                {0, 0, 1}, // 1
                //3, 0, 2
        };
        System.out.println(numberOfRightTriangles(grid));
    }

    @Test
    public void test1() {
        int[][] grid = new int[][]{
                {1, 1, 1}, // 3
                {1, 0, 1}, // 2
                //2, 1, 2
        };
        System.out.println(numberOfRightTriangles(grid));
    }

    public long numberOfRightTriangles(int[][] grid) {
        long res = 0L;
        int height = grid.length;
        int width = grid[0].length;
        int[] heightCount = new int[height];
        int[] widthCount = new int[width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                heightCount[i] += grid[i][j];
                widthCount[j] += grid[i][j];
            }
        }
        for (int i = 0; i < height; i++) {
            if (heightCount[i] < 2)
                continue;
            for (int j = 0; j < width; j++) {
                res += (grid[i][j] * (heightCount[i] - 1) * (widthCount[j] - 1L));
            }
        }
        return res;
    }
}
