package com.zhzm;

import com.zhzm.structure.object.Heap;
import com.zhzm.structure.object.MinHeap;
import org.junit.Test;

public class class_1705 {
    @Test
    public void test() {
        int[] apples = {3,0,0,0,0,2};
        int[] days = {3,0,0,0,0,2};
        System.out.println(eatenApples(apples, days));
    }

    public int eatenApples(int[] apples, int[] days) {
        int res = 0;
        Heap<AppleCont> heap = new MinHeap<>();
        int day = 0;
        for (; day < apples.length; day++) {
            if(apples[day] > 0)
                heap.add(new AppleCont(day+days[day], apples[day]));

            while (!heap.isEmpty())
                if(heap.getTop().day <= day)
                    heap.removeTop();
                else
                    break;
            if(heap.isEmpty())
                continue;
            res++;
            int size = heap.getTop().size - 1;
            if(size == 0)
                heap.removeTop();
            else
                heap.getTop().size = size;
        }
        while (!heap.isEmpty()) {
            if (heap.getTop().day <= day)
                heap.removeTop();
            else {
                res++;
                int size = heap.getTop().size - 1;
                if(size == 0)
                    heap.removeTop();
                else
                    heap.getTop().size = size;
                day++;
            }
        }
        return res;
    }
}

class AppleCont implements Comparable<AppleCont> {
    int size;
    int day;

    public AppleCont(int day, int size) {
        this.day = day;
        this.size = size;
    }

    @Override
    public int compareTo(AppleCont o) {
        return this.day - o.day;
    }
}

