package com.zhzm;

import com.zhzm.structure.object.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_138 {
    public Node copyRandomList(Node head) {
        Map<Node, Node> nodemap = new HashMap<>();
        Node cursor = head;
        while (cursor != null) {
            Node temp = new Node(cursor.val);
            nodemap.put(cursor, temp);
            cursor = cursor.next;
        }
        for (Node key : nodemap.keySet()) {
            nodemap.get(key).next = nodemap.get(key.next);
            nodemap.get(key).random = nodemap.get(key.random);
        }
        return nodemap.get(head);
    }
}
