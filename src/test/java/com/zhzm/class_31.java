package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

public class class_31 {
    int[] n=new int[]{2,6,1,7,3,2};

    @Test
    public void test() {
        nextPermutation(n);
        System.out.println(Arrays.toString(n));
    }

    public void nextPermutation(int[] nums) {
        int i = nums.length-1;
        for ( ;i >= 1; i--) {
            int ll=nums[i]-nums[i-1];
            if(ll>0){
                int llidx=i;
                for (int j = i; j < nums.length; j++) {
                    if(nums[j]-nums[i-1]>0 && ll>nums[j]-nums[i-1]){
                        llidx=j;
                        ll=nums[j]-nums[i-1];
                    }
                }

                int tmp=nums[i-1];
                nums[i-1]=nums[llidx];
                nums[llidx]=tmp;

                Arrays.sort(nums,i,nums.length);
                break;
            }
        }

        //说明从后向前，各位数字是严格单调递增的（后一位等于前一位的话视为递增）
        if(i==0){
            Arrays.sort(nums);
        }
    }
}
