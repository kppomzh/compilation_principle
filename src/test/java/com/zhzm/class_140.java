package com.zhzm;

import java.util.*;

public class class_140 {
    public static void main(String[] args) {
        class_140 class140=new class_140();
        print(class140.wordBreak("catsanddog",Arrays.asList("cat","cats","and","sand","dog")));
        print(class140.wordBreak("catsandog",Arrays.asList("cat","cats","and","sand","dog")));
        print(class140.wordBreak("pineapplepenapple",Arrays.asList("apple","pen","applepen","pine","pineapple")));
        print(class140.wordBreak("aaaaaaa",Arrays.asList("aaaa","aaa")));
    }

    private static void print(List<String> res){
        for (String str:res) {
            System.out.print(str);
            System.out.println();
        }
    }

    Trie2 t2;
    public List<String> wordBreak(String s, List<String> wordDict) {
        t2=new Trie2();
        for (String str:wordDict) {
            t2.addTrie(str,0);
        }

        return findStr(s,0);
    }

    public List<String> wordBreak2(String s, List<String> wordDict) {
        Map<Integer, List<StringBuilder>> map = new HashMap<>();
        List<StringBuilder> wordBreaks = backtrack2(s, s.length(), new HashSet<>(wordDict), 0, map);
        List<String> breakList = new LinkedList<>();
        for (StringBuilder wordBreak : wordBreaks) {
            breakList.add(wordBreak.toString());
        }
        return breakList;
    }

    private LinkedList<String> findStr(String str, int idx){
        List<String> words=t2.getCont(str,idx);
        LinkedList<String> res=new LinkedList<>();
        for (String word:words){
            int length=word.length();
            LinkedList<String> tmps;
            if(idx+length<str.length()){
                tmps=findStr(str,idx+length);
                for (String tmp:tmps) {
                    res.add(word+' '+tmp);
                }
            } else {
                res.add(word);
            }
        }
        return res;
    }

    public List<List<String>> backtrack(String s, int length, Set<String> wordSet, int index, Map<Integer, List<List<String>>> map) {
        if (!map.containsKey(index)) {
            List<List<String>> wordBreaks = new LinkedList<>();
            if (index == length) {
                wordBreaks.add(new LinkedList<>());
            }
            for (int i = index + 1; i <= length; i++) {
                String word = s.substring(index, i);
                if (wordSet.contains(word)) {
                    List<List<String>> nextWordBreaks = backtrack(s, length, wordSet, i, map);
                    for (List<String> nextWordBreak : nextWordBreaks) {
                        LinkedList<String> wordBreak = new LinkedList<>(nextWordBreak);
                        wordBreak.offerFirst(word);
                        wordBreaks.add(wordBreak);
                    }
                }
            }
            map.put(index, wordBreaks);
        }
        return map.get(index);
    }

    public List<StringBuilder> backtrack2(String s, int length, Set<String> wordSet, int index, Map<Integer, List<StringBuilder>> map) {
        if (!map.containsKey(index)) {
            List<StringBuilder> wordBreaks = new LinkedList<>();
            if (index == length) {
                wordBreaks.add(new StringBuilder());
            }
            for (int i = index + 1; i <= length; i++) {
                String word = s.substring(index, i);
                if (wordSet.contains(word)) {
                    List<StringBuilder> nextWordBreaks = backtrack2(s, length, wordSet, i, map);
                    for (StringBuilder nextWordBreak : nextWordBreaks) {
                        nextWordBreak.insert(0,' ');
                        nextWordBreak.insert(0,word);
                        wordBreaks.add(nextWordBreak);
                    }
                }
            }
            map.put(index, wordBreaks);
        }
        return map.get(index);
    }
}

class Trie2{
    Map<Character,Trie2> childs;
    String stop=null;

    public Trie2(){
        childs=new HashMap<>();
    }

    public void addTrie(String s,int idx){
        if(idx<s.length()) {
            Trie2 t;
            if (childs.containsKey(s.charAt(idx))) {
                t = childs.get(s.charAt(idx));
            } else {
                t = new Trie2();
                childs.put(s.charAt(idx), t);
            }
            t.addTrie(s,idx+1);
        } else {
            stop=s;
        }
    }

    public List<String> getCont(String prefix,int idx){
        List<String> res;
        if(idx < prefix.length() && childs.containsKey(prefix.charAt(idx))){
            res=childs.get(prefix.charAt(idx)).getCont(prefix,idx+1);
        } else
            res=new ArrayList<>();

        if(stop!=null){
            res.add(stop);
        }
        return res;
    }
}