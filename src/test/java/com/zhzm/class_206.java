package com.zhzm;

import com.zhzm.structure.object.ListNode;

public class class_206 {
    public ListNode reverseList(ListNode head) {
        ListNode node1 = null;
        ListNode node2 = head;
        while(node2 != null) {
            ListNode temp = node2.next;
            node2.next = node1;
            node1 = node2;
            node2 = temp;
        }
        return node1;
    }
}
