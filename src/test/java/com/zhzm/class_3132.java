package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

public class class_3132 {
    @Test
    public void test() {
        minimumAddedInteger(new int[]{4,6,3,1,4,2,10,9,5}, new int[]{5,10,3,2,6,1,9});
    }

    public int minimumAddedInteger(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int lastIdx = nums2.length - 1;
        int res = Integer.MAX_VALUE;
        for(int i = 0; i < 3; i++) {
            int idx = 0;
            int tempRes = nums2[0] - nums1[i];
            for (int j = 1; j < nums2.length;) {
                if(tempRes == nums2[j] - nums1[i+idx]) {
                    j++;
                }
                idx++;
            }
            if(nums2[0] - nums1[i] == nums2[lastIdx] - nums1[i + lastIdx]) {
                res = Math.min(res, nums2[0] - nums1[i]);
            }
        }
        return res;
    }
}
