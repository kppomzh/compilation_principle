package com.zhzm;

import java.util.List;
import java.util.stream.IntStream;

public class class_412 {
    public List<String> fizzBuzz(int n) {
        return IntStream.rangeClosed(1,n).boxed().map(i -> {
            if(i % 15 == 0)
                return "FizzBuzz";
            else if(i % 5 == 0)
                return "Buzz";
            else if (i % 3 == 0)
                return "Fizz";
            else
                return String.valueOf(i);
        }).toList();
    }
}
