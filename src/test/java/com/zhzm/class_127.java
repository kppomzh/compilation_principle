package com.zhzm;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class class_127 {
    @Test
    public void test1() {
        String beginWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");
        int res = ladderLength(beginWord, endWord, wordList);
        System.out.println(res);
    }

    private int ladderLength(String beginWord, String endWord, List<String> wordList) {
        return 0;
    }

    public int ladderLength1(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordSet = new HashSet<>(wordList);
        if (!wordSet.contains(endWord))
            return 0;

        boolean findEnd = false;
        int res = 0;
        Set<String> nextKey = Set.of(beginWord);
        while (!nextKey.isEmpty()) {
            wordSet.removeAll(nextKey);
            Set<String> temp = new HashSet<>();
            nextKey.forEach(ks -> {
                Set<String> ksNext = compileDiff(ks, wordSet);
                temp.addAll(ksNext);
            });
            res++;
            if (temp.contains(endWord)) {
                findEnd = true;
                break;
            } else
                nextKey = temp;
        }
        return findEnd ? res + 1 : 0;
    }

    private Set<String> compileDiff(String source, Set<String> targets) {
        return targets.parallelStream().filter(target->{
            int diff = 0;
            for (int i = 0; i < source.length(); i++) {
                if (source.charAt(i) != target.charAt(i)) {
                    diff++;
                    if (diff > 1)
                        break;
                }
            }
            return diff == 1;
        }).collect(Collectors.toSet());
    }
}
