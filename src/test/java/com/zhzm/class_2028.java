package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

public class class_2028 {
    @Test
    public void test1() {
        System.out.println(Arrays.toString(missingRolls(new int[]{3,2,4,3}, 4, 2)));
    }

    public int[] missingRolls(int[] rolls, int mean, int n) {
        int rollSum = Arrays.stream(rolls).parallel().sum(); // 改为循环速度可以极大提高
        int meanSum = mean * (n + rolls.length);
        int assign = meanSum - rollSum;
        if(assign < n || 6 * n < assign)
            return new int[0];
        int base = assign / n;
        int reminder = assign % n;
        int[] res = new int[n];
        for (int i = reminder; i < n; i++) {
            res[i] = base;
        }
        for (int i = 0; i < reminder; i++) {
            res[i] = 1 + base;
        }
        return res;
    }
}
