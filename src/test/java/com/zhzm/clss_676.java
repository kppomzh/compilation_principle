package com.zhzm;

import com.zhzm.structure.object.Trie;
import org.junit.Test;

// 魔法字典
public class clss_676 {
    @Test
    public void test() {
        MagicDictionary magicDictionary = new MagicDictionary();
//        magicDictionary.buildDict(new String[]{"hello", "hallo", "leetcode"});
//        System.out.println(magicDictionary.search("hello"));
//        System.out.println(magicDictionary.search("hhllo"));
//        System.out.println(magicDictionary.search("hell"));
//        System.out.println(magicDictionary.search("leetcoded"));
        magicDictionary.buildDict(new String[]{"hello", "hallo", "judge"});
        System.out.println(magicDictionary.search("judge"));
    }
}

class MagicDictionary {
    Trie trie = new Trie();

    public void buildDict(String[] dictionary) {
        for (int i = 0; i < dictionary.length; i++) {
            trie.insert(dictionary[i]);
        }
    }

    public boolean search(String searchWord) {
        return trie.searchMagic(0, searchWord);
    }
}