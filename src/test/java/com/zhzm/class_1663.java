package com.zhzm;

import java.util.Arrays;

public class class_1663 {
    public static void main(String[] args) {
        class_1663 class1663=new class_1663();
        System.out.println(class1663.getSmallestString(3,27));
    }
    public String getSmallestString(int n, int k) {
        char[] sc=new char[n];
        int start=n+25;
        while(n>0){
            if(k<=start){
                Arrays.fill(sc,0,n,'a');
                sc[n-1]+=k-n;
                break;
            } else {
                sc[n-1]='z';
                n--;
                k-=26;
                start--;
            }
        }

        return String.valueOf(sc);
    }
}
