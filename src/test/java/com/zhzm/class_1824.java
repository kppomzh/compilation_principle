package com.zhzm;

import java.util.ArrayList;
import java.util.List;

public class class_1824 {
    public static void main(String[] args) {
        class_1824 class1824=new class_1824();
        System.out.println(class1824.minSideJumps(new int[]{0,1,2,3,0}));
        System.out.println(class1824.minSideJumps(new int[]{0,1,1,3,3,0}));
        System.out.println(class1824.minSideJumps(new int[]{0,2,1,0,3,0}));
        System.out.println(class1824.minSideJumps(new int[]{0,3,3,0,3,1,3,0,2,2,0}));
    }

    public int minSideJumps(int[] obstacles) {
        int now=2,res=0;
        List<int[]> compact=new ArrayList<>();
        compact.add(new int[]{-2,0});
        for (int i = 0; i < obstacles.length; i++) {
            if(obstacles[i]!=0){
                compact.add(new int[]{i,obstacles[i]});
            }
        }
        compact.add(new int[]{obstacles.length,-10});

        for (int i = 0; i < compact.size()-2; i++) {
            if(compact.get(i+1)[1]==now){
                if(compact.get(i)[0]==compact.get(i+1)[0]-1){
                    now=6-compact.get(i)[1]-compact.get(i+1)[1];
                } else {
                    int next=compact.get(i+1)[1],nnext=compact.get(compact.size()-1)[1];
                    for (int j = i+2; j < compact.size(); j++) {
                        if(compact.get(j)[1]!=now) {
                            nnext = compact.get(j)[1];
                            break;
                        }
                    }
                    now=6-nnext-next;
                }
                res++;
            }
        }

        return res;
    }
}
