package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class class_46 {
    @Test
    public void test() {
        System.out.println(permute(new int[]{1,2,3,4,5,6,7,8}));
    }

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        permuteHelper(nums, 0, result);
        return result;
    }

    private void permuteHelper(int[] nums, int start, List<List<Integer>> result) {
        if (start == nums.length) {
            result.add(IntStream.of(nums).boxed().toList());
            return;
        }
        for (int i = start; i < nums.length; i++) {
            swap(nums, start, i);
            permuteHelper(nums, start + 1, result);
            swap(nums, start, i);
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public List<List<Integer>> permute1(int[] nums) {
        return permute(nums.length-1,nums);
    }

    private List<List<Integer>> permute(int idx,int[] nums) {
        LinkedList<List<Integer>> res=new LinkedList<>();

        if(idx==0){
//            res.add(new ArrayList<>(List.of(nums[idx])));
            res.add(List.of(nums[idx]));
        } else {
            List<List<Integer>> tmp=permute(idx-1,nums);
            for(int i=1;i<=idx;i++){
                for(List<Integer> list:tmp){
                    res.addLast(new ArrayList<>(list));
                    res.getLast().add(i,nums[idx]);
                }
            }
            for(List<Integer> list:tmp){
                res.addLast(list);
                res.getLast().add(0,nums[idx]);
            }
        }

        return res;
    }
}
