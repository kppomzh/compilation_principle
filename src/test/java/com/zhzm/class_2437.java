package com.zhzm;

public class class_2437 {
    public int countTime(String time) {
        int res;
        if(time.startsWith("??"))
            res = 24;
        else if(time.startsWith("?"))
            res = time.charAt(1) > '4' ? 2 : 3;
        else if(time.charAt(1) == '?') {
            res = time.charAt(0) == '2' ? 4 : 10;
        } else {
            res = 1;
        }
        for(int i = 0; i < 5; i++) {
            if(time.charAt(i) == '?') {
                switch (i) {
                    case 3:
                        res *= 6;
                        break;
                    case 4:
                        res *= 10;
                        break;
                }
            }
        }
        return res == 1 ? 0 : res;
    }
}
