package com.zhzm;

import org.junit.Test;

public class class_572 {
    @Test
    public void test() {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(4);
        root.right = new TreeNode(5);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(2);
        root.left.right.left = new TreeNode(0);
        TreeNode subRoot = new TreeNode(4);
        subRoot.left = new TreeNode(1);
        subRoot.right = new TreeNode(2);
        System.out.println(isSubtree(root, subRoot));
    }

    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        System.out.println(tree2str(root));
        System.out.println(tree2str(subRoot));
        return tree2str(root).contains(tree2str(subRoot));
    }

    public String tree2str(TreeNode t) {
        if (t == null) {
            return "n";
        }
        // 前序遍历位置
        return ","+t.val +
                tree2str(t.left) +
                tree2str(t.right);
    }

    public boolean isSubtree1(TreeNode root, TreeNode subRoot) {
        if(root == null) {
            return false;
        }
        if(root.val != subRoot.val) {
            return isSubtree1(root.left, subRoot) || isSubtree1(root.right, subRoot);
        } else {
            return (isSubtreeLock(root.left, subRoot.left) && isSubtreeLock(root.right, subRoot.right)) ||
                    (isSubtree1(root.left, subRoot) || isSubtree1(root.right, subRoot));
        }
    }

    public boolean isSubtreeLock(TreeNode root, TreeNode subRoot) {
        if(root == null && subRoot == null) {
            return true;
        }
        if(root == null || subRoot == null) {
            return false;
        }
        if(root.val != subRoot.val) {
            return false;
        }
        return isSubtreeLock(root.left, subRoot.left) && isSubtreeLock(root.right, subRoot.right);
    }
}
