package com.zhzm;

public class class_70 {
    int C(int m, int n)    //C^m_n = n! / m! / (n-m)!
    {
        m=Math.min(m,n-m);

        long ret = 1;
        int i = n - m + 1, j = 1;
        for (; j <= m; i++, j++) {    //相当于约掉公约数
            ret *= i;
            ret /= j;
        }
        return (int) (ret);
    }

    public int climbStairs(int n) {
        int res = 0;
        for (int twoStep = n / 2;twoStep >= 0;twoStep--){
            res += C(n - (2*twoStep), (n-twoStep));
        }
        return res;
    }
}
