package com.zhzm;

import java.util.*;

// 单词搜索
public class class_79 {
    public void test(){
        class_79 c79=new class_79();
//        System.out.println(c79.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}},"ABCCED"));
        System.out.println(c79.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}},"ABCB"));
        System.out.println(c79.exist2(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}},"ABCB"));

    }
    public boolean exist(char[][] board, String word) {
        Map<Character, List<Entry>> charMap=new HashMap<>();
        for (int x = 0; x < board[0].length; x++) {
            for (int y = 0; y < board.length; y++) {
                Entry e=new Entry(x,y);
                if(charMap.containsKey(board[y][x])){
                    charMap.get(board[y][x]).add(e);
                } else {
                    List<Entry> entryList = new ArrayList<>();
                    entryList.add(e);
                    charMap.put(board[y][x],entryList);
                }
            }
        }

        Collection<Entry> lastCharLocal=charMap.get(word.charAt(0));
        for (int i = 1; i < word.length(); i++) {
            if(lastCharLocal.isEmpty()){
                return false;
            } else {
                List<Entry> thisCharLocal=charMap.get(word.charAt(i));
                Set<Entry> resCharLocal=new HashSet<>();
                for(Entry e:lastCharLocal){
                    resCharLocal.addAll(e.adjacent(thisCharLocal));
                }
                lastCharLocal=resCharLocal;
            }
        }
        return !lastCharLocal.isEmpty();
    }

    public boolean exist2(char[][] board, String word) {
        for (int x = 0; x < board[0].length; x++) {
            for (int y = 0; y < board.length; y++) {
                if(word.charAt(0)==board[y][x]){
                    if(dfs(board,word,y,x,0))
                        return true;
                }
            }
        }
        return false;
    }

    public boolean dfs(char[][] board, String word, int i, int j, int k) {
//        if (k == word.length()) {
//            return true;
//        }
//        if (i < 0 || j < 0 || i >= board.length || j >= board[i].length) {
//            return false;
//        }

        if (word.charAt(k) != board[i][j]) {
            return false;
        } else if(word.length()==k+1){
            return true;
        }

        boolean res=false;
        if(i-1>=0){
            res|=dfs(board, word, i-1,j,k+1);
        }
        if(!res && j-1>=0){
            res|=dfs(board, word, i,j-1,k+1);
        }
        if(!res && i+1<board.length){
            res|=dfs(board, word, i+1,j,k+1);
        }
        if(!res && j+1<board[i].length){
            res|=dfs(board, word, i,j+1,k+1);
        }


//        char t = board[i][j];
//        board[i][j] = '0';
//        boolean res = dfs(board, word, i + 1, j, k + 1) ||
//                dfs(board, word, i - 1, j, k + 1) ||
//                dfs(board, word, i, j + 1, k + 1) ||
//                dfs(board, word, i, j - 1, k + 1);
//        board[i][j] = t;
        return res;
    }

    class Entry{
        public int x,y;

        public Entry(int x, int y){
            this.x=x;
            this.y=y;
        }

        public boolean equals(Object o){
            if(o instanceof Entry){
                Entry e=(Entry)o;
                return e.x==this.x&&e.y==this.y;
            }
            return false;
        }

        public List<Entry> adjacent(List<Entry> targets){
            List<Entry> res=new ArrayList<>();
            for(Entry e:targets){
                if(adjacent(e)){
                    res.add(e);
                }
            }
            return res;
        }

        public boolean adjacent(Entry target){
            return (target.x==this.x&&(target.y==this.y+1||target.y==this.y-1)) ||
                    (target.y==this.y&&(target.x==this.x+1||target.x==this.x-1));
        }
    }
}
