package com.zhzm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class class_1233 {
    public static void main(String[] args) {
        class_1233 class1233=new class_1233();
        print(class1233.removeSubfolders(new String[]{"/a", "/a/b", "/c/d", "/c/d/e", "/c/f"}));
    }
    private static void print(List<String> res){
        for (String s:res) {
            System.out.print(s);
            System.out.print(' ');
        }
        System.out.println();
    }

    public List<String> removeSubfolders(String[] folder) {
        Trie4 root=new Trie4();
        for (int i = 0; i < folder.length; i++) {
            String[] subPath=folder[i].split("/");
            root.addTrie(subPath,0,folder[i]);
        }

        List<String> res=new LinkedList<>();
        root.getFatherPath(res);
        return res;
    }
}
class Trie4{
    Map<String,Trie4> childs;
    boolean notEmpty;
    String fullPath;

    public Trie4(){
        childs = new HashMap<>();
        notEmpty=false;
    }

    public void addTrie(String[] s,int idx,String fullPath){
//        int res=0;
        if(idx<s.length) {
//            int local=s.charAt(idx)-97;
            if (!childs.containsKey(s[idx])) {
                childs.put(s[idx],new Trie4());
            }
//            this.cont+=cont;
            childs.get(s[idx]).addTrie(s,idx+1,fullPath);
//            this.cont-=res;
        } else {
            notEmpty=true;
            this.fullPath=fullPath;
//            res=realC;
//            this.cont+=cont;
//            if(!notEmpty){
//                this.cont-=realC;
//            }
//            realC=cont;
        }
//        return res;
    }

    public boolean hasChild(){
        return childs.size()!=0;
    }

    public void getFatherPath(List<String> paths){
        if(notEmpty){
            paths.add(fullPath);
        } else {
            for(Trie4 t: childs.values()){
                t.getFatherPath(paths);
            }
        }
    }
}