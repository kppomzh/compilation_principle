package com.zhzm;

import org.junit.Test;

public class class_3200 {

    @Test
    public void test1() {
        System.out.println(maxHeightOfTriangle(2, 3));
    }
    @Test
    public void test2() {
        System.out.println(maxHeightOfTriangle(0, 3));
    }

    public int maxHeightOfTriangle(int red, int blue) {
        if(red == 0 && blue == 0)
            return 0;
        if(red == 0 || blue == 0)
            return 1;
        int boll1 = 0, boll2 = 0;
        int maxBoll = Math.max(red, blue);
        int minBoll = Math.min(red, blue);
        int res = 0;
        boolean split = false;
        while (checkBoll(boll1, boll2, maxBoll, minBoll)) {
            res++;
            split = !split;
            if(split) {
                boll1 += res;
            } else {
                boll2 += res;
            }
        }
        return res - 1;
    }

    private boolean checkBoll(int boll1, int boll2, int maxBoll, int minBoll) {
        return Math.min(boll1, boll2) <= minBoll && Math.max(boll1, boll2) <= maxBoll;
    }
}
