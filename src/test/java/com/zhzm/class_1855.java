package com.zhzm;

import java.io.*;
import java.nio.channels.FileChannel;

public class class_1855 {
    public static void main(String[] args) throws IOException {
        class_1855 class1855=new class_1855();
        BufferedReader bf=new BufferedReader(new FileReader("./data.txt"));
        String[] as1= bf.readLine().split(","),as2=bf.readLine().split(",");
        int[] ai1=new int[as1.length],ai2=new int[as2.length];
        for (int i = 0; i < as1.length; i++) {
            ai1[i]=Integer.parseInt(as1[i]);
        }
        for (int i = 0; i < as2.length; i++) {
            ai2[i]=Integer.parseInt(as2[i]);
        }
        System.out.println(class1855.maxDistance(ai1,ai2));
        System.out.println(class1855.maxDistance(new int[]{2},new int[]{2,2,1}));
        System.out.println(class1855.maxDistance(new int[]{55,30,5,4,2},new int[]{100,20,10,10,5}));
        System.out.println(class1855.maxDistance(new int[]{2,2,2},new int[]{10,10,1}));
        System.out.println(class1855.maxDistance(new int[]{30,29,19,5},new int[]{25,25,25,25,25}));
    }

    public int maxDistance(int[] nums1, int[] nums2) {
        int res=0,i=0;
        if(nums2[0]>=nums1[nums1.length-1]) {
            for (int j = 0; j < nums2.length; j++) {
                int limit = Math.min(j, nums1.length-1);
                while (i <= limit && nums2[j] < nums1[i]) {
                    i++;
                }

                if (i <= limit)
                    res = Math.max(res, j - i);
            }
        }
        return res;
    }
}
