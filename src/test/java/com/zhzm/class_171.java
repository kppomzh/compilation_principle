package com.zhzm;

public class class_171 {
    public int titleToNumber(String columnTitle) {
        int res=0;
        for (int i = 0; i < columnTitle.length(); i++) {
            res*=26;
            res+=columnTitle.charAt(i)-64;
        }
        return res;
    }
}
