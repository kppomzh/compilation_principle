package com.zhzm;

// 2903. 找出满足差值条件的下标 I
public class class_2903 {

    public int[] findIndices(int[] nums, int indexDifference, int valueDifference) {
        int endIdx = nums.length - indexDifference;
        for (int i = 0; i < endIdx; i++) {
            for (int j = i+indexDifference; j < nums.length; j++) {
                if(Math.abs(nums[i] - nums[j]) >= valueDifference) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{-1, -1};
    }

    public int[] findIndices2(int[] nums, int indexDifference, int valueDifference) {
        int[] answer = new int[2];
        for (int i = 0; i <= nums.length - 1; i++) {
            for (int j = 0; j <= nums.length - 1; j++) {
                if (Math.abs(i - j) >= indexDifference && Math.abs(nums[i] - nums[j]) >= valueDifference) {
                    answer = new int[]{i, j};
                    break;
                } else {
                    answer = new int[]{-1, -1};
                }
            }
        }
        return answer;
    }
}
