package com.zhzm;

import com.zhzm.structure.object.MinHeap;

import java.util.Arrays;
import java.util.PriorityQueue;

public class class_3194 {
    public double minimumAverage(int[] nums) {
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length >> 1; i++) {
            heap.add(nums[i] + nums[nums.length - (1 + i)]);
        }
        return heap.peek() / 2d;
    }
}
