package com.zhzm;

import java.util.stream.IntStream;

public class class_48 {

    public void rotate(int[][] matrix) {
        int n = matrix.length;
        IntStream.range(0, n >> 1).parallel().forEach(i ->
                IntStream.range(i, n - 1 - i).forEach(j -> {
                    var temp = matrix[i][j];
                    matrix[i][j] = matrix[n - 1 - j][i];
                    matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];
                    matrix[n - 1 - i][n - 1 - j] = matrix[j][n - 1 - i];
                    matrix[j][n - 1 - i] = temp;
                }));
    }

    public void rotate1(int[][] matrix) {
        rotateOutRing(matrix, 0, matrix.length - 1);
    }

    public void rotateOutRing(int[][] matrix, int left, int right) {
        int tmpX = 0;
        while (tmpX < right - left) {
            int tmp = matrix[tmpX + left][left];
//            matrix[tmpX+left][left]=matrix[left][right-tmpX];
            matrix[tmpX + left][left] = matrix[right][tmpX + left];
//            matrix[left][right-tmpX]=matrix[right-tmpX][right];
            matrix[right][tmpX + left] = matrix[right - tmpX][right];
            matrix[right - tmpX][right] = matrix[left][right - tmpX];
            matrix[left][right - tmpX] = tmp;

            tmpX++;
        }

        left++;
        right--;
        if (left != right) {
            rotateOutRing(matrix, left, right);
        }
    }
}
