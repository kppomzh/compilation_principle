package com.zhzm;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class class_1260 {
    public static void main(String[] args) {
        class_1260 class1260=new class_1260();
        class1260.shiftGrid(new int[][]{{1,2,3,4},{4,5,6,7},{7,8,9,10}},4);
    }
    public List<List<Integer>> shiftGrid(int[][] grid, int k) {
        int length=grid.length*grid[0].length;
        Integer[] tmp=new Integer[length];

        k=k%length;
        int idx=k;
        int i=0;
        int j=0;
        for(;i<grid.length&&idx<length;i++){
            j=0;
            for(;j<grid[0].length&&idx<length;j++){
                tmp[idx]=grid[i][j];
                idx++;
            }
        }
        i--;
        idx=0;
        for(;i<grid.length;i++){
            for(;j<grid[0].length;j++){
                tmp[idx]=grid[i][j];
                idx++;
            }
            j=0;
        }
        List<List<Integer>> res=new LinkedList<>();
        for (int l = 0; l < grid.length; l++) {
            res.add(Arrays.asList(Arrays.copyOfRange(tmp,l*grid[0].length,(l+1)*grid[0].length)));
        }

        return res;
    }
}
