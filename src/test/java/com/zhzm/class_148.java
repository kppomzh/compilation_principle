package com.zhzm;

import com.zhzm.structure.object.ListNode;

public class class_148 {
    public void test(){
        class_148 c148=new class_148();
        ListNode head=new ListNode(2);
        ListNode cursor=head;

//        cursor.next=new ListNode(5);
//        cursor=cursor.next;
//        cursor.next=new ListNode(3);
//        cursor=cursor.next;
//        cursor.next=new ListNode(4);
//        cursor=cursor.next;
        cursor.next=new ListNode(0);

        ListNode res=c148.sortList(head);
        while (res!=null){
            System.out.println(res.val);
            res=res.next;
        }
    }

    public ListNode sortList(ListNode head) {
        if(head==null||head.next==null){
            return head;
        }
        int length=0;
        ListNode cursor=head;
        while(cursor!=null){
            length++;
            cursor=cursor.next;
        }

        return sortList(head,(length-1)/2);
    }
    private ListNode sortList(ListNode head,int splitLength) {
        if(head.next==null){
            return head;
        }

        ListNode cursor=head;
        for (int i = 0; i < splitLength; i++) {
            cursor=cursor.next;
        }
        ListNode secHead=cursor.next;
        cursor.next=null;

        ListNode merge1=sortList(head,(splitLength-1)/2);
        ListNode merge2=sortList(secHead,(splitLength-1)/2);
        head=new ListNode();
        cursor=head;
        while(true){
            if (merge1.val <= merge2.val) {
                cursor.next = merge1;
                merge1=merge1.next;
                cursor=cursor.next;

                if(merge1==null){
                    cursor.next=merge2;
                    break;
                }
            }else {
                cursor.next = merge2;
                merge2=merge2.next;
                cursor=cursor.next;

                if(merge2==null){
                    cursor.next=merge1;
                    break;
                }
            }
        }
        head=head.next;
        return head;
    }
}
