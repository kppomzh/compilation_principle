package com.zhzm;

// 汉明距离
public class class_461 {
    public int hammingDistance(int x, int y) {
        int k = x^y;
        int sum = 0;
        while(k>0) {
            if(k%2!=0)
                 sum++;
        }
        return sum;
    }
}
