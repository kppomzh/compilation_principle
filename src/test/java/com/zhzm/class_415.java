package com.zhzm;

import org.junit.Test;

public class class_415 {

    @Test
    public void test() {
//        System.out.println(addStrings("123", "654"));
//        System.out.println(addStrings("123", "976"));
        System.out.println(addStrings("456", "88"));
        System.out.println(addStrings("456", "66"));
        System.out.println(addStrings("456", "77"));
    }

    public String addStrings(String num1, String num2) {
        if(num1.length()<num2.length()){
            String c=num1;
            num1=num2;num2=c;
        }
        int lengthRange = num1.length() - num2.length();
        char[] res = new char[num1.length() + 1];
        int i = num1.length() - 1;
        boolean carry=false;
        for(;i >= lengthRange; i--) {
            char c = (char) ((carry?1:0)-48);
            c += num1.charAt(i);
            c += num2.charAt(i - lengthRange);
            carry = c > 57;
            res[i+1] = carry ? (char) (c - 10) : c;
        }
        for(;i >= 0; i--) {
            char c = (char) ((carry?1:0)+num1.charAt(i));
            carry = c > 57;
            res[i+1] = carry ? (char) (c - 10) : c;
        }
        if(carry) {
            res[0] = '1';
            return new String(res);
        } else {
            return new String(res, 1, num1.length());
        }
    }
}
