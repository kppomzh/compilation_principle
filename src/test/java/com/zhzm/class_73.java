package com.zhzm;

import java.util.Arrays;
import java.util.stream.IntStream;

public class class_73 {
    public void setZeroes(int[][] matrix) {
        boolean[] line = new boolean[matrix.length];
        boolean[] list = new boolean[matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0) {
                    line[i] = true;
                    list[j] = true;
                }
            }
        }
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[0].length; j++) {
//                if(line[i] || list[j]) {
//                    matrix[i][j] = 0;
//                }
//            }
//        }
        IntStream.range(0, line.length).parallel().
                filter(idx -> line[idx]).forEach(idx -> Arrays.fill(matrix[idx], 0));
        IntStream.range(0, list.length).parallel().
                filter(idx -> list[idx]).forEach(idx -> {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][idx] = 0;
            }
        });
    }
}
