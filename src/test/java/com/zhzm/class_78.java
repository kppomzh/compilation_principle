package com.zhzm;

import cn.hutool.core.lang.Assert;
import org.junit.Test;

import java.util.*;

// 子集Ⅰ
public class class_78 {
    int[] prime = new int[]{3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53};

    @Test
    public void test() {
        int maxN = 60; // 全集元素数量
        for (int n = 3; n <= maxN; n++) {
            for (int j = 0; j < prime.length; j++) {
                long mod = prime[j]; // 子集元素和的除数，满足：1.是质数；2.maxN % mod = 0
                if(n%mod!=0)
                    continue;
                int[] nums = new int[n];
                // 生成全集
                for (int i = 0; i < n; i++) {
                    nums[i] = i + 1;
                }
                // 全集元素和
                int arrayNum = Arrays.stream(nums).sum();
                // 将子集按元素和进行分类
                Map<Long, Long> map = this.countSubSets(nums);
                long modCount = 0L; // 满足 % mod为0的子集数量
                // 筛选满足 % mod为0的子集
                for (long i = 0; i <= arrayNum; i++) {
                    if (i % mod == 0) {
//                        System.out.println(String.format("%d (%d)", i, map.get(i)));
                        modCount += map.get(i);
                    }
                }
                if(modCount != modCountSum(nums.length, mod)) {
                    System.err.println(n);
                    System.err.println(mod);
                    System.err.println(modCount);
                    System.err.println(modCountSum(nums.length, mod));
                    System.out.println("========================");
                }
                Assert.equals(modCount, modCountSum(nums.length, mod));
            }

        }
    }

    // 此方法的逻辑相对于下方强行计算全部子集的方法优化了很多，大量的节约了内存
    // 主要是抽象了子类的数量，我们只需要抓住子类和作为key和满足子类和=key的集合的数量两点即可，不需要大量的重复计算每个子集当中到底有哪些元素
    // 最简单的验证方式是看输出的不同加和的子集数量是否满足正态分布
    public Map<Long, Long> countSubSets(int[] nums) {
        Map<Long, Long> res = new HashMap<>();
        res.put(0L, 1L);
        for (int num : nums) {
            List<Long> keys = List.copyOf(res.keySet()).stream().sorted(Comparator.reverseOrder()).toList();
            for (Long item : keys) {
                long nKey = item + num;
                Long subResCount = res.getOrDefault(nKey, 0L);
                res.put(nKey, res.get(item) + subResCount);
            }
        }
        return res;
    }

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(Collections.emptyList());
        for (int num : nums) {
            int inLoop = res.size();
            for (int j = 0; j < inLoop; j++) {
                List<Integer> newList = new ArrayList<>(res.get(j));
                newList.add(num);
                res.add(newList);
            }
        }
        return res;
    }

    @Test
    public void test2() {
        System.out.println(modCountSum(42,7));
    }
    @Test
    public void test3() {
        int n = 42;
        int mod = 7;
        int[] nums = new int[n];
        // 生成全集
        for (int i = 0; i < n; i++) {
            nums[i] = i + 1;
        }
        // 全集元素和
        int arrayNum = Arrays.stream(nums).sum();
        // 将子集按元素和进行分类
        Map<Long, Long> map = this.countSubSets(nums);
        long modCount = 0L; // 满足 % mod为0的子集数量
        // 筛选满足 % mod为0的子集
        for (long i = 0; i <= arrayNum; i++) {
            if (i % mod == 0) {
                modCount += map.get(i);
            }
        }
        System.out.println(modCount);
    }

    private long modCountSum(long n, long m) {
        return ((1L << n) + (m - 1L) * (1L << (n / m))) / m;
    }
}
