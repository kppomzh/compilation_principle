package com.zhzm;

import org.junit.Test;

// 生命游戏
public class class_289 {
    @Test
    public void test() {
        int[][] board = new int[][]{
                {0,1,0},
                {0,0,1},
                {1,1,1},
                {0,0,0}};
        for (int k = 0; k < 6; k++) {
            gameOfLife(board);
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    System.out.print(board[i][j]);
                }
                System.out.println();
            }
            System.out.println("===========");
        }
    }

    public void gameOfLife(int[][] board) {
        int height = board.length;
        int width = board[0].length;
        int[][] lifeKernel = new int[][]{{1,1,1}, {1,0,1}, {1,1,1}};
        int[][] upperBoard = new int[height+2][width+2];
        for (int i = 0; i < height; i++) {
            System.arraycopy(board[i], 0, upperBoard[i+1], 1, width);
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int count = countKernel(upperBoard, lifeKernel, i, j);
                switch (count) {
                    case 2:
                        break;
                    case 3:
                        board[i][j] = 1;
                        break;
                    default:
                        board[i][j] = 0;
                        break;
                }
            }
        }
    }

    private int countKernel(int[][] upperBoard, int[][] convolutionKernel, int height, int width) {
        int count = 0;
        for (int i = 0; i < convolutionKernel.length; i++) {
            for (int j = 0; j < convolutionKernel.length; j++) {
                count += upperBoard[height + i][width + j] * convolutionKernel[i][j];
            }
        }
        return count;
    }
}
