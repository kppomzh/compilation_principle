package com.zhzm;

import java.util.Stack;

class MyQueue {
    Stack<Integer> s1, s2;

    public MyQueue() {
        s1 = new Stack<>();
        s2 = new Stack<>();
    }

    public void push(int x) {
        s2.push(x);
    }

    public int pop() {
        if (s1.empty())
            while (!s2.empty()) {
                s1.push(s2.pop());
            }
        return s1.pop();
    }

    public int peek() {
        if (s1.empty())
            while (!s2.empty()) {
                s1.push(s2.pop());
            }
        return s1.peek();
    }

    public boolean empty() {
        return s1.empty() && s2.empty();
    }
}
