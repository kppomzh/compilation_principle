package com.zhzm;

import java.util.*;

public class class_768 {
    public int maxChunksToSorted(int[] arr) {
        int[] list=new int[arr.length];
        int top=0;
        list[0]=arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(arr[i]>=list[top]){
                top++;
                list[top]=arr[i];
            } else {
                int max=list[top];
                top--;
                while(top>=0&&list[top]>arr[i]){
                    top--;
                }
                top++;
                list[top]=max;
            }
        }

        return top+1;
    }

}
