package com.zhzm;

import com.zhzm.structure.number.FarleyFraction;
import com.zhzm.structure.number.Fraction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// 最简分数
public class class_1447 {
    int count;
    @Test
    public void test() {
        int n = 1024;// max = 32
        long time = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            simplifiedFractions(i);
        }
        System.out.println(System.currentTimeMillis() - time);
//        System.out.println(list.size());
    }

    public List<String> simplifiedFractionsNormal(int n) {
        if (n < 2)
            return Collections.emptyList();
        List<FarleyFraction> fractionList = new ArrayList<>();
        FarleyFraction first = new FarleyFraction(new Fraction(0,1), new Fraction(1,1));
        fractionList.add(first);
        fillFarleyListNormal(fractionList, first, n);
        return fractionList.parallelStream().map(Fraction::toString).toList();
    }

    private void fillFarleyListNormal(List<FarleyFraction> fractionList, FarleyFraction fraction, int level) {
        if(fraction == null) return;
        fillFarleyListNormal(fractionList, fraction.getLeftFarleySum(fractionList, level), level);
        fillFarleyListNormal(fractionList, fraction.getRightFarleySum(fractionList, level), level);
    }

    public List<String> simplifiedFractions(int n) {
        return fillFarleyList(new ArrayList<>(), 0, 1, 1, 1, n);
    }

    private List<String> fillFarleyList(List<String> fractionList, int lm, int ld, int rm, int rd, int level) {
        int tm = lm + rm;
        int td = ld + rd;
        if(td > level)
            return fractionList;
        fractionList.add(tm + "/" + td);
               fillFarleyList(fractionList, lm, ld, tm, td, level);
        return fillFarleyList(fractionList, tm, td, rm, rd, level);
    }
}
