package com.zhzm;

import com.zhzm.structure.object.ListNode;

public class class_92 {
    public void test(){
        ListNode node=new ListNode(),tmp=node;
        for (int i = 0; i < 2; i++) {
            tmp.next=new ListNode(i);
            tmp=tmp.next;
        }
        ListNode res=new class_92().reverseBetween(node.next,1,2);
        while(res!=null){
            System.out.println(res.val);
            res=res.next;
        }
    }

    /**
     * 限制ListNode只能使用next指针
     * @param head
     * @param left
     * @param right
     * @return
     */
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if(head.next==null || left==right)
            return head;

        ListNode start,end=head,res;
        ListNode beforeStart,afterEnd;

        res=new ListNode();
        res.next=head;

        if(left>1) {
            for (int i = 1; i < left-1; i++) {
                end = end.next;
            }
            beforeStart=end;
            end = end.next;
        } else {
            beforeStart=res;
        }
        start = end;
        for(int i=left;i<right;i++){
            end=end.next;
        }
        afterEnd=end.next;

        beforeStart.next=end;
        ListNode n1,n2,n3;
        n1=start;
        n2=n1.next;
        n3=n2.next;
        n1.next=afterEnd;
        while (n3!=afterEnd) {
            n2.next=n1;
            n1=n2;
            n2=n3;
            n3=n3.next;
        }
        n2.next=n1;

        return res.next;
    }

}
