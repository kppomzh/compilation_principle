package com.zhzm;

import java.util.ArrayList;
import java.util.List;

public class class_71 {
    public String simplifyPath(String path) {
        String[] paths = path.split("/");
        int lIndex = -1;
        List<String> res = new ArrayList<>();
        for (int i = 0; i < paths.length; i++) {
            switch (paths[i]) {
                case "":
                case ".":
                    continue;
                case "..":
                    if(!res.isEmpty()) {
                        res.remove(lIndex);
                        lIndex--;
                    }
                    break;
                default: {
                    res.add(paths[i]);
                    lIndex++;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        if(res.isEmpty())
            sb.append("/");
        else
            res.forEach(item -> {
                sb.append("/").append(item);
            });
        return sb.toString();
    }
}
