package com.zhzm;

import com.zhzm.structure.object.MultiTreeNode;

import java.util.*;

public class class_2477 {
    public long minimumFuelCost(int[][] roads, int seats) {
        Set<MultiTreeNode> noteset = new HashSet<>();
        Map<Integer, Set<Integer>> roadMap = new HashMap<>();
        long res = 0;
        for(int[] road : roads) {
            roadMap.computeIfAbsent(road[0], k->new HashSet<>()).add(road[1]);
            roadMap.computeIfAbsent(road[1], k->new HashSet<>()).add(road[0]);
        }
        MultiTreeNode root = new MultiTreeNode(0, 0);

        return 1l;
    }
}
