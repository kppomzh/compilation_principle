package com.zhzm;

import java.util.Arrays;

public class class_242 {
    public boolean isAnagram(String s, String t) {
        return Arrays.equals(getStrMap(s),getStrMap(t));
    }

    private char[] getStrMap(String str){
        char[] ca=str.toCharArray();
        Arrays.sort(ca);
        return ca;
    }
}
