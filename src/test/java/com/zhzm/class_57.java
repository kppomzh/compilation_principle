package com.zhzm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class class_57 {
    public void test(){
        int[][] res=new class_57().insert(new int[][]{{1,3},{6,9},{11,13}},new int[]{12,15});
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[i].length; j++) {
                System.out.print(res[i][j]);System.out.print(' ');
            }
            System.out.println();
        }
    }

    public int[][] insert(int[][] intervals, int[] newInterval) {
        int[][] res;
        if (intervals.length == 0) {
            return new int[][]{newInterval};
        } else if (newInterval[1] < intervals[0][0]) {
            res = new int[intervals.length + 1][];
            System.arraycopy(intervals, 0, res, 1, intervals.length);
            res[0] = newInterval;
        } else if (newInterval[0] > intervals[intervals.length - 1][1]) {
            res = new int[intervals.length + 1][];
            System.arraycopy(intervals, 0, res, 0, intervals.length);
            res[intervals.length] = newInterval;
        } else if (newInterval[1] == intervals[0][0]) {
            intervals[0][0] = newInterval[0];
            res = intervals;
        } else if (newInterval[0] == intervals[intervals.length - 1][1]) {
            intervals[intervals.length - 1][1] = newInterval[1];
            res = intervals;
        } else {
            List<int[]> lres = new ArrayList<>();
            boolean add=true;

            int i = 0;
            for (; i < intervals.length; i++) {
                int[] tmpRange=intervals[i];
                if(newInterval[0]>tmpRange[1]){
                    lres.add(intervals[i]);
                } else {
                    newInterval[0]=Math.min(newInterval[0],intervals[i][0]);
                    if(newInterval[1]<tmpRange[0]){
                        lres.add(newInterval);
                        add=false;
                        break;
                    }
                    else if(tmpRange[1]>=newInterval[1]){
                        newInterval[1]=tmpRange[1];
                        lres.add(newInterval);
                        i++;
                        add=false;
                        break;
                    }
                }
            }
            if(add){
                lres.add(newInterval);
            }

            res=new int[lres.size()+intervals.length-i][];
            for (int j = 0; j < lres.size(); j++) {
                res[j]=lres.get(j);
            }
            System.arraycopy(intervals,i,res,lres.size(),intervals.length-i);
        }
        return res;
    }
}
