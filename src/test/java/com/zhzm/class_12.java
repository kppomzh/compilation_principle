package com.zhzm;

public class class_12 {
    static char[] baseStrings = {'I', 'X', 'C', 'M'};
    static String[][] levelStrings = {{"IV", "V", "IX"}, {"XL", "L", "XC"}, {"CD", "D", "CM"}};

    public String intToRoman(int num) {
        StringBuilder result = new StringBuilder();
        for (int i = 3; i >= 0; i--) {
            int now = num / (int) Math.pow(10, i);
            num = num % (int) Math.pow(10, i);
            now = getSth(result, i, now);
            for (int j = 0; j < now; j++) {
                result.append(baseStrings[i]);
            }
        }

        return result.toString();
    }

    private int getSth(StringBuilder sb, int level, int now) {
        if (level > 2 || now < 4)
            return now;
        switch (now) {
            case 4:
                sb.append(levelStrings[level][0]);
                return 0;
            case 5:
            case 6:
            case 7:
            case 8:
                sb.append(levelStrings[level][1]);
                return now - 5;
            case 9:
                sb.append(levelStrings[level][2]);
                return 0;
            default:
                return now;
        }
    }
}
