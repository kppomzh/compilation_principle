package com.zhzm;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;

/**
 * Unit test for simple App.
 */
public class AppTest
{

    public static void main(String[] ar){
        CompilationUnit compilationUnit = new CompilationUnit();
        ClassOrInterfaceDeclaration nodeClass = compilationUnit.addClass("thisis");
        MethodDeclaration substance = nodeClass.addMethod("getSubStance", Modifier.Keyword.PUBLIC);

        ThrowStmt lastElse=new ThrowStmt();
        lastElse.setExpression("new SemanticException()");
        BinaryExpr check=new BinaryExpr(new NameExpr("MINUS"),new NameExpr("null"), BinaryExpr.Operator.NOT_EQUALS);

        Expression funcExp=new NameExpr("MathExtion.plus"+"(null,null)");
        ReturnStmt returnStmt=new ReturnStmt(funcExp);
        IfStmt ifStmt=new IfStmt(check,new BlockStmt(new NodeList<>(returnStmt)), null);
        substance.getBody().get().addStatement(ifStmt);
        substance.getBody().get().addStatement(lastElse);


        System.out.println(compilationUnit);

    }
}
