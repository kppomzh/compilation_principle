package com.zhzm;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class class_198 {
    @Test
    public void test() {
        int[] nums = {2,7,9,3,1};
        System.out.println(rob(nums));
    }

    private Map<Integer, Integer> cache;

    public int rob(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        }
        cache = new HashMap<>();
        return Math.max(rob(nums, 0), rob(nums, 1));
    }

    /*
    * 打家劫舍，正向动态规划实现，需要增加缓存以保障执行速度
    */
    private int rob(int[] nums, int start) {
        if (cache.containsKey(start)) {
            return cache.get(start);
        }
        int res = nums[start];
        if (start < nums.length - 3) {
            int res1 = rob(nums, start + 2);
            int res2 = rob(nums, start + 3);
            res += Math.max(res1, res2);
        } else if (start == nums.length - 3) {
            res += nums[start + 2];
        }
        cache.put(start, res);
        return res;
    }

    /*
     * 打家劫舍，逆向动态规划实现
     */
    public int rob1(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        } else if (nums.length >= 3) {
            nums[nums.length - 3] += nums[nums.length - 1];
        }
        for (int i = nums.length - 4; i >= 0; i--) {
            nums[i] += Math.max(nums[i + 2], nums[i + 3]);
        }
        return Math.max(nums[0], nums[1]);
    }
}
