package com.zhzm;

import java.util.HashSet;
import java.util.Set;

public class class_2299 {
    static Set<Character> set=new HashSet<>();
    static {
        set.add('!');
        set.add('@');
        set.add('#');
        set.add('$');
        set.add('%');
        set.add('^');
        set.add('&');
        set.add('*');
        set.add('(');
        set.add(')');
        set.add('-');
        set.add('+');
    }
    public boolean strongPasswordCheckerII(String password) {
        if(password.length()<8)
            return false;

        boolean hasNum=false,hasExt=false,hasUpper=false,hasLower=false;
        char last=0;
        for (int i = 0; i < password.length(); i++) {
            if(password.charAt(i)==last)
                return false;
            else if(password.charAt(i)>=48 && password.charAt(i)<=57){
                hasNum=true;
            } else if(password.charAt(i)>=65 && password.charAt(i)<=90){
                hasUpper=true;
            }else if(password.charAt(i)>=97 && password.charAt(i)<=122){
                hasLower=true;
            }else if(set.contains(password.charAt(i))){
                hasExt=true;
            }
            last=password.charAt(i);
        }
        return hasExt&&hasNum&&hasUpper&&hasLower;
    }
}
