package com.zhzm;

import java.util.*;

public class class_753 {
    public static void main(String[] ar){
        class_753 c753=new class_753();
        System.out.println(c753.crackSafe(4,3));
    }

    public String crackSafe(int n, int k) {
        int nodenum= (int) Math.pow(k,n-1);
        int[] nodes=new int[nodenum];
        int edgenum=nodenum*k;
        Arrays.fill(nodes,k-1);


        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < edgenum + (n - 1); i++) {
            sb.append('0');
        }
        for (int i = n-1, node = 0; i < edgenum + (n-1); i++) {
            nodes[node]--;
            sb.setCharAt(i, (char) (nodes[node]+48));
            node = node*k - (sb.charAt(i-(n-1))-'0')*nodenum + nodes[node] + 1;
        }
        return sb.toString();
    }

    Set<String> visited=new HashSet<>();
    int maxNodeSize;
    String res;
    public String crackSafe2(int n, int k){
        maxNodeSize = (int) Math.pow(k , n);
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append('0');
        }
        visited.add(sb.toString());
        res=sb.toString();

        dfs(res,n,k);
        return res;
    }

    public boolean dfs(String tmpRes,int n, int k){
        if(visited.size()==maxNodeSize){
            res=tmpRes;
            return true;
        }

        StringBuilder sb=new StringBuilder();
        sb.append(tmpRes);
        sb.deleteCharAt(sb.length()-1);
        for(int i = 0 ; i < k ; ++i){
            sb.append(i);
            if(!visited.contains(sb.toString())){

            }
        }
        return false;
    }
}

