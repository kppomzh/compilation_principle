package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.Stack;

public class LCP_40 {
    @Test
    public void test() {
        System.out.println(maxmiumScore(new int[]{7,6,4,6},1));
    }

    public int maxmiumScore(int[] cards, int cnt) {
        int res = 0;
        int idx = cards.length;
        Integer lastOdd = null;
        Integer lastEven = null;
        Arrays.sort(cards);
        for (int i = 0; i < cnt; i++) {
            idx--;
            int last = cards[idx];
            if(last % 2 == 0)
                lastEven = last;
            else
                lastOdd = last;
            res += last;
        }
        if(res % 2 != 0) {
            Integer upperOdd = null;
            Integer upperEven = null;
            while (idx > 0 && (upperOdd == null || upperEven == null)) {
                idx--;
                int last = cards[idx];
                if (last % 2 == 0 && upperEven == null) {
                    upperEven = last;
                } else if(last % 2 != 0 && upperOdd == null)
                    upperOdd = last;
            }
            int res1 = Integer.MIN_VALUE;
            int res2 = Integer.MIN_VALUE;
            if(upperOdd != null && lastEven != null) {
                res1 = res - lastEven + upperOdd;
            }
            if(upperEven != null && lastOdd != null) {
                res2 = res - lastOdd + upperEven;
            }
            res = res1 < 0 && res2 < 0 ? 0 : Math.max(res1, res2);
        }
        return res;
    }
}
