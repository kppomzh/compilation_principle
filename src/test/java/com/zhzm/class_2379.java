package com.zhzm;

import java.util.LinkedList;

public class class_2379 {
    public static void main(String[] args) {
        class_2379 class2379=new class_2379();
        System.out.println(class2379.minimumRecolors("WBWBBBW",2));
    }
    public int minimumRecolors(String blocks, int k) {
        int tmpB=0;
        LinkedList<Character> window=new LinkedList<>();
        for (int i = 0; i < k; i++) {
            if(blocks.charAt(i)=='B')
                tmpB++;
            window.addLast(blocks.charAt(i));
        }

        int largeB=tmpB;
        for(int i=k;i<blocks.length();i++){
            char old=window.removeFirst();
            char nc=blocks.charAt(i);
            if(old!=nc) {
                if (nc == 'B') {
                    tmpB++;
                    largeB = Math.max(tmpB, largeB);
                    if (largeB == k)
                        break;
                } else
                    tmpB--;
            }
            window.addLast(nc);
        }
        return k-largeB;
    }
}
