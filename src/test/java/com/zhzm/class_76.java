package com.zhzm;

import java.util.HashMap;
import java.util.Map;

public class class_76 {
    public static void main(String[] args) {
        class_76 class76=new class_76();
        System.out.println(class76.minWindow("ADOBECODEBANC","ABC"));
        System.out.println(class76.minWindow("ADOBECOCDCEBANvC","ABC"));
        System.out.println(class76.minWindow("A","AA"));
        System.out.println(class76.minWindow("ab","A"));
        System.out.println(class76.minWindow("ab","Ab"));
        System.out.println(class76.minWindow("abc","cba"));
        System.out.println(class76.minWindow("bba","ba"));
        System.out.println(class76.minWindow("abcabdebac","cda"));
        System.out.println(class76.minWindow("babcaacabcabbbca","aaabb"));
    }

    Map<Integer,Integer> tmap;
    Map<Integer,Integer> smap;
    public String minWindow(String s, String t) {
        if(s.length()<t.length())
            return "";

        tmap=new HashMap<>();
        smap=new HashMap<>();
        for (int i = 0; i < t.length(); i++) {
            int c=t.charAt(i);
            tmap.put(c,tmap.getOrDefault(c,0)+1);
        }

        int start=-1,end=-1,length,notEqualNum=tmap.size();
        int i = 0;
        int rstart,rend;
        for (; i < s.length(); i++) {
            int c = s.charAt(i);
            if (tmap.containsKey(c)) {
                start = i;
                break;
            }
        }
        for (; i < s.length(); i++) {
            int c = s.charAt(i);
            if (tmap.containsKey(c)) {
                smap.put(c, smap.getOrDefault(c, 0) + 1);
                if (smap.get(c).equals(tmap.get(c))) {
                    notEqualNum--;
                }
            }

            if (notEqualNum == 0) {
                end = i;
                i++;
                break;
            }
        }

        if(end>=0) {
            start=checkStart(s,start);
            rstart=start;
            rend=end;
            length = rend - rstart + 1;
            for (; i < s.length(); i++) {
                int c = s.charAt(i);
                if(tmap.containsKey(c)) {
                    if (c == s.charAt(start)) {
                        int ts = start + 1;
                        while (true) {
                            while (!tmap.containsKey((int) s.charAt(ts))) {
                                ts++;
                            }
                            if (smap.get((int) s.charAt(ts)) <= tmap.get((int) s.charAt(ts))) {
                                break;
                            }
                            smap.put((int) s.charAt(ts), smap.get((int) s.charAt(ts)) - 1);
                            ts++;
                        }
                        start = ts;
                        end = i;
                        if (end - start + 1 < length) {
                            rstart = start;
                            rend = end;
                            length = end - start + 1;
                        }
                    } else
                        smap.put(c, smap.get(c) + 1);
                }
            }

            return s.substring(rstart,rend+1);
        }
        else
            return "";
    }

    private int checkStart(String s,int rstart){
        while (!smap.containsKey((int) s.charAt(rstart)) || smap.get((int) s.charAt(rstart)) > tmap.get((int) s.charAt(rstart))) {
            if(smap.containsKey((int) s.charAt(rstart)))
                smap.put((int) s.charAt(rstart), smap.get((int) s.charAt(rstart)) - 1);
            rstart++;
        }
        return rstart;
    }
}
