package com.zhzm;

// 2391. 收集垃圾的最少总时间
public class class_2391 {
    public int garbageCollection(String[] garbage, int[] travel) {
        countTravelTimeList(travel);
        boolean endG = true;
        boolean endM = true;
        boolean endP = true;
        int res = 0;
        for(int i = garbage.length - 1; i > 0; i--) {
            if(endM && garbage[i].indexOf('M') != -1) {
                endM = false;
                res += travel[i-1];
            }
            if(endG && garbage[i].indexOf('G') != -1) {
                endG = false;
                res += travel[i-1];
            }
            if(endP && garbage[i].indexOf('P') != -1) {
                endP = false;
                res += travel[i-1];
            }
            res += garbage[i].length();
        }
        res += garbage[0].length();
        return res;
    }
    private void countTravelTimeList(int[] travel) {
        int sum = 0;
        for (int i = 0; i < travel.length; i++) {
            sum+=travel[i];
            travel[i] = sum;
        }
    }

    public int garbageCollection1(String[] garbage, int[] travel) {
        int ans = 0;
        // 收集垃圾用时
        for (String g : garbage) {
            ans += g.length();
        }
        // 跑1次全程的时间
        int length = 0;
        for (int t : travel) {
            length += t;
        }
        // 跑3次全程的时间
        ans += length<<1 + length;

        // 针对三种垃圾
        for (char c : new char[]{'M', 'P', 'G'}) {
            // 从后往前遍历
            for (int i = garbage.length - 1; i > 0; i--) {
                if(garbage[i].indexOf(c) < 0)// indexOf返回第一次出现的索引 如果不存在 返回 -1
                    ans -= travel[i - 1];// 没有垃圾c 多跑了
                else
                    break;
            }
        }
        return ans;
    }

}
