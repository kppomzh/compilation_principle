package com.zhzm;

import java.util.*;

public class class_1798 {
    public static void main(String[] args) {
        class_1798 class1798=new class_1798();
        System.out.println(class1798.getMaximumConsecutive(new int[]{1,1,1,4}));
        System.out.println(class1798.getMaximumConsecutive(new int[]{1,4,10,3,1}));
    }
    public int getMaximumConsecutive(int[] coins) {
        int res=1;

        Arrays.sort(coins);
        for (int i = 0; i < coins.length; i++) {
            if(coins[i]<=res)
                res+=coins[i];
            else
                break;
        }

        return res;
    }
}
