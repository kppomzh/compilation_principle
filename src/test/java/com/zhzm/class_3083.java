package com.zhzm;

import org.junit.Test;

import java.util.stream.IntStream;

public class class_3083 {
    @Test
    public void test() {
        System.out.println(isSubstringPresent("leetcode"));
    }
    public boolean isSubstringPresent(String s) {
        return IntStream.range(0, s.length()-1).
                mapToObj(idx -> s.contains(new String(new char[]{s.charAt(idx + 1), s.charAt(idx)}))).
                anyMatch(p -> p);
    }
}
