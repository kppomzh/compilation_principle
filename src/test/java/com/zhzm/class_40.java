package com.zhzm;

import java.util.*;

public class class_40 {
    public static void main(String[] args) {
        class_40 class40=new class_40();
        class40.combinationSum2(new int[]{10,1,2,7,6,1,5},8);
        class40.combinationSum2(new int[]{2,1,5,2,2,2},5);
    }

    private LinkedList<Integer> tmpRes=new LinkedList<>();
    private int[] candidates;
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> res=new ArrayList<>();
        Arrays.sort(candidates);
        this.candidates=candidates;
        recursion(res,target,0);
        return res;
    }

    public void recursion(List<List<Integer>> res, int target,int begin){
        for (int i = begin; i < candidates.length; i++) {
            if (target < candidates[i]) {
                break;
            }
            if (i <= begin || candidates[i] != candidates[i - 1]) {
                tmpRes.addLast(candidates[i]);
                if(target==candidates[i])
                    res.add(List.copyOf(tmpRes));
                else
                    recursion(res, target - candidates[i], i + 1);
                tmpRes.removeLast();
            }
        }
    }
}
