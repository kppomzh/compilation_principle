package com.zhzm;

import org.junit.Test;

public class class_3175 {
    @Test
    public void test() {
        int res = this.findWinningPlayer(new int[]{4,2,6,3,9}, 2);
        System.out.println(res);
    }
    @Test
    public void test1() {
        int res = this.findWinningPlayer(new int[]{4,2,6,3,9}, 5);
        System.out.println(res);
    }
    @Test
    public void test2() {
        int res = this.findWinningPlayer(new int[]{19,3,6,16,10,9,2}, 10);
        System.out.println(res);
    }

    public int findWinningPlayer(int[] skills, int k) {
//        if(k >= skills.length) {
//            k = skills.length - 1;
//        }

        boolean find = false;
        int res = skills[0] > skills[1] ? 0 : 1;
        int idx = 1;
        while (!find && idx < skills.length) {
            int beginIdx = idx + 1;
            int endIdx = Math.min(idx + k - 1, skills.length - 1);
            find = true;
            for (int i = beginIdx; i <= endIdx; i++) {
                if(skills[i] > skills[res]) {
                    find = false;
                    idx = i;
                    res = i;
                    break;
                }
            }
        }
        return res;
    }
}
