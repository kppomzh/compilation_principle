package com.zhzm;

import java.util.*;

public class class_1807 {
    public static void main(String[] args) {
        class_1807 class1807=new class_1807();
        System.out.println(class1807.evaluate("(name)is(age)yearsold", Arrays.asList(new List[]{Arrays.asList("name","bob"),Arrays.asList("age","two")})));
        System.out.println(class1807.evaluate("(name)isyearsold(age)", Arrays.asList(new List[]{Arrays.asList("name","bob"),Arrays.asList("age","two")})));
    }

    public String evaluate(String s, List<List<String>> knowledge) {
        Map<String,String> map=new HashMap<>();
        for(List<String> entry:knowledge){
            map.put(entry.get(0),entry.get(1));
        }

        int left=-1,right=0,end=0;
        StringBuilder sb=new StringBuilder();

        for (int i = s.length()-1; i >= 0; i--) {
            if(s.charAt(i)==')'){
                end=i+1;
                break;
            }
        }
        for(int i=0;i<end;i++){
            if(s.charAt(i)=='('){
                sb.append(s, right, i);
                left=i;
            } else if(s.charAt(i)==')'){
                sb.append(map.getOrDefault(s.substring(left+1,i),"?"));
                right=i+1;
            }
        }
        return sb.append(s.substring(end)).toString();
    }
}
