package com.zhzm;

import java.util.*;
import java.util.stream.IntStream;

public class class_3238 {
    public int winningPlayerCount(int n, int[][] pick) {
        boolean[] res = new boolean[n];
        int[][] userMap = new int[n][11];
        for (int i = 0; i < pick.length; i++) {
            int user = pick[i][0];
            int boll = pick[i][1];
            userMap[user][boll]++;
            if (userMap[user][boll] > user)
                res[user] = true;
        }
        int count = 0;
        for (int i = 0; i < res.length; i++) {
            count += res[i] ? 1 : 0;
        }
        return count;
    }
}
