package com.zhzm;

import org.junit.Test;

import java.util.*;

public class class_18 {
    @Test
    public void test() {
        List<List<Integer>> res = new class_18().fourSum(new int[]{1, 0, -1, 0, -2, 2}, 0);
        for (int i = 0; i < res.size(); i++) {
            for (int j = 0; j < res.get(i).size(); j++) {
                System.out.print(res.get(i).get(j));
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    public List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<>();
        HashMap<Integer, Integer[]> countLocalMap = new HashMap<>();

        for (int i = 0; i < nums.length - 2; i++) {
            for (int j = i + 1; j < nums.length - 1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    int halfsum = target - (nums[i] + nums[j] + nums[k]);
                    Integer[] index = new Integer[]{i, j, k};
                    countLocalMap.put(halfsum, index);
                }

            }
        }

        for (int i = 0; i < nums.length; i++) {
            int halfsum = nums[i];
            if (countLocalMap.containsKey(halfsum)) {
                Integer[] array = countLocalMap.get(halfsum);

                if (i < array[0] || i > array[2] || (i != array[0] && i != array[1] && i != array[2])) {
                    Integer[] partRes = new Integer[4];
                    partRes[0] = nums[array[0]];
                    partRes[1] = nums[array[1]];
                    partRes[2] = nums[array[2]];
                    partRes[3] = nums[i];
                    res.add(Arrays.asList(partRes));
                }
            }
        }

//        for(int halfsum: countLocalMap.keySet()){
//            int othhalf=target-halfsum;
//            if(countLocalMap.containsKey(othhalf)){
//                List<Integer[]> list=countLocalMap.get(othhalf);
//                if(othhalf==halfsum ){
//                    if(list.size()>1){
//                        for (int i = 0; i < list.size() - 1; i++) {
//                            for (int j = i+1; j < list.size(); j++) {
//                                Integer[] partRes=new Integer[4];
//                                partRes[0]=nums[list.get(i)[0]];
//                                partRes[1]=nums[list.get(i)[1]];
//                                partRes[2]=nums[list.get(j)[0]];
//                                partRes[3]=nums[list.get(j)[1]];
//                                if(!set.contains(partRes)){
//                                    set.added(partRes);
//                                    res.added(Arrays.asList(partRes));
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    List<Integer[]> list2=countLocalMap.get(halfsum);
//                    for (int i = 0; i < list.size(); i++) {
//                        for (int j = 0; j < list2.size(); j++) {
//                            Integer[] partRes=new Integer[4];
//                            partRes[0]=nums[list.get(i)[0]];
//                            partRes[1]=nums[list.get(i)[1]];
//                            partRes[2]=nums[list2.get(j)[0]];
//                            partRes[3]=nums[list2.get(j)[1]];
//                            if(!set.contains(partRes)){
//                                set.added(partRes);
//                                res.added(Arrays.asList(partRes));
//                            }
//                        }
//                    }
//                }
//            }
//        }

        return res;
    }
}
