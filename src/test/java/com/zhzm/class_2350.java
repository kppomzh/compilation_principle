package com.zhzm;

import java.util.HashMap;

public class class_2350 {
    public static void main(String[] args) {
        class_2350 class2350=new class_2350();
        System.out.println(class2350.shortestSequence(new int[]{4,2,1,2,3,3,2,4,1},4));
        System.out.println(class2350.shortestSequence(new int[]{1,1,3,2,2,2,3,3},4));
        System.out.println(class2350.shortestSequence(new int[]{1,1,2,2},2));
    }
    public int shortestSequence(int[] rolls, int k) {
        Trie3 root=new Trie3(k);
        for (int i = 0; i < rolls.length; i++) {
            root.addTrie(rolls[i]);
        }

        return root.getUnFindLength();
    }
}

class Trie3{
    HashMap<Integer,Trie3> childs;
    int size,deepth;
    int unFindLength,unFind;

    public Trie3(int size){
        this(size,size);
    }
    private Trie3(int size,int deepth){
        this.size = size;
        this.deepth=deepth;
        childs=new HashMap<>((int) (size*1.25)+1);
//        childs=new Trie3[size];
        unFindLength=1;
        unFind = size;
    }

    public int addTrie(int i){
        int res=unFind!=0?1:Integer.MAX_VALUE;
        if(deepth>1){
            for (Trie3 t:childs.values()) {
                res=Math.min(res,t.addTrie(i)+1);
            }
        }
        if(!childs.containsKey(i)){
            childs.put(i,new Trie3(size,deepth-1));
            unFind--;
            if(unFind==0){
                res=2;
            }
        }
        unFindLength=res;
        return res;
    }

    public int getUnFindLength(){
//        int res=Integer.MAX_VALUE;
//
//        if(childs.size()<size){
//            res=1;
//        } else {
//            for (Trie3 t:childs.values()){
//                res=Math.min(res,t.getUnFindLength()+1);
//            }
//        }
//
//        return res;
        return unFindLength;
    }
}
