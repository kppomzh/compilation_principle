package com.zhzm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class class_2461 {
    public static void main(String[] args) {
        class_2461 class2461=new class_2461();
        int[] nums=new int[100000];
        for (int i = 0; i < 100000; i++) {
            nums[i]=i+1;
        }

        System.out.println(class2461.maximumSubarraySum(nums,100000));
    }

    public long maximumSubarraySum(int[] nums, int k){
        long res=0;
        long tmpres=0;

        int mapLeength=0;
        for (int i = 0; i < nums.length; i++) {
            mapLeength=Math.max(mapLeength,nums[i]);
        }

        int[] map=new int[mapLeength+1];
        int mapSize=0;
        int left=0,right=k-1;

        for(int i=0;i<k;i++){
            if(map[nums[i]]==0)
                mapSize++;
            map[nums[i]]++;
            tmpres+=nums[i];
        }
        if(mapSize==k)
            res=tmpres;

        for (int i=k;i<nums.length;i++){
            tmpres-=nums[left];
            if(map[nums[left]]==1)
                mapSize--;
            map[nums[left]]--;
            left++;

            right++;
            tmpres+=nums[right];
            if(map[nums[right]]==0)
                mapSize++;
            map[nums[right]]++;

            if(mapSize==k)
                res=Math.max(res,tmpres);
        }

        return res;
    }

    public long maximumSubarraySum2(int[] nums, int k) {
        long res=0;
        long tmpres=0;

        LinkedList<Integer> coll=new LinkedList<>();
        Map<Integer,Integer> map=new HashMap<>();
        for(int i=0;i<k;i++){
            coll.addLast(nums[i]);
            map.put(nums[i],map.getOrDefault(nums[i],0)+1);
            tmpres+=nums[i];
        }
        if(map.size()==k)
            res=tmpres;

        for(int i=k;i<nums.length;i++){
            int toRemove=coll.removeFirst();
            tmpres-=toRemove;
            if(map.get(toRemove)==1)
                map.remove(toRemove);
            else
                map.put(toRemove,map.get(toRemove)-1);

            coll.addLast(nums[i]);
            map.put(nums[i],map.getOrDefault(nums[i],0)+1);
            tmpres+=nums[i];
            if(map.size()==k)
                res=Math.max(res,tmpres);
        }

        return res;
    }
}
