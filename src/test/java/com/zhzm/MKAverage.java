package com.zhzm;

import java.util.Arrays;
import java.util.LinkedList;

public class MKAverage {
    private int size,maxsize;
    private int start, end,div;
    private int[] maxOrder;
    private LinkedList<Integer> addOrder;
    private double sum=0d;

    public MKAverage(int m, int k) {
        maxOrder=new int[m];
        maxsize=m;
        size=0;
        start =k-1;
        end =maxsize-k;
        div= end -k;
        addOrder=new LinkedList<>();
    }

    public void addElement(int num) {
        int left=0,right=size-1;
        int rl=0,al=0;
        if(size>=maxsize) {
            int toRemove = addOrder.removeFirst();

            while(left<=right) {
                int middle=(left+right)/2;
                if(maxOrder[middle]>toRemove){
                    right=middle-1;
                } else if(maxOrder[middle]<toRemove){
                    left=middle+1;
                } else {
                    rl=middle;
                    break;
                }
            }

            left=0;right=size-1;

            if(num<=maxOrder[left]){
                al=left;
            } else if(right>=0&&num>=maxOrder[right]){
                al=right;
            } else {
                while (left <= right) {
                    int middle = (left + right) / 2;
                    if (maxOrder[middle] > num) {
                        right = middle - 1;
                        if(num >= maxOrder[right]) {
                            al = middle>rl?right:middle;
                            break;
                        }
                    } else if (maxOrder[middle] < num) {
                        left = middle + 1;
                        if (maxOrder[left] >= num) {
                            al = middle<rl?left:middle;
                            break;
                        }
                    } else {
                        al=middle;
                        break;
                    }
                }
            }

            if(rl<=start) {
                if(al>=end){
                    sum+=maxOrder[end];
                    sum-=maxOrder[start+1];
                } else if(al>start) {
                    sum+=num;
                    sum-=maxOrder[start+1];
                }
            } else if(rl>=end) {
                if(al<=start) {
                    sum+=maxOrder[start];
                    sum-=maxOrder[end-1];
                } else if(al<end){
                    sum+=num;
                    sum-=maxOrder[end-1];
                }
            } else {
                if(al<=start){
                    sum+=maxOrder[start];
                } else if(al>=end){
                    sum+=maxOrder[end];
                } else {
                    sum+=num;
                }
                sum-=maxOrder[rl];
            }

            if(rl<al){
                System.arraycopy(maxOrder,rl+1,maxOrder,rl,al-rl);
            } else if(rl>al){
                System.arraycopy(maxOrder,al,maxOrder,al+1,rl-al);
            }
            maxOrder[al]=num;
        } else {
            maxOrder[size]=num;
            size++;
            if(size==maxsize) {
                Arrays.sort(maxOrder);
                for (int i = this.start+1; i < this.end; i++) {
                    sum += maxOrder[i];
                }
            }
        }

        addOrder.addLast(num);
    }

    public int calculateMKAverage() {
        return size>=maxsize? (int)sum/div:-1;
    }
}
