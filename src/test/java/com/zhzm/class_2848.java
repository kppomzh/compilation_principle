package com.zhzm;

import org.junit.Test;

import java.util.*;

public class class_2848 {
    @Test
    public void test() {
        List<List<Integer>> nums = new ArrayList<>();
        nums.add(Arrays.asList(4,4));
        nums.add(Arrays.asList(9,10));
        nums.add(Arrays.asList(9,10));
        nums.add(Arrays.asList(3,8));
        System.out.println(this.numberOfPoints(nums)==8);
    }
    @Test
    public void test1() {
        List<List<Integer>> nums = new ArrayList<>();
        nums.add(Arrays.asList(3,6));
        nums.add(Arrays.asList(1,5));
        nums.add(Arrays.asList(4,7));
        System.out.println(this.numberOfPoints(nums)==7);
    }
    @Test
    public void test2() {
        List<List<Integer>> nums = new ArrayList<>();
        nums.add(Arrays.asList(1,9));
        nums.add(Arrays.asList(2,10));
        nums.add(Arrays.asList(5,8));
        nums.add(Arrays.asList(1,3));
        System.out.println(this.numberOfPoints(nums)==10);
    }
    public int numberOfPoints(List<List<Integer>> nums) {
        Set<Integer> foreignKeys = new HashSet<>();
        Map<Integer, Integer> line = new HashMap<>();
        for (List<Integer> list : nums) {
            int left = list.get(0);
            int right = list.get(1);
            List<Integer> garb = new ArrayList<>();
            for (Integer key : foreignKeys) {
                if(key > right || key + line.get(key) < left)
                    continue;
                else if (key <= left && key + line.get(key) >= right) {
                    left = key;
                    right = key + line.get(key);
                } else if (key <= left) {
                    left = key;
                } else if(key + line.get(key) >= right){
                    right = key + line.get(key);
                    garb.add(key);
                } else
                    garb.add(key);
            }
            for (Integer index : garb) {
                foreignKeys.remove(index);
                line.remove(index);
            }
            foreignKeys.add(left);
            line.put(left, right - left);
        }
        return line.values().parallelStream().mapToInt(i->i+1).sum();
    }
}
