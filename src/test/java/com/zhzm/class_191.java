package com.zhzm;

import org.junit.Test;

// 汉明重量
public class class_191 {

    @Test
    public void test() {
        System.out.println(hammingWeight(11));
        System.out.println(hammingWeight(21));
        System.out.println(hammingWeight(41));
        System.out.println(hammingWeight(61));
        System.out.println(hammingWeight(128));
        System.out.println(hammingWeight(255));
    }

    public int hammingWeight(int n) {
        int sum = 0;
        int tow = 1;
        int tempN = n;
        for (int i = 0; i <= 31 && tempN!=0; i++) {
            if((tempN & tow) != 0)
                sum++;
            tow<<=1;
        }
        return sum;
    }
}
