package com.zhzm;

import com.zhzm.structure.object.Node;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_133 {
    public Node cloneGraph(Node node) {
        if(node == null)
            return null;
        Map<Node, Node> nodeMap = new HashMap<>();
        List<Node> next = List.of(node);

        while (!next.isEmpty()) {
            next = next.stream().
                    peek(tn -> nodeMap.put(tn, new Node(tn.val))).
                    flatMap(tn -> tn.neighbors.stream()).distinct().
                    filter(tnn -> !nodeMap.containsKey(tnn)).toList();
        }
        nodeMap.forEach((k, v) -> k.neighbors.stream().map(nodeMap::get).forEach(v.neighbors::add));
        return nodeMap.get(node);
    }
}
