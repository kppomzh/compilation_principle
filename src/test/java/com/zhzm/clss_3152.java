package com.zhzm;

import org.junit.Test;

import java.util.*;

public class clss_3152 {
    @Test
    public void test1() {
        boolean[] res = isArraySpecial(new int[]{2,3,1,6}, new int[][]{{0,1},{2,3}});
        System.out.println(Arrays.toString(res));
    }
    @Test
    public void test2() {
        boolean[] res = isArraySpecial(new int[]{2,3,2}, new int[][]{{0,0},{1,2}});
        System.out.println(Arrays.toString(res));
    }

    public boolean[] isArraySpecial(int[] nums, int[][] queries) {
        boolean[] res = new boolean[queries.length];
        boolean lastsD = nums[0] % 2 == 0;
        Stack<int[]> queryRanges = new Stack<>();
        queryRanges.push(new int[]{0, -1});

        for (int i = 1; i < nums.length; i++) {
            if(nums[i] % 2 == 0 != lastsD) {
                lastsD = !lastsD;
            } else {
                queryRanges.peek()[1] = i - 1;
                queryRanges.push(new int[]{i, -1});
            }
        }
        queryRanges.peek()[1] = nums.length - 1;
        for (int i = 0; i < queries.length; i++) {
            int[] query = queries[i];
            if (query[0] == query[1]) {
                res[i] = true;
                continue;
            }
            for (int[] range : queryRanges) {
                if (query[0] > range[1]) {
                    continue;
                }
                if (range[0] > query[1]) {
                    continue;
                }
                if (query[0] >= range[0] && query[1] <= range[1]) {
                    res[i] = true;
                    break;
                }
            }
        }
        return res;
    }
}
