package com.zhzm;

import org.junit.Test;

public class class_5 {
    @Test
    public void test() {
        System.out.println(longestPalindrome("ah"));
        System.out.println(longestPalindrome("hhhh"));
        System.out.println(longestPalindrome("dhh"));
        System.out.println(longestPalindrome("hhha"));
    }

    public String longestPalindrome(String s) {
        String res;
        if(s.length()==1)
            return s;
        else if(s.charAt(0)==s.charAt(1)){
            res=s.substring(0,2);
        } else {
            res=Character.toString(s.charAt(0));
        }
        int i;
        for (i = 1; i < s.length() - 1; i++) {
            String tmp=growChar(s,i);
            if(tmp.length()>res.length()){
                res=tmp;
            }
        }
//        if(s.charAt(i-1)==s.charAt(i) && res.length()<2){
//            res=s.substring(i-1,i+1);
//        }

        return res;
    }

    private boolean checkStr(String s, int start, int end) {
        while (end > start) {
            if (s.charAt(start) != s.charAt(end)) {
                return false;
            } else {
                start++;
                end--;
            }
        }
        return true;
    }

    private String growChar(String s, int local){
        String res="";
        if(s.charAt(local - 1) == s.charAt(local + 1)){
            res=growCoreStr(s,local-1,local+1);
        }
        if(s.charAt(local)==s.charAt(local - 1)){
            String tmp=growCoreStr(s,local-1,local+2);
            res=res.length()>tmp.length()?res:tmp;
        }
        return res;
    }

    private String growCoreStr(String s, int start, int end) {
        if (start - 1 < 0 || end + 1 >= s.length()) {
            return s.substring(start, Math.min(s.length(),end+1));
        } else if (s.charAt(start - 1) == s.charAt(end + 1)) {
            return growCoreStr(s, start - 1, end + 1);
        } else {
            return s.substring(start, end+1);
        }
    }
}
