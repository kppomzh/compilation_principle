package com.zhzm;

import java.util.Stack;

public class class_150 {
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String s : tokens) {
            switch (s) {
                case "+": {
                    int i = stack.pop() + stack.pop();
                    stack.push(i);
                    break;
                }
                case "-": {
                    int div = stack.pop();
                    int i =  stack.pop() - div;
                    stack.push(i);
                    break;
                }
                case "*": {
                    int i = stack.pop() * stack.pop();
                    stack.push(i);
                    break;
                }
                case "/": {
                    int sub = stack.pop();
                    int i =  stack.pop() / sub;
                    stack.push(i);
                    break;
                }
                default:
                    stack.push(Integer.valueOf(s));
                    break;
            }
        }
        return stack.pop();
    }
}
