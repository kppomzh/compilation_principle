package com.zhzm;

import com.zhzm.structure.object.ListNode;
import org.junit.Test;

public class class_86 {

    @Test
    public void test() {
        ListNode head = new ListNode(1, new ListNode(4, new ListNode(3, new ListNode(2, new ListNode(5, new ListNode(2))))));
//        ListNode head = new ListNode(1, new ListNode(4, new ListNode(3)));
        ListNode n = partition(head, 4);
        System.out.println(n);
//        while(n != null) {
//            n = n.next;
//        }
    }

    public ListNode partition(ListNode head, int x) {
        ListNode fronthead = new ListNode();
        ListNode lasthead = new ListNode();
        ListNode front = fronthead;
        ListNode last = lasthead;
        while(head != null) {
            ListNode temp = head;
            head = head.next;
            temp.next = null;
            if(temp.val >= x) {
                last.next = temp;
                last = last.next;
            } else {
                front.next = temp;
                front = front.next;
            }
        }
        front.next = lasthead.next;
        return fronthead.next;
    }
}
