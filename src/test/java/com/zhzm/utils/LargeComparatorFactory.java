package com.zhzm.utils;

import java.util.Comparator;

public class LargeComparatorFactory {
    public static <T> Comparator getComparator(T clazz){
        if (Integer.class.equals(clazz)) {
            return new intComparator();
        } else if (Double.class.equals(clazz)) {
            return new doubleComparator();
        } else if (Long.class.equals(clazz)) {
            return new longComparator();
        } else if (Float.class.equals(clazz)) {
            return new floatComparator();
        }
        return new objComparator();
    }

    private static class intComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            if(o2==o1){
                return 0;
            } else
                return o2-o1>0?1:-1;
        }
    }

    private static class doubleComparator implements Comparator<Double> {
        @Override
        public int compare(Double o1, Double o2) {
            if(o2 == o1){
                return 0;
            } else
                return o2-o1>0?1:-1;
        }
    }

    private static class floatComparator implements Comparator<Float> {
        @Override
        public int compare(Float o1, Float o2) {
            if(o2 == o1){
                return 0;
            } else
                return o2-o1>0?1:-1;
        }
    }

    private static class longComparator implements Comparator<Long> {
        @Override
        public int compare(Long o1, Long o2) {
            if(o2 == o1){
                return 0;
            } else
                return o2-o1>0?1:-1;
        }
    }

    private static class objComparator implements Comparator<Object> {
        @Override
        public int compare(Object o1, Object o2) {
            return o2.hashCode()-o1.hashCode();
        }
    }
}
