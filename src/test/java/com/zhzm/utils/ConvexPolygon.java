package com.zhzm.utils;

import com.zhzm.structure.geometry.Point;
import com.zhzm.structure.geometry.Vector;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

public class ConvexPolygon {

    @Test
    public void test() {
        int[] xI = {12, 19, -15, -4, 12, 7, -1, -6, 12, -4};
        int[] yI = {84, 10, 49, 79, 50, 74, 75, 98, 21, 20};
        Random random = new Random();
        List<Point> points = IntStream.range(0, xI.length).boxed().
                map(i -> new Point(xI[i], yI[i])).toList();

        points.forEach(System.out::println);
        System.out.println("------------------");
        getConvexPointColl(points).forEach(System.out::println);
    }

    public static List<Point> getConvexPointColl(List<Point> points) {
        List<Point> list = points.stream().sorted((Point p1, Point p2) -> {
            if(p2.x != p1.x)
                return p2.x - p1.x;
            else
                return p2.y - p1.y;
        }).toList();
        Point maxX = list.getFirst();
        List<Point> cache = new ArrayList<>();
        List<Point> convexs = new ArrayList<>();
        Stack<Point> upEdges = getHalfEdgePoints(list, cache);

        if(!cache.isEmpty()) {
            cache.add(upEdges.pop());
            cache.add(maxX);
            list = cache.stream().sorted((Point p1, Point p2) -> {
                if(p2.x != p1.x)
                    return p1.x - p2.x;
                else
                    return p1.y - p2.y;
            }).toList();
            cache.clear();
            Stack<Point> downEdges = getHalfEdgePoints(list, cache);
            downEdges.pop();
            convexs.addAll(downEdges);
        }

        convexs.addAll(upEdges);
        return convexs;
    }

    private static Stack<Point> getHalfEdgePoints(List<Point> heap, List<Point> cache) {
        Stack<Point> convexs = new Stack<>();
        convexs.push(heap.get(0));
        convexs.push(heap.get(1));

        for (int i = 2; i < heap.size(); i++) {
            Point top = heap.get(i);
            Point middle = convexs.pop();
//            if(top.x == middle.x) {
//                convexs.push(middle);
//                cache.add(top);
//                continue;
//            }
            Point start = convexs.peek();
            Vector next = Vector.getVector(middle, top);
            Vector before = Vector.getVector(start, middle);
            if (before.getCrossProductInt(next) < 0) {
                if (convexs.size() < 2) {
                    convexs.push(top);
                } else {
                    i--;
                }
                cache.add(middle);
            } else {
                convexs.push(middle);
                convexs.push(top);
            }
        }
        return convexs;
    }
}
