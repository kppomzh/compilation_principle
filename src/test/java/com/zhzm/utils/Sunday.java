package com.zhzm.utils;

public class Sunday {
    public static int search(String text, String pattern) {
        if (pattern.isEmpty() || text.isEmpty()) {
            return -1;
        }

        int[] skip = new int[256];
        for (int i = 0; i < skip.length; i++) {
            skip[i] = pattern.length(); // 如果字符不在模式串中，则偏移整个模式串的长度
        }
        for (int i = 0; i < pattern.length() - 1; i++) { // 最后一个字符不需要设置偏移
            skip[pattern.charAt(i)] = pattern.length() - 1 - i;
        }

        int idx = 0;
        while (idx <= text.length() - pattern.length()) {
            int j = 0;
            for (; j < pattern.length(); j++) {
                if (text.charAt(idx + j) != pattern.charAt(j)) {
                    break;
                }
            }
            if (j == pattern.length()) { // 找到模式串
                return idx;
            } else {
                idx += skip[text.charAt(idx + pattern.length())]; // 根据偏移表移动
            }
        }
        return -1; // 没有找到模式串
    }

    public static void main(String[] args) {
        String text = "HERE IS SIMPLE TEXT";
        String pattern = "TEXT";
        int result = search(text, pattern);
        System.out.println("Pattern found at index: " + result);
    }
}
