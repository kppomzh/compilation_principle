package com.zhzm;

public class class_2303 {
    public double calculateTax(int[][] brackets, int income) {
        int lastLimit=0;
        double res=0d;
        for (int i = 0; i < brackets.length; i++) {
            if(income>=brackets[i][0]){
                res+=(brackets[i][0]-lastLimit)/100d*brackets[i][1];
                lastLimit=brackets[i][0];
            } else {
                res+=(income-lastLimit)/100d*brackets[i][1];
                break;
            }
        }
        return res;
    }

    public double calculateTax2(int[][] brackets, int income) {
        int lastUpper = 0;
        double tax = 0;
        for (int[] i : brackets) {
            if (i[0] >= income) {
                tax += (income - lastUpper) * i[1] * 0.01;
            } else {
                int upper = i[0];
                tax += (upper - lastUpper) * i[1] * 0.01;
                lastUpper = upper;
            }
        }
        return tax;
    }
}
