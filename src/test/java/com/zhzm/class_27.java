package com.zhzm;

public class class_27 {
    public int removeElement(int[] nums, int val) {
        int sum = 0;
        for (int i = 0, numsLength = nums.length; i < numsLength; i++) {
            if (nums[i] == val) {
                nums[i] = Integer.MAX_VALUE;
                sum++;
            }
        }
        return sum;
    }
}
