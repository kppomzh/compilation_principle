package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

public class class_910 {

    @Test
    public void test1(){
        int[] nums={1,49,42,51,53,3,6,99};
        for (int i = 10; i <= 100; i+=10) {
            System.out.println(smallestRangeII(nums, i));
        }
    }
    @Test
    public void test2(){
        int[] nums={1,10};
        for (int i = 10; i <= 100; i+=10) {
            System.out.println(smallestRangeII(nums, i));
        }
    }
    @Test
    public void test3(){
        int[] nums={1,5,10};
        for (int i = 10; i <= 100; i+=10) {
            System.out.println(smallestRangeII(nums, i));
        }
    }
    @Test
    public void test4(){
        int[] nums={1};
        for (int i = 10; i <= 100; i+=10) {
            System.out.println(smallestRangeII(nums, i));
        }
    }
    @Test
    public void test5(){
        int[] nums={0,3,6,0,3,3};
        System.out.println(smallestRangeII(nums, 4));
    }
    @Test
    public void test6(){
        int[] nums={8038,9111,5458,8483,5052,9161,8368,2094,8366,9164,53,7222,9284,5059,4375,2667,2243,5329,3111,5678,5958,815,6841,1377,2752,8569,1483,9191,4675,6230,1169,9833,5366,502,1591,5113,2706,8515,3710,7272,1596,5114,3620,2911,8378,8012,4586,9610,8361,1646,2025,1323,5176,1832,7321,1900,1926,5518,8801,679,3368,2086,7486,575,9221,2993,421,1202,1845,9767,4533,1505,820,967,2811,5603,574,6920,5493,9490,9303,4648,281,2947,4117,2848,7395,930,1023,1439,8045,5161,2315,5705,7596,5854,1835,6591,2553,8628};
        System.out.println(smallestRangeII(nums, 4643));
    }

    public int smallestRangeII(int[] dsNums, int k) {
        Arrays.sort(dsNums);
        int max = dsNums[dsNums.length - 1];
        int min = dsNums[0];
        int minA = IntStream.range(0, dsNums.length - 1).parallel().
                map(idx -> Math.max(max - k, k + dsNums[idx]) - Math.min(min + k, dsNums[idx + 1] - k)).
                min().orElse(Integer.MAX_VALUE);
        return Math.min(max - min, minA);
    }

    public int smallestRangeII1(int[] nums, int k) {
        int[] dsNums = IntStream.of(nums).distinct().sorted().toArray();
        int middle_int_2 = (dsNums[dsNums.length - 1] + dsNums[0]);
        int[] nArr = IntStream.range(0, dsNums.length).parallel().map(idx -> {
//            if((dsNums[idx] << 1) > middle_int_2)
//                return dsNums[idx] - k;
//            else if((dsNums[idx] << 1) < middle_int_2)
//                return dsNums[idx] + k;
//            else
                if(dsNums[idx] - dsNums[0] > dsNums[dsNums.length - 1] - dsNums[idx])
                return dsNums[idx] - k;
            else
                return dsNums[idx] + k;

        }).sorted().toArray();
        return Math.min(dsNums[dsNums.length - 1] - dsNums[0], nArr[nArr.length - 1] - nArr[0]);
    }

    public int smallestRangeII2(int[] nums, int k) {
        Arrays.sort(nums);
        int sub = nums[nums.length - 1] - nums[0];
        int k_2 = k << 1;
        int middle = nums.length >> 1;
        int middle_int_2 = (nums[nums.length - 1] + nums[0]);
        if (nums.length == 1)
            return 0;
        else if (sub <= k) {
            return sub;
        } else {
            int idx;
            idx = 0;
            while (nums[idx] + nums[idx + 1] < middle_int_2) {
                idx++;
            }
            int minScoreUp = k_2 + nums[idx] - nums[idx + 1];
            int minScoreDown = k_2 + nums[idx - 1] - nums[idx];
            return Math.min(minScoreUp, minScoreDown);
        }
    }
}
