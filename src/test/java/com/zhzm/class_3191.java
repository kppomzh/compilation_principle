package com.zhzm;

import org.junit.Test;

public class class_3191 {

    @Test
    public void test1(){
        int[] nums={1,1,0,1,1};
        System.out.println(minOperations(nums));
    }
    @Test
    public void test2(){
        int[] nums={1,1,0,1,1,0};
        System.out.println(minOperations(nums));
    }
    @Test
    public void test3(){
        int[] nums={1,1,0,1};
        System.out.println(minOperations(nums));
    }

    public int minOperations(int[] nums) {
        int res = 0;
        for (int i = 0; i < nums.length - 2; i++) {
            if(nums[i] != 1) {
                res ++;
                for (int j = 1; j < 3; j++) {
                    nums[i+j] ^= 1;
                }
            }
        }
        return nums[nums.length - 2] != 1 || nums[nums.length - 1] != 1 ? -1 : res;
    }
}
