package com.zhzm;

import java.util.LinkedList;

public class class_953 {
    public static void main(String[] args) {
        class_953 class953=new class_953();
        System.out.println(class953.isAlienSorted(new String[]{"kuvp","q"},"ngxlkthsjuoqcpavbfdermiywz"));
    }
    public boolean isAlienSorted(String[] words, String order) {
        int[] map=new int[26];
        for(int i=0;i<26;i++){
            map[order.charAt(i)-97]=i;
        }

        LinkedList<Integer> last=new LinkedList<>();
        for(int i=0;i<words[0].length();i++){
            last.addLast(map[words[0].charAt(i)-97]);
        }
        for(int i=1;i<words.length;i++){
            int size=last.size();
            int j=0,min=Math.min(size,words[i].length());
            boolean res=size<=words[i].length();
            for(;j<min;j++){
                int lidx=last.removeFirst();
                int idx=map[words[i].charAt(j)-97];
                if(lidx>idx){
                    return false;
                } else if(lidx<idx) {
                    last.addLast(idx);
                    res=true;
                    break;
                } else
                    last.addLast(idx);
            }
            for(int c=j;c<size;c++){
                last.removeFirst();
            }
            for(;j<words[i].length();j++){
                last.addLast(map[words[i].charAt(j)-97]);
            }
            if(!res){
                return false;
            }
        }
        return true;
    }
}
