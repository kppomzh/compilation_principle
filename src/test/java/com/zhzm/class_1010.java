package com.zhzm;

public class class_1010 {
    public static void main(String[] args) {
        int[] todo=new int[]{30,20,100,150,40,60};


    }
    public int numPairsDivisibleBy60(int[] time) {
        int res=0;
        int[] map=new int[60];
        for (int i = 0; i < time.length; i++) {
            int remainder=time[i]%60;
            int complement=(60-remainder)%60;
            res+=map[complement];
            map[remainder]++;
        }
//        for (int t: time) {
//            int remainder=t%60;
//            int complement=(60-remainder)%60;
//            res+=map[complement];
//            map[remainder]++;
//        }
        return res;
    }
}
