package com.zhzm;

import com.zhzm.structure.geometry.Point;
import com.zhzm.utils.ConvexPolygon;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class class_587 {

    @Test
    public void test() {
        int[][] trees = new int[][]{{1,1},{2,2},{2,0},{2,4},{3,3},{4,2}};
        int[][] result = outerTrees(trees);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i][0] + " " + result[i][1]);
        }
    }
    @Test
    public void test1() {
        int[][] trees = new int[][]{{3,7},{6,8},{7,8},{11,10},{4,3},{8,5},{7,13},{4,13}};
        int[][] result = outerTrees(trees);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i][0] + " " + result[i][1]);
        }
    }

    public int[][] outerTrees(int[][] trees) {
        if (trees.length < 3)
            return trees;
        List<Point> points = Arrays.stream(trees).parallel().map((int[] ia) -> new Point(ia[0], ia[1])).toList();
        List<Point> edges = ConvexPolygon.getConvexPointColl(points);
        int[][] res = new int[edges.size()][];
        IntStream.range(0, edges.size()).parallel().forEach(i -> res[i] = edges.get(i).toArray());
        return res;
    }

}
