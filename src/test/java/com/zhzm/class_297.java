package com.zhzm;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class class_297 {
    public static void main(String[] args) {
        class_297 class297 = new class_297();
        System.out.println(class297.deserialize2(class297.serialize2(class297.deserialize("1,2,3,null,null,4,5"))));
    }

    public String serialize(TreeNode root) {
        if (root == null)
            return "null";

        StringBuilder sb = new StringBuilder();
        sb.append(root.val).append(',');

        List<TreeNode> nodeRes = new ArrayList<>();
        List<Integer> tmpRes;
        nodeRes.add(root);

        while (!nodeRes.isEmpty()) {
            tmpRes = new ArrayList<>();
            List<TreeNode> tmpNode = new ArrayList<>();
            for (TreeNode node : nodeRes) {
                tmpRes.add(node.val);
                if (node.left != null) {
                    tmpNode.add(node.left);
                    sb.append(node.left.val).append(',');
                } else {
                    sb.append("null,");
                }
                if (node.right != null) {
                    tmpNode.add(node.right);
                    sb.append(node.right.val).append(',');
                } else {
                    sb.append("null,");
                }
            }
            nodeRes = tmpNode;
        }

        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    public String serialize2(TreeNode root) {
        if(root==null)
            return "null";

        StringBuilder sb = new StringBuilder();
        getVal(root, sb);
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    private void getVal(TreeNode node, StringBuilder sb) {
        sb.append(node.val);
        sb.append(',');

        if (node.left == null) {
            sb.append("null,");
        } else
            getVal(node.left, sb);
        if (node.right == null) {
            sb.append("null,");
        } else
            getVal(node.right, sb);
    }

    public TreeNode deserialize2(String data) {
        String[] datas = data.split(",");
        TreeNode root=null;

        if(!"null".equals(datas[0])) {
            root=new TreeNode(Integer.parseInt(datas[0]));
            setVal(datas, 1, root);
        }

        return root;
    }

    private int setVal(String[] data,int idx,TreeNode now){
        if("null".equals(data[idx])){
            idx++;
        } else {
            TreeNode left=new TreeNode(Integer.parseInt(data[idx]));
            now.left=left;
            idx=setVal(data, idx+1, left);
        }

        if("null".equals(data[idx])){
            idx++;
        } else {
            TreeNode right=new TreeNode(Integer.parseInt(data[idx]));
            now.right=right;
            idx=setVal(data, idx+1, right);
        }

        return idx;
    }

    public TreeNode deserialize(String data) {
        String[] datas = data.split(",");
        TreeNode root = null;
        if (!"null".equals(datas[0])) {
            root = new TreeNode(Integer.parseInt(datas[0]));
            List<TreeNode> nodes = new ArrayList<>(2);
            nodes.add(root);

            for (int loop = 1; loop < datas.length; ) {
                int limit = nodes.size();
                List<TreeNode> tmpnodes = new ArrayList<>();
                for (int loopi = 0; loopi < limit; loopi++) {
                    TreeNode left, right;
                    if ("null".equals(datas[loop + (loopi * 2)])) {
                        left = null;
                    } else {
                        left = new TreeNode(Integer.parseInt(datas[loop + (loopi * 2)]));
                        tmpnodes.add(left);
                    }
                    if ("null".equals(datas[loop + (loopi * 2 + 1)])) {
                        right = null;
                    } else {
                        right = new TreeNode(Integer.parseInt(datas[loop + (loopi * 2 + 1)]));
                        tmpnodes.add(right);
                    }
                    nodes.get(loopi).left = left;
                    nodes.get(loopi).right = right;
                }
                loop += limit * 2;
                nodes = tmpnodes;
            }
        }
        return root;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}