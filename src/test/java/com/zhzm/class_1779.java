package com.zhzm;

import java.util.Comparator;
import java.util.PriorityQueue;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class class_1779 {
    public static void main(String[] args) {
        class_1779 class1779=new class_1779();
        System.out.println(class1779.nearestValidPoint(1,1,new int[][]{{1,2},{3,3},{3,3}}));
    }

    public int nearestValidPoint(int x, int y, int[][] points) {
        int res=-1;
        int minlength=Integer.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            if(points[i][0]==x){
                int length=Math.abs(y-points[i][1]);
                if(length<minlength){
                    minlength=length;
                    res=i;
                }
            } else if(points[i][1]==y){
                int length=Math.abs(x-points[i][0]);
                if(length<minlength){
                    minlength=length;
                    res=i;
                }
            }

            if(minlength==0)
                break;
        }
        return res;
    }

    public int nearestValidPoint2(int x, int y, int[][] points) {
        int res=-1;
        PriorityQueue<int[]> queue=new PriorityQueue<>(new LocalComparator(x, y));
        for (int i = 0; i < points.length; i++) {
            if(points[i][0]==x||points[i][1]==y)
                queue.add(points[i]);
        }
        if(!queue.isEmpty()){
            int[] point= queue.peek();
//            if(point[0]==x){
//                if(point[1]==y)
//                    res=0;
//                else
//                    res=point[1];
//            } else if(point[1]==y)
//                res=point[0];
            res=point[0]==x?0:point[0];
        }
        return res;
    }

    private class LocalComparator implements Comparator<int[]>{
        private int x,y;
        public LocalComparator(int x,int y){
            this.x=x;
            this.y=y;
        }

        @Override
        public int compare(int[] o1, int[] o2) {
            int o1length=Math.abs(x - o1[0]) + Math.abs(y - o1[1]);
            int o2length=Math.abs(x - o2[0]) + Math.abs(y - o2[1]);

            if(o1length==o2length){
                int di1=x==o1[0]?o1[1]:o1[0];
                int di2=x==o2[0]?o2[1]:o2[0];
                return di1-di2;
            } else
                return o1length-o2length;
        }
    }
}
