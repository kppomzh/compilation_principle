package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class class_204 {
    @Test
    public void test() {
        int count = 49979;
        long time = System.currentTimeMillis();
//            for (int i = 5; i < count; i++) {
//                countPrimes(i);
//            }
//            System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        int n = 0;
        for (int i = 15; i < count; i++) {
            n = countPrimes1(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");
        System.out.println(n);

        time = System.currentTimeMillis();
        for (int i = 15; i < count; i++) {
            n = countPrimes1plus(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");
        System.out.println(n);
//
        time = System.currentTimeMillis();
        for (int i = 15; i < count; i++) {
            countPrimes4(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");
    }

    @Test
    public void test1() {
        System.out.println(countPrimes1(20000));
        System.out.println(countPrimes1plus(20000));
//        boolean equals = Arrays.equals(countPrimes1(49978), countPrimes1plus(49978));
//        System.out.println(equals);
    }

    public int countPrimes(int n) {
        int res = 0;
        if (n > 2) {
            res = 1;
            boolean[] notPrimes = new boolean[n];
            for (int loop = 2; ; loop++) {
                int point = loop * 2;
                if (point < n) {
                    notPrimes[point] = true;
                } else {
                    break;
                }
            }

            int t = (int) Math.sqrt(n);
            int loop = 3;
            for (; loop <= t; loop += 2) {
                if (!notPrimes[loop]) {
                    for (int loopi = loop; ; loopi += 2) {
                        int pointc = loop * loopi;
                        if (pointc >= n) {
                            break;
                        }
                        notPrimes[pointc] = true;
                    }
                    res++;
                }
            }

            for (; loop < n; loop += 2) {
                if (!notPrimes[loop]) {
                    res++;
                }
            }
        }
        return res;
    }

    public int countPrimes2(int n) {
        int res = 0;
        if (n > 2) {
            res = 1;
            boolean[] notPrimes = new boolean[n];
            for (int loop = 2; ; loop++) {
                int point = loop * 2;
                if (point < n) {
                    notPrimes[point] = true;
                } else {
                    break;
                }
            }

            int t = (int) Math.sqrt(n);
            int loop = 3;
            for (; loop <= t; loop += 2) {
                if (!notPrimes[loop]) {
                    for (int loopi = loop; ; loopi += 2) {
                        int pointc = loop * loopi;
                        if (pointc >= n) {
                            break;
                        }
                        notPrimes[pointc] = true;
                    }
                    res++;
                }
            }

            for (; loop < n; loop += 2) {
                if (!notPrimes[loop]) {
                    res++;
                }
            }
        }
        return res;
    }


    public int countPrimes1(int n) {
        if (n < 2)
            return 0;
//            return new boolean[]{true};
        boolean[] notPrimes = new boolean[n];
        int res = 0;
        int i;
        int maxSieve = (int) Math.sqrt(n) + 1;
        for (i = 2; i < maxSieve; i++) {
            if (!notPrimes[i]) {
                res++;
                for (int j = i * i; j < n; j += i) {
                    notPrimes[j] = true;
                }
            }
        }
        for (; i < n; i++) {
            if (!notPrimes[i]) {
                res++;
            }
        }
        return res;
    }

    public int countPrimes1plus(int n) {
        if (n <= 2)
            return 0;
        boolean[] notPrimes = new boolean[n];
        int res = 1;
        int i;
        int maxSieve = (int) Math.sqrt(n) + 1;
        for (i = 3; i < maxSieve; i+=2) {
            if (!notPrimes[i]) {
                res++;
                int maxJ = (int) Math.ceil((double) n /i);
                for (int j = i ; j < maxJ; j +=2) {
                    notPrimes[i*j] = true;
                }
            }
        }
        for (; i < n; i+=2) {
            if (!notPrimes[i]) {
                res++;
            }
        }
        return res;
    }

    public int countPrimes4(int n) {
        boolean[] notPrime = new boolean[n + 1];
//        Arrays.fill(notPrime, false); // 初始化数组，默认所有数都不是质数标记
        List<Integer> primes = new ArrayList<>(); // 用于存储找到的质数

        for (int i = 2; i <= n; ++i) {
            if (!notPrime[i]) {
                primes.add(i);
            }
            for (int prime : primes) {
                if (i * prime > n)
                    break;
                notPrime[i * prime] = true;
                if (i % prime == 0) {
                    break;
                }
            }
        }

        return primes.size();
    }

    public int countPrimes3(int n) {
        List<Integer> primes = new ArrayList<>();
        int[] isPrime = new int[n];
        Arrays.fill(isPrime, 1);
        for (int i = 2; i < n; ++i) {
            if (isPrime[i] == 1) {
                primes.add(i);
            }
            for (int j = 0; j < primes.size() && i * primes.get(j) < n; ++j) {
                isPrime[i * primes.get(j)] = 0;
                if (i % primes.get(j) == 0) {
                    break;
                }
            }
        }
        return primes.size();
    }
}