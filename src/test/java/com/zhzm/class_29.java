package com.zhzm;

public class class_29 {
    public static void main(String[] args) {
        class_29 class29=new class_29();
        System.out.println(class29.divide(Integer.MAX_VALUE,3));
        System.out.println(class29.divide(Integer.MIN_VALUE,2));
        System.out.println(class29.divide(Integer.MAX_VALUE,2));
        System.out.println(class29.divide(3,3));
        System.out.println(class29.divide(10,3));
    }
    public int divide(int dividend, int divisor) {
        if (divisor == 1) {
            return dividend;
        }
        if (divisor == -1) {
            if (dividend == Integer.MIN_VALUE)
                return Integer.MAX_VALUE;
            else
                return -dividend;
        }

        boolean less0=false;
        if (dividend > 0) {
            dividend = -dividend;
            less0 = !less0;
        }
        if (divisor > 0) {
            divisor = -divisor;
            less0 = !less0;
        }

        if(dividend>divisor){
            return 0;
        }

        int idx=0;
        int[] array=new int[31];
        array[0]=divisor;
        while(array[idx]>=dividend-array[idx]){
            int dadd=array[idx]+array[idx];
            idx++;
            array[idx]=dadd;
        }

        int res=0;
        int added=0;
        for(int i=idx;i>=0;i--){
            int ta=added+array[i];
            if(ta<0 && ta>dividend){
                res+=(int)Math.pow(2,i);
                added=ta;
            }
            else if(ta==dividend){
                res+=(int)Math.pow(2,i);
                break;
            }
        }

        return less0?-res:res;
    }
}
