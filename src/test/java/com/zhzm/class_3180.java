package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

public class class_3180 {
    @Test
    public void test1() {
        System.out.println(maxTotalReward(new int[]{3,3}));;
    }

    public int maxTotalReward(int[] rewardValues) {
        Arrays.sort(rewardValues);
        return rewardValues[rewardValues.length-1] + find(rewardValues, rewardValues[rewardValues.length-1] - 1, rewardValues.length-2);
    }

    private int find(int[] rewardValues, int maxLimit, int idx) {
        if(maxLimit == 0)
            return 0;
        if(idx == 0)
            return maxLimit >= rewardValues[0] ? rewardValues[0] : 0;
        int res = 0;
        for (int i = idx; i >= 0; i--) {
            if(rewardValues[i] == maxLimit)
                return maxLimit;
            else if (rewardValues[i] < maxLimit) {
                int nextLimit = maxLimit - rewardValues[i];
                nextLimit = nextLimit>=rewardValues[i] ? rewardValues[i] - 1 : nextLimit;
                int down = find(rewardValues, nextLimit, i-1);
                if(rewardValues[i] + down == maxLimit)
                    return maxLimit;
                res = Math.max(res, rewardValues[i] + down);
            }
        }
        return res;
    }
}
