package com.zhzm;

import com.zhzm.structure.geometry.Point;
import com.zhzm.structure.geometry.Vector;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

public class class_2280 {
    @Test
    public void test() {
        int[][] stockPrices = new int[][]{
                {72,98},{62,27},{32,7},{71,4},{25,19},
                {91,30},{52,73},{10,9},{99,71},{47,22},
                {19,30},{80,63},{18,15},{48,17},{77,16},
                {46,27},{66,87},{55,84},{65,38},{30,9},
                {50,42},{100,60},{75,73},{98,53},{22,80},
                {41,61},{37,47},{95,8},{51,81},{78,79},
                {57,95}};
//        int[][] stockPrices = new int[][]{{1,1}, {2, 1}, {3, 2}, {4,4}};
        int c = minimumLines(stockPrices);
        System.out.println(c);
    }

    public int minimumLines(int[][] stockPrices) {
        if(stockPrices.length == 1)
            return 0;
        Arrays.sort(stockPrices, Comparator.comparingInt(a -> a[0]));
        int res = 1;
        Vector front = Vector.getVector(
                new Point(stockPrices[0][0], stockPrices[0][1]),
                new Point(stockPrices[1][0], stockPrices[1][1]));
        for (int i = 2; i < stockPrices.length; i++) {
            Point nextP = new Point(stockPrices[i][0], stockPrices[i][1]);
            Vector nextV = Vector.getVector(front.getEndPoint(), nextP);
            if(front.getCrossProductInt(nextV) != 0) {
                System.out.println(front.getEndPoint());
                res++;
            }
            front = nextV;
        }
        return res;
    }
}
