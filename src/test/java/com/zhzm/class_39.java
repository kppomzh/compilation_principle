package com.zhzm;

import java.util.*;

public class class_39 {
    public static void main(String[] args) {
        class_39 class39=new class_39();
        List<List<Integer>> res=class39.combinationSum(new int[]{2,3,5,7},7);
        System.out.println(res);
    }

    private LinkedList<Integer> tmpRes=new LinkedList<>();
    private int[] candidates;
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> res=new ArrayList<>();
        Arrays.sort(candidates);
        this.candidates=candidates;
        recursion(res,target,0);
        return res;
    }


    public void recursion(List<List<Integer>> res,int target,int idx){
        if (idx + 1 <= candidates.length && target >= candidates[idx]) {
            recursion(res, target, idx + 1);

            tmpRes.addLast(candidates[idx]);
            if (target == candidates[idx]) {
                res.add(List.copyOf(tmpRes));
            } else {
                recursion(res, target - candidates[idx], idx);
            }
            tmpRes.removeLast();
        }
    }
}
