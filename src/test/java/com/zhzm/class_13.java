package com.zhzm;

import java.util.HashMap;

public class class_13 {
    static HashMap<Character,Integer> chartoInt=new HashMap<>();

    static {
        chartoInt.put('M',1000);
        chartoInt.put('D',500);
        chartoInt.put('C',100);
        chartoInt.put('L',50);
        chartoInt.put('X',10);
        chartoInt.put('V',5);
        chartoInt.put('I',1);
        chartoInt.put('#',0);
    }

    public int romanToInt(String s) {
        int result=0;
        s=s+'#';

        for(int loop = 0; loop < s.length()-1; loop++) {
            int now=chartoInt.get(s.charAt(loop));
            int next=chartoInt.get(s.charAt(loop+1));
            result=now>=next?result+now:result-now;
        }
        return result;
    }
}
