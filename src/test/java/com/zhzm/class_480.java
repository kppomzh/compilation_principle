package com.zhzm;

import java.util.Arrays;

public class class_480 {
    public static void main(String[] ar){
        class_480 class480=new class_480();
//        print(class480.medianSlidingWindow(new int[]{1,3,-1,-3,5,3,6,7},3));
//        print(class480.medianSlidingWindow(new int[]{1,2,3,4,2,3,1,4,2},3));
//        print(class480.medianSlidingWindow(new int[]{1,2,3,4,2,3,1,4,2},4));
//        print(class480.medianSlidingWindow(new int[]{2147483647,2147483647},2));
//        print(class480.medianSlidingWindow(new int[]{2147483647,1,2,3,4,5,6,7,2147483647},2));
        print(class480.medianSlidingWindow(new int[]{Integer.MIN_VALUE,Integer.MIN_VALUE,2147483647},2));
    }

    private static void print(double[] res){
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i]);
            System.out.print(' ');
        }
        System.out.println();
    }

    public double[] medianSlidingWindow(int[] nums, int k) {
        mid window;
        double[] res=new double[nums.length-k+1];

        int[] init=new int[k];
        System.arraycopy(nums,0,init,0,k);
        if(k%2==0){
            window=new midB(init);
        } else {
            window=new midA(init);
        }
        res[0]=window.getMid();

        for (int i = 0; i < nums.length - k; i++) {
            window.changeNode(nums[i], nums[i+k]);
            res[i+1]=window.getMid();
        }

        return res;
    }
}

abstract class mid{
    protected double[] array;
    protected int middle;
    public mid(int[] conts){
        Arrays.sort(conts);
        array=new double[conts.length];
        for (int i = 0; i < conts.length; i++) {
            array[i]=conts[i];
        }
    }

    abstract double getMid();
    public void changeNode(int toRemove,int toAdd){
        if(toAdd==toRemove)
            return;

        int left=0,right=array.length-1;
        int rl=-1,al=-1;
        while(left<=right) {
            int middle=(left+right)/2;
            if(array[middle]>toRemove){
                right=middle-1;
            } else if(array[middle]<toRemove){
                left=middle+1;
            } else {
                rl=middle;
                break;
            }
        }

        left=0;
        right=array.length-1;
        if(toAdd<=array[left]){
            al=left;
        } else if(toAdd>=array[right]){
            al=right;
        } else {
            while (left <= right) {
                int middle = (left + right) / 2;
                if (array[middle] > toAdd) {
                    right = middle - 1;
                    if(toAdd >= array[right]) {
                        al = middle>rl?right:middle;
                        break;
                    }
                } else if (array[middle] < toAdd) {
                    left = middle + 1;
                    if (array[left] >= toAdd) {
                        al = middle<rl?left:middle;
                        break;
                    }
                } else {
                    al=middle;
                    break;
                }
            }
        }

        if(rl<al){
            System.arraycopy(array,rl+1,array,rl,al-rl);
        } else if(rl>al){
            System.arraycopy(array,al,array,al+1,rl-al);
        }
        array[al]=toAdd;
    }
}

class midA extends mid{

    public midA(int[] conts) {
        super(conts);
        middle=(conts.length-1)/2;
    }

    @Override
    double getMid() {
        return array[middle];
    }
}
class midB extends mid{
    public midB(int[] conts) {
        super(conts);
        middle=conts.length/2-1;
    }

    @Override
    double getMid() {
//        if(array[middle]==Integer.MIN_VALUE)
//            return array[middle]+(array[middle+1]-Integer.MAX_VALUE-1)/2d;
//        else
            return array[middle]+(array[middle+1]-array[middle])/2d;
    }
}
