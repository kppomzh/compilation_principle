package com.zhzm;

import java.util.Collections;

public class class_1210 {
    int[][] mod=new int[][]{
            {0,0,1},
            {0,0,0},
            {0,1,1}
    };

    public int minimumMoves(int[][] grid) {
        int res=0;

        int xlocal=0,ylocal=0;



        return res;
    }

    private int[] checkMoveMode(int[][] grid,int xlocal,int ylocal){
        int[] mode=new int[3];
        if(grid[ylocal][xlocal+2]==0) //允许横向移动一步
            mode[0]=1;

        if(grid[ylocal+1][xlocal]==0&&grid[ylocal+1][xlocal+1]==0){ //能够横向纵向滚动或转弯的前提
            if(grid[ylocal+1][xlocal+2]==0) //允许纵向滚动
                mode[1]=1^mode[0]; //如果已经可以横向移动，则无需纵滚
            if(grid[ylocal+2][xlocal]==0){
                if(grid[ylocal+2][xlocal+1]==0)
                    mode[1]=1;
                else
                    mode[2]=1;
            }
        }
        return mode;
    }
}
