package com.zhzm;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class class_145 {
    public void test(){
        class_145 c145=new class_145();
        TreeNode root = new TreeNode(1), left = new TreeNode(2),right = new TreeNode(3);
        root.left = left;
        root.right = right;
//        left.left = right;
        right.right=new TreeNode(5);
        System.out.println(c145.postorderTraversal(root));
        System.out.println(c145.postorderTraversal2(root));
    }

    public List<Integer> postorderTraversal(TreeNode root){
        List<Integer> res=new LinkedList<>();
        if(root!=null) {
            Stack<TreeNode> stack = new Stack<>();
            stack.push(root);

            TreeNode cursor = root.left,lastRight=null;
            while (!stack.isEmpty()) {
                if(cursor==null)
                    cursor = stack.pop();
                else while (cursor.left != null) {
                    stack.push(cursor);
                    cursor = cursor.left;
                }

                if (cursor.right != null && cursor.right != lastRight) {
                    stack.push(cursor);
                    cursor = cursor.right;
                } else {
                    res.add(cursor.val);
                    lastRight=cursor;
                    cursor=null;
                }
            }
        }
        return res;
    }

    public List<Integer> postorderTraversal2(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();
            TreeNode prev = null;
            while (root != null || !stack.isEmpty()) {
                while (root != null) {
                    stack.push(root);
                    root = root.left;
                }
                root = stack.pop();
                if (root.right == null || root.right == prev) {
                    res.add(root.val);
                    prev = root;
                    root = null;
                } else {
                    stack.push(root);
                    root = root.right;
                }
            }
        }
        return res;
    }
}
