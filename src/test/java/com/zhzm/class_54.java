package com.zhzm;

import java.util.ArrayList;
import java.util.List;

public class class_54 {
    public void test(){
        int[][] data=new int[][]{{1,2,3,8},{4,5,7,6},{3,8,9,8},{3,8,9,8}};
        List<Integer> res=new class_54().spiralOrder(data);
        for (int i:res){
            System.out.print(i);
            System.out.print(' ');
        }
    }

    public List<Integer> spiralOrder(int[][] matrix) {
        int height=matrix.length,width=matrix[0].length;
        List<Integer> res=new ArrayList<>();

        int xlocal=0,ylocal=0;
        int xstep=1,ystep=1;
        int xleftLimit=-1,xrightLimit=width,xLimit=xrightLimit;
        int yleftLimit=-1,yrightLimit=height,yLimit=yrightLimit;
        boolean Transverse=true;
        for(int i=0;i<height*width;i++){
            res.add(matrix[ylocal][xlocal]);
            if(Transverse){
                xlocal+=xstep;
                if(xlocal==xLimit){
                    Transverse=false;
                    xstep=-xstep;
                    xlocal=xLimit+xstep;
                    if(xstep>0){
                        xLimit=xrightLimit;
//                        xleftLimit++;
                        yrightLimit--;
                    } else {
                        xLimit=xleftLimit;
//                        xrightLimit--;
                        yleftLimit++;
                    }
                    ylocal+=ystep;
                }
            } else {
                ylocal+=ystep;
                if(ylocal==yLimit){
                    Transverse=true;
                    ystep=-ystep;
                    ylocal=yLimit+ystep;
                    if(ystep>0){
                        yLimit=yrightLimit;
//                        yleftLimit++;
                        xleftLimit++;
                    } else {
                        yLimit=yleftLimit;
//                        yrightLimit--;
                        xrightLimit--;
                    }
                    xlocal+=xstep;
                }
            }
        }

        return res;
    }
}
