package com.zhzm;

import java.util.Arrays;
import java.util.Collections;

public class class_1605 {
    public static void main(String[] args) {
        class_1605 class1605=new class_1605();
//        int[][] res= class1605.restoreMatrix(new int[]{3,8},new int[]{4,7});
        int[][] res= class1605.restoreMatrix(new int[]{5,7,10},new int[]{8,6,8});
        for (int i = 0; i < res.length; i++) {
            Arrays.stream(res[i]).forEach(System.out::print);
            System.out.println();
        }

    }
    public int[][] restoreMatrix(int[] rowSum, int[] colSum) {
        int length= rowSum.length,height=colSum.length;
        int[][] res=new int[length][height];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < height; j++) {
                if(rowSum[i] >= colSum[j]){
                    rowSum[i]-=colSum[j];
                    res[i][j]=colSum[j];
                    colSum[j]=0;
                } else {
                    colSum[j]-=rowSum[i];
                    res[i][j]=rowSum[i];
                    rowSum[i]=0;
                }
            }
        }
        return res;
    }
}
