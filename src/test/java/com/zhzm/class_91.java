package com.zhzm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class class_91 {
    public static void main(String[] args) {
        class_91 class91=new class_91();
        System.out.println(class91.numDecodings("2172114"));
        System.out.println(class91.numDecodings("2272114"));
        System.out.println(class91.numDecodings("2232114"));
        System.out.println(class91.numDecodings("12"));
        System.out.println(class91.numDecodings("226"));
        System.out.println(class91.numDecodings("06"));
        System.out.println(class91.numDecodings("70"));
        System.out.println(class91.numDecodings("2006"));
        System.out.println(class91.numDecodings("2610"));
        System.out.println(class91.numDecodings("271"));
    }
    public int numDecodings(String s) {
        int res=0,last=s.charAt(0)-48;
        boolean lastAdd=true;
        if(last!=0) {
            PriorityQueue<Integer> fibonacciNum=new PriorityQueue<>();
            int fnum=1,maxFnum=1;
            for (int i = 1; i < s.length(); i++) {
                int tmp = s.charAt(i) - 48;

                if (tmp == 0) {
                    if(last>2 || last<1)
                        return 0;
                    else {
                        maxFnum=fnum-1>maxFnum?fnum-1:maxFnum;
                        fibonacciNum.add(fnum-1);
                        fnum=0;
                        lastAdd=false;
                    }
                } else if(last*10+tmp>26) {
                    maxFnum=fnum>maxFnum?fnum:maxFnum;
                    fibonacciNum.add(fnum);
                    fnum=1;
                    lastAdd=false;
                } else {
                    fnum++;
                    lastAdd=true;
                }
                last=tmp;
            }
            if(lastAdd) {
                maxFnum = fnum > maxFnum ? fnum : maxFnum;
                fibonacciNum.add(fnum);
            }

            List<Integer> multiArray=new ArrayList<>();
            countFibonacci(maxFnum,fibonacciNum,multiArray);
            res=1;
            for(int multi:multiArray){
                res*=multi;
            }
        }
        return res;
    }

    private int[] countFibonacci(int max,PriorityQueue<Integer> fibonacciNum,List<Integer> resArray){
        int[] tmpArray;
        if(max==1){
            tmpArray=new int[]{1,1};
            while(fibonacciNum.peek()<2){
                fibonacciNum.poll();
                if(fibonacciNum.isEmpty())
                    break;
            }
        } else {
            tmpArray=countFibonacci(max-1, fibonacciNum, resArray);
            int now=tmpArray[0]+tmpArray[1];
            tmpArray[0]=tmpArray[1];
            tmpArray[1]=now;

            while(max==fibonacciNum.peek()){
                resArray.add(tmpArray[1]);
                fibonacciNum.poll();
                if(fibonacciNum.isEmpty())
                    break;
            }
        }
        return tmpArray;
    }

    public int numDecodings1(String s) {
        int res=0,tmp=s.charAt(0)-48;
        for (int i = 1; i < s.length(); i++) {
            int remainder=tmp%10;
            if(remainder!=0) {
                tmp = remainder;
                tmp *= 10;
                tmp += s.charAt(i) - 48;
                if (tmp <= 26)
                    res += 2;
                else
                    res++;
            } else if(tmp/10<3 && tmp/10>0){
                tmp = remainder;
                res++;

            } else {
                res = 0;
                break;
            }
        }
        return res;
    }
}
