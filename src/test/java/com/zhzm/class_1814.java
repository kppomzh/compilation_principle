package com.zhzm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class class_1814 {
    public static void main(String[] args) throws IOException {
        class_1814 class1814=new class_1814();
        BufferedReader bf=new BufferedReader(new FileReader("./data.txt"));
        String[] arri=bf.readLine().split(",");
        int[] data=new int[arri.length];
        for (int i = 0; i < arri.length; i++) {
            data[i]=Integer.parseInt(arri[i]);
        }

        System.out.println(class1814.countNicePairs(data));
    }
    public int countNicePairs(int[] nums) {
        int res=0;
        Map<Integer,Integer> map=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int transform=nums[i]-rev(nums[i]);
            int value=map.getOrDefault(transform,0);
            res=(res+value)%1000000007;
            value++;
            map.put(transform, value);
        }
        return res;
    }

    private int rev(int i){
        int res=0;
        while(i>0){
            res*=10;
            res+=(i%10);
            i/=10;
        }
        return res;
    }
}
