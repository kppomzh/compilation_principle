package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class class_219 {
    @Test
    public void test() {
        System.out.println(containsNearbyDuplicate3(new int[]{1, 2, 3, 1, 2, 3}, 2));
    }

    public boolean containsNearbyDuplicate(int[] nums, int k) {
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < Math.min(nums.length, i + k + 1); j++) {
                if (nums[i] == nums[j])
                    return true;
            }
        }
        return false;
    }

    public boolean containsNearbyDuplicate1(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i]) && i - map.get(nums[i]) <= k) {
                return true;
            } else {
                map.put(nums[i], i);
            }
        }
        return false;
    }

    public boolean containsNearbyDuplicate2(int[] nums, int k) {
        Set<Integer> cacheSet = new HashSet<>();
        List<Integer> window = new ArrayList<>(k+1);
        for (int i = 0; i <= Math.min(nums.length - 1, k); i++) {
            if (cacheSet.contains(nums[i]))
                return true;
            cacheSet.add(nums[i]);
            window.add(nums[i]);
        }
        if (k < nums.length) {
            for (int i = k + 1; i < nums.length; i++) {
                cacheSet.remove(window.remove(0));
                if (cacheSet.contains(nums[i]))
                    return true;
                cacheSet.add(nums[i]);
                window.add(nums[i]);
            }
        }
        return false;
    }

    public boolean containsNearbyDuplicate3(int[] nums, int k) {
        Set<Integer> cacheSet = new HashSet<>();
        for (int i = 0; i < Math.min(nums.length, k + 1); i++) {
            if (cacheSet.contains(nums[i]))
                return true;
            cacheSet.add(nums[i]);
        }
        for (int i = k + 1; i < nums.length; i++) {
            cacheSet.remove(nums[i - 1 - k]);
            if (cacheSet.contains(nums[i]))
                return true;
            cacheSet.add(nums[i]);
        }
        return false;
    }
}
