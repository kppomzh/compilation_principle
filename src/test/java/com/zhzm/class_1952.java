package com.zhzm;

public class class_1952 {

    public static void main(String[] args) {
        int count = 2000000;
        class_1952 class1952 = new class_1952();
        long time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThreeFast(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThree2(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThree2super(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThree2superplus(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThree2superpluspro(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");

        time = System.currentTimeMillis();
        for (int i = 5; i < count; i++) {
            class1952.isThree2superplusproturbo(i);
        }
        System.out.println(System.currentTimeMillis() - time + " ms");
    }

    public boolean isThreeFast(int n) {
        boolean res = n == 4;
        if (n > 4 && n % 2 == 1) {
            double t = Math.sqrt(n);
            if ((int) t == t)
                for (int i = 3; i <= t; i += 2) {
                    if (n % i == 0) {
                        res = i * i == n;
                        break;
                    }
                }
        }
        return res;
    }

    public boolean isThree2(int n) {
        for (int i = 2; i * i <= n; ++i) {
            if (n % i == 0)
                return i * i == n;
        }
        return false;
    }

    public boolean isThree2super(int n) {
        double t = Math.sqrt(n);
        for (int i = 2; i <= t; ++i) {
            if (n % i == 0)
                return i * i == n;
        }
        return false;
    }

    public boolean isThree2superplus(int n) {
        if (n > 4) {
            double t = Math.sqrt(n);
            for (int i = 3; i <= t; i += 2) {
                if (n % i == 0)
                    return i * i == n;
            }
            return false;
        } else
            return n == 4;
    }

    public boolean isThree2superpluspro(int n) {
        if (n > 4 && n % 2 == 1) {
            double t = Math.sqrt(n);
            for (int i = 3; i <= t; i += 2) {
                if (n % i == 0)
                    return i * i == n;
            }
            return false;
        } else
            return n == 4;
    }

    public boolean isThree2superplusproturbo(int n) {
        if (n > 4 && n % 2 == 1) {
            double t = Math.sqrt(n);
            if ((int) t == t)
                for (int i = 3; i <= t; i += 2) {
                    if (n % i == 0)
                        return i * i == n;
                }
            return false;
        } else
            return n == 4;
    }
}
