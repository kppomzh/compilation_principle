package com.zhzm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class class_3117 {
    public int minimumValueSum(int[] nums, int[] andValues) {
        List<List<Integer>> idxColl = new ArrayList<>();
        idxColl.add(countZeroAndEqual(nums, andValues[0], nums.length - andValues.length - 1));
        for (int i = 1; i < andValues.length; i++) {
            countAndEqual(nums, andValues, i, idxColl.get(i - 1));
        }
        return 1;
    }

    private List<Integer> countZeroAndEqual(int[] nums, int andValue, int last) {
        int temp = Integer.MAX_VALUE;
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < last; i++) {
            temp &= nums[i];
            if(temp == andValue)
                res.add(i);
        }
        return res;
    }

    private void countAndEqual(int[] nums, int[] andValues, int idx, List<Integer> res) {

    }
}
