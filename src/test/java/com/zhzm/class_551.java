package com.zhzm;

public class class_551 {
    public boolean checkRecord(String s) {
        int lInt = 0; // 连续迟到次数 可置零
        int aInt = 0; // 缺勤次数 单调递增
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case 'A':
                    aInt++;
                    if(aInt == 2)
                        return false;
                    else
                        lInt = 0;
                    break;
                case 'L':
                    lInt++;
                    if(lInt == 3)
                        return false;
                    break;
                case 'P':
                    lInt = 0;
            }
        }
        return true;
    }
}
