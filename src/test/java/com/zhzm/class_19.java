package com.zhzm;

import com.zhzm.structure.object.ListNode;

public class class_19 {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null){
            return head;
        }

        if (n <=0 ){
            return head;
        }

        int count = 0;
        ListNode t = head;
        while (t != null){
            count ++;
            t = t.next;
        }

        if (count < n){
            return head;
        }

        if (count == n){
            return head.next;
        }

        int del = count - n;
        count = 0;
        t = head;
        while (t != null){
            count++;

            if (count == del){
                break;
            }

            t = t.next;
        }

        t.next = t.next.next;

        return head;
    }
}
