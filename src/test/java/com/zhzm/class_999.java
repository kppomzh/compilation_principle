package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

// 可以被一步捕获的棋子数
public class class_999 {
    @Test
    public void test() {
    }

    public int numRookCaptures(char[][] board) {
        List<int[]> coord = new ArrayList<>();
        IntStream.range(0, board.length).forEach(i -> {
            IntStream.range(0, board[i].length).forEach(j -> {
                if(board[i][j] == 'R') {
                    coord.add(new int[]{i, j});
                }
            });
        });

        int res = 0;
        res += coord.stream().mapToInt(arr -> getChar(board[arr[0]], arr[1], 'p')).sum();
        res += coord.stream().mapToInt(arr -> getListChar(board, arr, 'p')).sum();
        return res;
    }

    private int getListChar(char[][] board, int[] arr, char c) {
        int res = 0;
        for (int i = arr[0]-1; i >= 0; i--) {
            if (board[i][arr[1]] != '.') {
                if(board[i][arr[1]] == c)
                    res++;
                break;
            }
        }
        for (int i = arr[0]+1; i < board.length; i++) {
            if (board[i][arr[1]] != '.') {
                if(board[i][arr[1]] == c)
                    res++;
                break;
            }
        }
        return res;
    }

    private int getChar(char[] arr, int j, char c) {
        int res = 0;
        for (int i = j-1; i >= 0; i--) {
            if (arr[i] != '.') {
                if(arr[i] == c)
                    res++;
                break;
            }
        }
        for (int i = j+1; i < arr.length; i++) {
            if (arr[i] != '.') {
                if(arr[i] == c)
                    res++;
                break;
            }
        }
        return res;
    }
}
