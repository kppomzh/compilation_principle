package com.zhzm;

import java.util.*;

public class class_1632 {
    public static void main(String[] args) {
        class_1632 class1632=new class_1632();
//        class1632.matrixRankTransform(new int[][]{{20,-21,14},{-19,4,19},{22,-47,24},{-19,4,19}});
//        class1632.matrixRankTransform(new int[][]{{-2,-35,-32,-5,-30,33,-12},{7,2,-43,4,-49,14,17},{4,23,-6,-15,-24,-17,6},{-47,20,39,-26,9,-44,39},{-50,-47,44,43,-22,33,-36},{-13,34,49,24,23,-2,-35},{-40,43,-22,-19,-4,23,-18}});
        class1632.matrixRankTransform(new int[][]{{7,7},{7,7}});
        class1632.matrixRankTransform(new int[][]{{7,7,0},{0,7,7},{0,0,7}});
        class1632.matrixRankTransform(new int[][]{{1,2},{3,4}});
//        class1632.matrixRankTransform(new int[][]{});
    }

    Map<Integer,Integer> lineMax,listMax;
    public int[][] matrixRankTransform(int[][] matrix) {
        int[][] res=new int[matrix.length][matrix[0].length];

        PriorityQueue<int[]> queue=new PriorityQueue<>(Comparator.comparingInt(o->o[0]));
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                queue.add(new int[]{matrix[i][j],i,j});
            }
        }

        lineMax=new HashMap<>((int) ((matrix.length*1.34)+1));//i
        listMax=new HashMap<>((int) ((matrix[0].length*1.34)+1));//j

        int lmat=queue.peek()[0];
        Map<Integer,List<int[]>> nodeLineMap=new HashMap<>(),nodeListMap=new HashMap<>();
        Set<List<int[]>> equalNum=new HashSet<>();
        while(true){
            if(queue.isEmpty()) {
                for (List<int[]> nodes:equalNum){
                    int maxLevel=0;
                    for (int[] node:nodes) {
                        maxLevel = Math.max(maxLevel,countNode(node, matrix, res));
                    }
                    for (int[] node:nodes) {
                        res[node[1]][node[2]]=maxLevel;
                    }
                }
                break;
            }
            else if (lmat != queue.peek()[0]) {
                lmat=queue.peek()[0];
                for (List<int[]> nodes:equalNum){
                    int maxLevel=0;
                    for (int[] node:nodes) {
                        maxLevel = Math.max(maxLevel,countNode(node, matrix, res));
                    }
                    for (int[] node:nodes) {
                        res[node[1]][node[2]]=maxLevel;
                    }
                }
                equalNum.clear();
                nodeLineMap.clear();
                nodeListMap.clear();
            } else {
                int[] node= queue.poll();
                List<int[]> xColl,yColl;
                xColl=nodeLineMap.get(node[1]);
                yColl=nodeListMap.get(node[2]);
                if(xColl==null){
                    if(yColl==null){
                        List<int[]> nColl=new ArrayList<>();
                        nColl.add(node);
                        equalNum.add(nColl);
                        nodeLineMap.put(node[1], nColl);
                        nodeListMap.put(node[2], nColl);
                    } else {
                        yColl.add(node);
                        nodeLineMap.put(node[1], yColl);
                    }
                } else {
                    nodeLineMap.put(node[2],xColl);
                    nodeListMap.put(node[2],xColl);
                    xColl.add(node);
                    if(yColl==null || xColl==yColl){
//                        xColl.add(node);
                    } else {
                        for(int[] onode:yColl){
                            xColl.add(onode);
                            nodeLineMap.put(onode[2],xColl);
                        }
                        equalNum.remove(yColl);
                    }
                }

            }
        }

        return res;
    }

    private int countNode(int[] node,int[][] matrix,int[][] result){
        int xLocal=node[1],yLocal=node[2];
        int xMaxLocal=lineMax.getOrDefault(xLocal,-1);
        int yMaxLocal=listMax.getOrDefault(yLocal,-1);
        int res=1;

        if(xMaxLocal!=-1){
            if(node[0]==matrix[xLocal][xMaxLocal]){
                res=result[xLocal][xMaxLocal];
            } else {
                res=result[xLocal][xMaxLocal]+1;
                lineMax.put(xLocal,yLocal);
            }
        } else {
            lineMax.put(xLocal,yLocal);
        }

        if(yMaxLocal!=-1){
            if(node[0]==matrix[yMaxLocal][yLocal]){
                res=Math.max(res,result[yMaxLocal][yLocal]);
            } else {
                res=Math.max(res,result[yMaxLocal][yLocal]+1);
                listMax.put(yLocal,xLocal);
            }
        } else {
            listMax.put(yLocal,xLocal);
        }
        return res;
    }
}
