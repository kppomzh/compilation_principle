package com.zhzm;

public class class_1653 {
    public static void main(String[] args) {
        class_1653 class1653 = new class_1653();
        System.out.println(class1653.minimumDeletions("aababbab"));
        System.out.println(class1653.minimumDeletions("bbaaaab"));
        System.out.println(class1653.minimumDeletions("baaaabb"));
        System.out.println(class1653.minimumDeletions("bbaaaabb"));
        System.out.println(class1653.minimumDeletions("ababaaaabbbbbaaababbbbbbaaabbaababbabbbbaabbbbaabbabbabaabbbababaa"));
    }

    private int minimumDeletions(String s) {
        int lb = 0, ra = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a') {
                ra++;
            }
        }
        int res = ra;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a')
                ra--;
            else
                lb++;
//            if(lb+ra>res)
//                break;
//            else
//                res=lb+ra;
            res=Math.min(res,lb+ra);
        }
        return res;
    }

    public int minimumDeletions2(String s) {
        int[] fa = new int[s.length()], bb = new int[s.length()];
        int ta = 0, tb = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            fa[i] = ta;
            if (s.charAt(i) == 'a') {
                ta++;
            }
        }
        for (int i = 0; i < s.length(); i++) {
            bb[i] = tb;
            if (s.charAt(i) == 'b') {
                tb++;
            }
        }

        int res = Integer.MAX_VALUE;
        for (int i = 0; i < s.length(); i++) {
            int tmp = fa[i] + bb[i];
            if (tmp > res)
                break;
            else
                res = tmp;
            res=Math.min(res,tmp);
        }
        return res;
    }
}
