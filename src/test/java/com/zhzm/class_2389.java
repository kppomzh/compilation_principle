package com.zhzm;

import java.util.Arrays;

public class class_2389 {
    public static void main(String[] args) {
        class_2389 class2389=new class_2389();
        Arrays.stream(class2389.answerQueries(new int[]{4,6,2,1},new int[]{3,10,11,12})).forEach(System.out::println);
    }
    public int[] answerQueries(int[] nums, int[] queries) {
        Arrays.sort(nums);
        int[] prefix=new int[nums.length+1];
        for(int i=0;i<nums.length;i++){
            prefix[i+1]=prefix[i]+nums[i];
        }

        int[] res=new int[queries.length];
        for (int i = 0; i < queries.length; i++) {
            res[i]=selectByMiddle(prefix,queries[i]);
        }
        return res;
    }

    private int selectByMiddle(int[] array,int find){
        int left=0,right=array.length-1,res=0;
        while(left<=right) {
            int middle=(left+right)/2;
            if(array[middle]>find){
                right=middle-1;
            } else if (array[middle] == find) {
                res=middle;
                break;
            } else {
                if(middle == array.length - 1 || array[middle + 1] > find){
                    res=middle;
                    break;
                } else {
                    left = middle + 1;
                }
            }
        }
        return res;
    }
}
