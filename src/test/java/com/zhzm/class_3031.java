package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

// 将单词恢复初始状态所需的最短时间 II
public class class_3031 {
    @Test
    public void test() {
        String word = "abacaba";
        char[] ca = new char[Integer.MAX_VALUE / 2000];
        Arrays.fill(ca, 'a');
        ca[Integer.MAX_VALUE / 2000 - 1] = 'z';
        String cas = new String(ca);

        long start = System.currentTimeMillis();
        int res = minimumTimeToInitialState(cas, 1);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(res);

        start = System.currentTimeMillis();
        res = minimumTimeToInitialStateold(cas, 1);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(res);

        start = System.currentTimeMillis();
        res = minimumTimeToInitialState(word, 2);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(res);
    }

    public int minimumTimeToInitialState(String S, int k) {
        char[] s = S.toCharArray();
        int n = s.length;
        int[] z = new int[n];
        int l = 0, r = 0;
        for (int i = 1; i < n; i++) {
            if (i <= r) {
                z[i] = Math.min(z[i - l], r - i + 1);
            }
            while (i + z[i] < n && s[z[i]] == s[i + z[i]]) {
                l = i;
                r = i + z[i];
                z[i]++;
            }
            if (i % k == 0 && z[i] >= n - i) {
                return i / k;
            }
        }
        return (n - 1) / k + 1;
    }

    public int minimumTimeToInitialStatec(String word, int k) {
        int length = word.length();
        int[] kmp = new int[length];
        int start = 0;
        int end = 0;
        for (int i = 0; i < length; i++) {
            if(i <= end) {
                kmp[i] = Math.min(kmp[i - start], end - i + 1);
            }
            while (kmp[i] + i < length && word.charAt(kmp[i]) == word.charAt(kmp[i] + i)) {
                start = i;
                end = kmp[i] + i;
                kmp[i]++;
            }
            if(i % k == 0 && kmp[i] >= length - i ) {
                return i/k;
            }
        }
        return (length-1)/k+1;
    }

    public int minimumTimeToInitialStateold(String word, int k) {
        int length = word.length();
        int page = length / k;
        int remainder = length % k;
        page = remainder == 0 ? page - 1 : page;
        remainder = remainder == 0 ? k : remainder;

        int usePage = page - 1;
        for (; usePage >= 0; usePage--) {
            int last = k * (page - usePage);
            if(word.startsWith(word.substring(last))) {
                break;
            }
        }
        if(usePage == 0) {
            int last = length - remainder;
            if(!word.startsWith(word.substring(last))) {
                usePage = -1;
            }
        }
        return page - usePage;
    }
}
