package com.zhzm;

public class class_1234 {
    int limit;
    int[] nums;

    public int balancedString(String s) {
        limit = s.length() / 4;
        nums = new int[4];

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            nums[getIdx(c)]++;
        }
        if (check())
            return 0;

        int res=Integer.MAX_VALUE;
        for (int left = 0, right = 0; left < s.length(); left++) {
            while(right<s.length() && !check()){
                nums[getIdx(s.charAt(right))]--;
                right++;
            }
            if(!check())
                break;
            res=Math.min(res,right-left);
            nums[getIdx(s.charAt(left))]++;
        }
        return res;
    }

    private int getIdx(char c) {
        switch (c) {
            case 'Q':
                return 0;
            case 'W':
                return 1;
            case 'E':
                return 2;
            case 'R':
            default:
                return 3;
        }
    }

    private boolean check() {
        boolean res = true;
        for (int i = 0; i < nums.length; i++) {
            res &= nums[i] <= limit;
        }
        return res;
    }
}
