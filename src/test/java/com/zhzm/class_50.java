package com.zhzm;

import java.math.BigDecimal;
import java.util.Arrays;

public class class_50 {

    public static void main(String[]ar){
        class_50 c=new class_50();
//        System.out.println(BigDecimal.valueOf(c.myPow(8.84372,-6)));
//        System.out.println(BigDecimal.valueOf(Math.pow(8.84372,-6)));
//        System.out.println(BigDecimal.valueOf(c.myPow(8,-5)));
//        System.out.println(BigDecimal.valueOf(Math.pow(8,-5)));
//        System.out.println(BigDecimal.valueOf(c.myPow(8,-6)));
//        System.out.println(BigDecimal.valueOf(Math.pow(8,-6)));
//        System.out.println(BigDecimal.valueOf(c.myPow(2,10)));
//        System.out.println(BigDecimal.valueOf(Math.pow(2,10)));
//        System.out.println(BigDecimal.valueOf(c.myPow(2,11)));
//        System.out.println(BigDecimal.valueOf(Math.pow(2,11)));
//        System.out.println(BigDecimal.valueOf(c.myPow(2,12)));
//        System.out.println(BigDecimal.valueOf(Math.pow(2,12)));
//        System.out.println(BigDecimal.valueOf(c.myPow(1,2147483647)));
        System.out.println(BigDecimal.valueOf(c.myPow(2.00000,-2147483648)));
//        System.out.println(Math.pow(0.2,-2147483647));
    }

    public double myPow(double x, int n) {
        if(n==0 || x==1){
            return 1;
        }
        if(x==-1){
            return n%2==0?1:-1;
        }

        double res=1;
        boolean N=n>0;

        while( n!=0 ) {
            int check=n%2;
            if((check>=0?check:-check)==1){
                res*=x;
            }
            x*=x;
            n/=2;
        }
        return N?res:1/res;
    }

}
