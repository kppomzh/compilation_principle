package com.zhzm;

public class class_122 {
    public int maxProfit(int[] prices) {
        int min = prices[0];
        int max = prices[0];
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            if(prices[i] < max) {
                res += max - min;
                min = prices[i];
                max = prices[i];
            } else {
                max = prices[i];
            }
        }
        return res + (max - min);
    }
}
