package com.zhzm;

// 镜面反射
public class class_858 {
    public int mirrorReflection(int p, int q) {
        int gcd = gcd(p, q);
        int xLength = q / gcd;
        int yLength = p / gcd;
        int xMod = xLength % 2;
        int yMod = yLength % 2;
        if(xMod == yMod)
            return 1;
        else if(xMod > yMod)
            return 0;
        else
            return 2;
    }

    public static int gcd (int a,int b) {
        while (a!=b) {
            if (a>b) {
                a = a-b;
            } else {
                b = b-a;
            }
        }
        return a;
    }
}
