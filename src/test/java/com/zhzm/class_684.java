package com.zhzm;

public class class_684 {
    public int[] findRedundantConnection(int[][] edges) {
        boolean[][] map = new boolean[edges.length + 1][edges.length + 1];
        int[] mapCount = new int[edges.length + 1];
        for (int i = 0; i < edges.length; i++) {
            int[] arr = edges[i];
            map[arr[0]][arr[1]] = true;
            map[arr[1]][arr[0]] = true;
            mapCount[arr[1]]++;
            mapCount[arr[0]]++;
        }
        int countOne = 1;
        while (countOne > 0) {
            countOne = 0;
            for (int i = 0; i < mapCount.length; i++) {
                if (mapCount[i] == 1) {
                    countOne++;
                    for (int j = 0; j < mapCount.length; j++) {
                        if (map[i][j]) {
                            map[i][j] = false;
                            map[j][i] = false;
                            mapCount[i]--;
                            mapCount[j]--;
                        }
                    }
                }
            }
        }
        for (int i = edges.length - 1; i >= 0; i--) {
            if (map[edges[i][0]][edges[i][1]]) {
                return edges[i];
            }
        }
        return new int[0];
    }
}
