package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class class_3145 {

    @Test
    public void testFindProductsOfElements() {
        long[][] queries = {{10, 20, 21}, {2, 3, 4}, {1, 3, 5}};
        int[] res = findProductsOfElements(queries);
        for (int i : res) {
            System.out.println(i);
        }
    }

    static List<List<Integer>> numArray = new ArrayList<>();
    static long MAX_ARRAY_SIZE = 1;
    static {
        numArray.add(List.of(0));
    }

    @Test
    public void testBuildNumberArray() {
        buildNumArray(100L);
        numArray.forEach(System.out::println);
    }

    public int[] findProductsOfElements(long[][] queries) {
        int[] res = new int[queries.length];
        for (int i = 0; i < queries.length; i++) {
            buildNumArray(queries[i][1]);
            int[] fromIdx = getNumArrIdx(queries[i][0]);
            int[] toIdx = getNumArrIdx(queries[i][1]);
            long arraysSum;
            if(fromIdx[0] == toIdx[0]) {
                arraysSum = getArraySum(numArray.get(toIdx[0]), fromIdx[1], toIdx[1]);
            } else {
                arraysSum = getArraySum(numArray.get(fromIdx[0]), fromIdx[1]);
                for (int j = fromIdx[0]+1; j < toIdx[0]; j++) {
                    arraysSum += numArray.get(j).stream().mapToInt(Integer::valueOf).sum();
                }
                arraysSum += getArraySum(numArray.get(toIdx[0]), 0, toIdx[1]);
            }
            res[i] = getLargeReminder(arraysSum ,queries[i][2]);
        }
        return res;
    }

    private int getLargeReminder(long arraysSum, long l) {
        long reminder62 = arraysSum % 62;
        long div62 = arraysSum / 62;
        long const62 = 1L << 62;
        long reminderRes = div62 * (const62 % l) + (1L << reminder62) % l;
        return (int) (reminderRes % l);
    }

    private int getArraySum(List<Integer> array, int start) {
        return getArraySum(array, start, array.size());
    }

    private int getArraySum(List<Integer> array, int start, int end) {
        int sum = 0;
        for (int i = start; i < end; i++) {
            sum += array.get(i);
        }
        return sum;
    }

    private int[] getNumArrIdx(long l) {
        int[] res = new int[2];
        long x = l;
        for (int i = 0; i < numArray.size(); i++) {
            List<Integer> array = numArray.get(i);
            if(x > array.size()) {
                x -= array.size();
            } else {
                res[0] = i;
                res[1] = (int) (x - 1);
                break;
            }
        }
        return res;
    }

    private void buildNumArray(long to) {
        while(MAX_ARRAY_SIZE < to) {
            List<Integer> lastBuild = numArray.getLast();
            int base = lastBuild.getFirst();
            int nowBase = base + 1;
            Integer[] nowBuild = new Integer[(lastBuild.size() << 1) + (1 << (numArray.size() - 1))];
            int idx = 0;
            while (idx < lastBuild.size()) {
                nowBuild[idx] = lastBuild.get(idx) == base ? nowBase : lastBuild.get(idx);
                idx++;
            }
            for (int l : lastBuild) {
                nowBuild[idx] = l;
                idx++;
                if (l == base) {
                    nowBuild[idx] = nowBase;
                    idx++;
                }
            }
            numArray.add(List.of(nowBuild));
            MAX_ARRAY_SIZE += nowBuild.length;
        }
    }
}
