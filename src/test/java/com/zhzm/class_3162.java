package com.zhzm;

import org.junit.Test;

import java.util.Arrays;

// 3162. 优质数对的总数 I
public class class_3162 {

    @Test
    public void test1() {
        int[] nums1 = {1, 3, 4};
        int[] nums2 = {1, 3, 4};
        int k = 1;
        System.out.println(numberOfPairs(nums1, nums2, k));
    }
    @Test
    public void test2() {
        int[] nums1 = {2,12};
        int[] nums2 = {3, 4};
        int k = 4;
        System.out.println(numberOfPairs(nums1, nums2, k));
    }
    @Test
    public void test3() {
        int[] nums1 = new int[Integer.MAX_VALUE >> 1];
        int[] nums2 = {3, 4};
        int k = 4;
        Arrays.fill(nums1, 74389);
        System.out.println(numberOfPairs(nums1, nums2, k));
    }

    public int numberOfPairs(int[] nums1, int[] nums2, int k) {
        return Arrays.stream(nums1).parallel().
                filter(i -> i % k == 0).map(i -> i/k).map(i -> {
            int pres = 0;
            for (int value : nums2) {
                if (i >= value && i % value == 0) pres++;
            }
            return pres;
        }).sum();
    }
}
