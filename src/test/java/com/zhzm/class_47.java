package com.zhzm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class class_47 {
    public static void main(String[] args) {
        class_47 class47 = new class_47();
        class47.permute(new int[]{1, 1, 2});
    }

    public List<List<Integer>> permute(int[] nums) {
        return permute(nums.length - 1, nums);
    }

    private List<List<Integer>> permute(int idx, int[] nums) {
        LinkedList<List<Integer>> res = new LinkedList<>();

        if (idx == 0) {
            res.add(new ArrayList<>(List.of(nums[idx])));
        } else {
            List<List<Integer>> tmp = permute(idx - 1, nums);
            for (List<Integer> list : tmp) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i) != nums[idx]) {
                        res.addLast(new ArrayList<>(list));
                        res.getLast().add(i, nums[idx]);
                    }
                }
                res.addLast(list);
                res.getLast().add(nums[idx]);
            }
        }

        return res;
    }
}
