package com.zhzm;

import com.zhzm.structure.object.ListNode;
import org.junit.Test;

// 重排链表
public class class_143 {

    @Test
    public void test() {
        int[] arr = {1,2,3,4,5,6,7,8};
        ListNode head = new ListNode(arr[0]);
        ListNode current = head;

        for (int i = 1; i < arr.length; i++) {
            current.next = new ListNode(arr[i]);
            current = current.next;
        }
        this.reorderList2(head);
        System.out.println(head);
    }

    /**
     * 重新排列给定的单链表，使得链表的前半部分和后半部分交替连接。
     * 例如，输入: 1->2->3->4->5
     * 输出: 1->4->2->5->3
     */
    public void reorderList(ListNode head) {
        /**
         * 找到链表的中间节点。这里使用快慢指针法，快指针每次移动两步，慢指针每次移动一步，
         * 当快指针到达链表末尾时，慢指针正好指向链表中点。
         */
        ListNode middle = this.getMiddleNode(head);

        /**
         * 反转链表的后半部分。从中间节点的下一个节点开始，将链表的后半部分反转，
         * 为后续的合并操作做准备。
         */
        ListNode reverse = this.reverseList(middle.next);

        /**
         * 断开原链表的后半部分，防止在合并过程中对原链表的前半部分产生影响。
         */
        middle.next = null;

        /**
         * 合并两个链表。将原链表的前半部分和反转后的后半部分交替连接，
         * 形成新的链表顺序。
         */
        this.mergeList(head, reverse);
    }

    /**
     * 获取链表的中间节点。
     * 通过快慢指针法，快指针每次移动两步，慢指针每次移动一步，当快指针到达链表末尾时，慢指针正好指向链表的中间节点。
     * 此方法适用于非循环链表。
     *
     * @param head 链表的头节点
     * @return 链表的中间节点
     */
    public ListNode getMiddleNode(ListNode head) {
        // 初始化快慢指针，快指针从头节点的下一个节点开始
        ListNode fast = head.next;
        ListNode slow = head;
        // 当快指针及其下一个节点都不为空时，进行循环
        while(fast != null && fast.next != null) {
            // 快指针每次移动两步
            fast = fast.next.next;
            // 慢指针每次移动一步
            slow = slow.next;
        }
        // 返回慢指针，此时慢指针指向链表的中间节点
        return slow;
    }

    /**
     * 反转一个单链表。
     *
     * @param head 单链表的头节点
     * @return 反转后的链表的头节点
     */
    public ListNode reverseList(ListNode head) {
        // 初始化两个指针，node1 作为反转后链表的尾节点，node2 用于遍历链表
        ListNode node1 = null;
        ListNode node2 = head;

        // 遍历链表直到 node2 为空，此时 node1 指向反转后的链表的头节点
        while(node2 != null) {
            // 临时变量 temp 用于存储 node2 的下一个节点，防止丢失
            ListNode temp = node2.next;
            // 将 node2 的 next 指向 node1，实现反转
            node2.next = node1;
            // 移动 node1 指针，将其指向当前的 node2，为下一次反转做准备
            node1 = node2;
            // 移动 node2 指针，将其指向临时变量 temp 指向的节点，继续遍历
            node2 = temp;
        }

        // 返回反转后的链表的头节点
        return node1;
    }

    /**
     * 合并两个链表。
     * 将链表reverse的节点逐一插入到链表head中，形成一个新链表，head成为新链表的头节点。
     * 注意：此方法不涉及返回值，操作直接在原链表上进行。
     *
     * @param head 原链表的头节点
     * @param reverse 需要合并入原链表的另一个链表的头节点
     */
    private void mergeList(ListNode head, ListNode reverse) {
        // 初始化两个指针，分别指向原链表和待合并链表的当前节点
        ListNode node1 = head;
        ListNode node2 = reverse;

        // 当两个链表都有节点时，进行合并操作
        while(node1 != null && node2 != null) {
            // 临时保存node1的下一个节点和node2的下一个节点
            ListNode temp1 = node1.next;
            ListNode temp2 = node2.next;

            // 将node2插入到node1之后
            node1.next = node2;
            // 将node1的原下一个节点插入到node2之后
            node2.next = temp1;

            // 移动node1和node2指针到下一个待处理节点
            node1 = temp1;
            node2 = temp2;
        }
    }

    int maxIdx;
    ListNode last;
    public void reorderList2(ListNode head) {
        int idx = 1;
        recordByRecursion(head, idx);
        mergeList(head, last);
    }

    public ListNode recordByRecursion(ListNode n, int idx) {
        if(n.next == null) {
            last = n;
            maxIdx = idx;
            return n;
        }
        ListNode upper = recordByRecursion(n.next, idx+1);
        if(upper != null) {
            upper.next = n;
            n.next = null;
        }
        return (maxIdx + 1) / 2 >= idx ? null : n;
    }
}
