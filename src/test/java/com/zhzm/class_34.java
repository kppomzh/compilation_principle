package com.zhzm;

import org.junit.Test;

public class class_34 {
    @Test
    public void test() {
        printArray(searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8));
        printArray(searchRange(new int[]{5, 6, 7, 7, 8, 8, 8, 9, 9, 9, 9, 10, 11, 12, 15, 17, 19, 19}, 8));
        printArray(searchRange(new int[]{5, 7, 7, 8, 8, 10}, 6));
        printArray(searchRange(new int[]{}, 6));
        printArray(searchRange(new int[]{6, 6, 6}, 6));
        printArray(searchRange(new int[]{1, 2, 6, 6, 6, 6, 7, 8, 8}, 6));
        printArray(searchRange(new int[]{1, 4}, 4));
    }

    public int[] searchRange2(int[] nums, int target) {
        if (nums.length == 0)
            return new int[]{-1, -1};

        int l = 0, r = nums.length - 1;
        while (l < r) {
            int mid = (l + r) / 2;
            if (nums[mid] >= target)
                r = mid;
            else
                l = mid + 1;
        }
        if (nums[r] != target)
            return new int[]{-1, -1};

        int L = r;
        l = 0;
        r = nums.length - 1;
        while (l < r) {
            int mid = (l + r + 1) / 2;
            if (nums[mid] <= target)
                l = mid;
            else
                r = mid - 1;
        }
        return new int[]{L, r};
    }

    public int[] searchRange(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return new int[]{-1, -1};
        }

        int[] res = new int[2];
        res[0] = nums[0] == target ? 0 : -1;
        res[1] = nums[nums.length - 1] == target ? nums.length - 1 : -1;

        int left = 0, right = nums.length - 1;
        int tleft = -1, tright = -1;
        int index = 0;
        while (left <= right) {
            index = (left + right) / 2;
            if (target > nums[index]) {
                left = index + 1;
            } else if (target < nums[index]) {
                right = index - 1;
            } else {
                tleft = index;
                tright = index;
                break;
            }
        }

        if (tleft > -1) {
            res[0] = tleft;
            res[1] = tright;
            while (tleft > 0 && nums[tleft - 1] == target) {
                int index1 = (left + tleft) / 2;
                if (target > nums[index1]) {
                    left = index1 + 1;
                } else {
                    tleft = index1;
                }
            }
            res[0] = tleft;
            while (tright < nums.length - 1 && nums[tright + 1] == target) {
                int index2 = (tright + right) / 2;
                if (target < nums[index]) {
                    right = index2 - 1;
                } else {
                    tright = index2 == tright ? index2 + 1 : index2;
                }
            }
            res[1] = tright;
        }
        return res;
    }

    public void printArray(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
            System.out.print(' ');
        }
        System.out.println();
    }
}
