package com.zhzm;

import java.util.*;

public class class_1129 {
    public static void main(String[] args) {
        class_1129 class1129=new class_1129();
        Arrays.stream(class1129.shortestAlternatingPaths(3,new int[][]{{0,1},{1,2}},new int[][]{{0,1},{2,0}})).forEach(System.out::println);
    }

    List<int[]> next;//0 is red,1 is blue
    boolean[] visitBlue,visitRed;

    public int[] shortestAlternatingPaths(int n, int[][] redEdges, int[][] blueEdges) {
        next = new LinkedList<>();
        visitRed = new boolean[n];
        visitBlue = new boolean[n];

        List<List<Integer>> redList=new ArrayList<>(n+1),blueList=new ArrayList<>(n+1);
        int[] res=new int[n];
        for (int i = 0; i < n; i++) {
            redList.add(new ArrayList<>());
            blueList.add(new ArrayList<>());
            res[i]=-1;
        }

        for (int i = 0; i < redEdges.length; i++) {
            int[] edge=redEdges[i];
            if(edge[0]<n){
                redList.get(edge[0]).add(edge[1]);
            }
        }
        for (int i = 0; i < blueEdges.length; i++) {
            int[] edge=blueEdges[i];
            if(edge[0]<n){
                blueList.get(edge[0]).add(edge[1]);
            }
        }

        int length=0;
        next.add(new int[]{1,1,0});

        while(!next.isEmpty()){
            List<int[]> tmpNext=next;
            next=new LinkedList<>();
            for (int[] entry: tmpNext){
                res[entry[2]]=res[entry[2]]==-1?length:res[entry[2]];
                if(entry[1]==1&&!visitBlue[entry[2]]) {
                    visitBlue[entry[2]]=true;
                    buildNext(redList, entry[2],0);
                }
                if(entry[0]==1&&!visitRed[entry[2]]) {
                    visitRed[entry[2]]=true;
                    buildNext(blueList, entry[2],1);
                }
            }
            length++;
        }


        return res;
    }

    private void buildNext(List<List<Integer>> list,int idx,int bi){
        for (int nextNode:list.get(idx)){
            int[] b = new int[3];
            b[2]=nextNode;
            b[bi] = 1;
            next.add(b);
        }
    }
}
