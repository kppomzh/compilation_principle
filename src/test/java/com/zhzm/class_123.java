package com.zhzm;

import com.zhzm.structure.object.MaxHeap;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

public class class_123 {

    @Test
    public void test() {
        System.out.println(maxProfit(new int[]{1,2,4,2,5,7,2,4,9,0}));
    }

    public int maxProfit(int[] prices) {
        List<Integer> maxList = new ArrayList<>();
        List<Integer> minList = new ArrayList<>();
        minList.add(prices[0]);
        IntStream.range(1, prices.length).parallel().filter(i -> prices[i] < prices[i - 1]).forEach(i -> {
            maxList.add(prices[i - 1]);
            minList.add(prices[i]);
        });
        maxList.add(prices[prices.length - 1]);

//        return IntStream.range(0, maxList.size()).
//                map(i -> countProfit(0, minList, i, maxList, 2)).
//                max().orElse(-1);
        return countProfit(minList, maxList, 2);
    }

    private int countProfit(List<Integer> minList, List<Integer> maxList, int k) {
        return 0;
    }

    public int maxProfit_failed(int[] prices) {
        PriorityQueue<Integer> heap = new PriorityQueue<>(Comparator.reverseOrder());
        heap.add(0);
        int min = prices[0];
        int max = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if(prices[i] < max) {
                heap.add(max - min);
                min = prices[i];
                max = prices[i];
            } else {
                max = prices[i];
            }
        }
        heap.add(max - min);
        return heap.poll() + heap.poll();
    }
}
