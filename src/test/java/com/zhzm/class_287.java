package com.zhzm;

import org.junit.Test;

public class class_287 {

    @Test
    public void test()
    {
        int[] nums = {1,2,4,2,3};
        System.out.println(findDuplicate(nums));
    }

    public int findDuplicate(int[] nums) {
        int slow = 0, fast = 0;
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
            System.out.println("slow:"+slow+",fast:"+fast);
        } while (slow != fast);
        slow = 0;
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
            System.out.println("slow:"+slow+",fast:"+fast);
        }
        return slow;
    }
}
