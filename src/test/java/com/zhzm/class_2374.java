package com.zhzm;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_2374 {
    @Test
    public void test() {
        int[] edges = {3,3,3,0};
        System.out.println(edgeScore(edges));
    }
    @Test
    public void test1() throws IOException {
        List<String> nums = Files.readAllLines(Path.of("C:\\Users\\rkppo\\Downloads\\37547.txt"));
        System.out.println(edgeScore(nums.parallelStream().mapToInt(Integer::parseInt).toArray()));
    }

    public int edgeScore(int[] edges) {
        long[] score = new long[edges.length];
        for (int i = 0; i < edges.length; i++) {
            score[edges[i]] += i;
        }
        int res = score.length - 1;
        Map<Integer, Long> idxMap = new HashMap<>();
        for (int i = 0; i < score.length; i++) {
            if(score[i] != 0) {
                idxMap.put(i, score[i]);
            }
        }
        for (int i = score.length - 2; i >= 0; i--) {
            if(score[i] >= score[res])
                res = i;
        }
        return res;
    }

    public int edgeScore1(int[] edges) {
        int[] score = new int[edges.length];
        int res = 0;
        for (int i = 0; i < edges.length; i++) {
            score[edges[i]] += i;
            if(score[edges[i]] > score[res]) {
                res = edges[i];
            } else if(score[edges[i]] == score[res]) {
                res = Math.min(res, edges[i]);
            }
        }
        return res;
    }
}
