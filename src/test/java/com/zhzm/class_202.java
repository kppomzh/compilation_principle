package com.zhzm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class class_202 {
    public static void main(String[] args) {
        class_202 class202=new class_202();
        class202.isHappy(3);
    }
    public boolean isHappy(int n) {
        Set<Integer> set=new HashSet<>(Arrays.asList(4,16,20,37,42,58,89,145));
        while(n!=1){
            int tmp=0;
            while(n>0){
                int l=n%10;
                tmp+=l*l;
                n/=10;
            }
            if(set.contains(tmp))
                return false;
            else
                n=tmp;
        }
        return true;
    }
}
