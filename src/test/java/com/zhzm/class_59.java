package com.zhzm;

public class class_59 {

    public void test(){
        int[][] data=new class_59().generateMatrix(9);
        for (int[] i:data){
            for (int j:i) {
                System.out.print(j);
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    public int[][] generateMatrix(int n) {
        int length= n*n;
        int[][] res=new int[n][n];

        int xlocal=0,ylocal=0;
        int xstep=1,ystep=1;
        int xleftLimit=-1,xrightLimit=n,xLimit=xrightLimit;
        int yleftLimit=-1,yrightLimit=n,yLimit=yrightLimit;
        boolean Transverse=true;
        for (int i = 0; i < length; i++) {
            res[ylocal][xlocal]=i+1;
            if(Transverse){
                xlocal+=xstep;
                if(xlocal==xLimit){
                    Transverse=false;
                    xstep=-xstep;
                    xlocal=xLimit+xstep;
                    if(xstep>0){
                        xLimit=xrightLimit;
                        yrightLimit--;
                    } else {
                        xLimit=xleftLimit;
                        yleftLimit++;
                    }
                    ylocal+=ystep;
                }
            } else {
                ylocal+=ystep;
                if(ylocal==yLimit){
                    Transverse=true;
                    ystep=-ystep;
                    ylocal=yLimit+ystep;
                    if(ystep>0){
                        yLimit=yrightLimit;
                        xleftLimit++;
                    } else {
                        yLimit=yleftLimit;
                        xrightLimit--;
                    }
                    xlocal+=xstep;
                }
            }
        }
        return res;
    }
}
