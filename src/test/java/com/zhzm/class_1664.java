package com.zhzm;

public class class_1664 {
    public static void main(String[] args) {
        class_1664 class1664=new class_1664();
        System.out.println(class1664.waysToMakeFair(new int[]{2,6,6,4,3,3}));System.out.println(class1664.waysToMakeFair(new int[]{2,1,6,4,3,3}));
    }

    public int waysToMakeFair(int[] nums) {
        int res=0;
        int singleSum=0,doubleSum=0;
        int[] suffixSin=new int[nums.length];
        int[] suffixDou=new int[nums.length];
        boolean open=(nums.length-1)%2!=0;
        if(open)
            singleSum=nums[nums.length-1];
        else
            doubleSum=nums[nums.length-1];
        for (int i = nums.length-2; i >= 0; i--) {
            suffixDou[i]=doubleSum;
            suffixSin[i]=singleSum;
            if(open){
                doubleSum+=nums[i];
                open=false;
            } else {
                singleSum+=nums[i];
                open=true;
            }
        }

        open=true;
        for (int i = 0; i < nums.length; i++) {
            if(open){
                if(doubleSum-suffixDou[i]-nums[i]+suffixSin[i]==singleSum-suffixSin[i]+suffixDou[i])
                    res++;
                open=false;
            } else {
                if(singleSum-suffixSin[i]-nums[i]+suffixDou[i]==doubleSum-suffixDou[i]+suffixSin[i])
                    res++;
                open=true;
            }
        }
        return res;
    }
}
