package com.zhzm;

import org.junit.Test;

// 拆炸弹
public class class_1652 {

    @Test
    public void test() {
        this.decrypt(new int[0], 0);
    }

    public int[] decrypt(int[] code, int k) {
        if(k==0)
            return new int[code.length];
        else if(k<0) {
            reverse(code);
            k=-k;
            int[] res = makeDecrypt(code, k);
            reverse(res);
            return res;
        } else {
            return makeDecrypt(code, k);
        }
    }

    private int[] makeDecrypt(int[] code, int k) {
        int[] res = new int[code.length];
        int midres = 0;
        for(int i = 0; i < k; i++)  {
            midres += code[i];
        }
        res[res.length - 1] = midres;
        int minusmid=midres;
        for(int i = 1; i < k; i++)  {
            res[res.length -1 - i] = minusmid - code[k-i] + code[code.length - i];
            minusmid = res[res.length -1 - i];
        }
        for(int i = 0; i < res.length - k; i++)  {
            midres = midres + code[i+k] - code[i];
            res[i] = midres;
        }

        return res;
    }

    private void reverse(int[] array) {
        int start = 0;
        int end = array.length - 1;
        while(start < end) {
            int t = array[start];
            array[start] = array[end];
            array[end] = t;
            start++;
            end = array.length - 1 - start;
        }
    }
}
