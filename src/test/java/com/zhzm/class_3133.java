package com.zhzm;

import org.junit.Test;

// 数组最后一个元素的最小值
public class class_3133 {
    @Test
    public void test() {
        System.out.println(minEnd(3, 4));
        System.out.println(minEnd(2, 7));
        System.out.println(minEnd(200000, 774));
    }

    public long minEnd(int n, int x) {
        boolean[] bArray = new boolean[64];
        long res = 0L;
        int count = x;
        int idx = 0;
        int minZeroIdx = Integer.MAX_VALUE;
        while(count > 0) {
            if(count % 2 == 1) {
                bArray[idx] = true;
            } else {
                minZeroIdx = Math.min(minZeroIdx, idx);
            }
            count >>= 1;
            idx++;
        }

        count = n - 1;
        idx = Math.min(idx, minZeroIdx);
        while(count > 0) {
            if(bArray[idx]) {
                idx++;
                continue;
            }
            if(count % 2 == 1) {
                bArray[idx] = true;
            }
            count >>= 1;
            idx++;
        }

        for(int bi = bArray.length - 1; bi >= 0; bi--) {
            res <<= 1;
            if(bArray[bi]) {
                res++;
            }
        }
        return res;
    }
}
