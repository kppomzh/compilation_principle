package com.zhzm;

import java.util.Arrays;

public class class_1250 {
    public static void main(String[] args) {
        class_1250 class1250=new class_1250();
        System.out.println(class1250.isGoodArray(new int[]{12,128,777}));
        System.out.println(class1250.isGoodArray(new int[]{12,5,7,23}));
    }

    public boolean isGoodArray(int[] nums) {
        Arrays.sort(nums);
        int appro = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if ((appro = gcd(nums[i], appro)) == 1)
                break;
        }
        return appro == 1;
    }

    private int gcd(int a, int b) {
        if (a == b || b==1)
            return a;
        else if (a % b == 0)
            return b;

        int t;
        while(true){
            t = b;
            if((b=a%b)==0)
                break;
            a=t;
        }
        return t;
    }

    private int steingcd(int a, int b) {
        if (a == b)
            return a;
        else if (a % b == 0)
            return b;

        int c = 0;
        while ((a & 1) == 0 && (b & 1) == 0) {
            a >>= 1;
            b >>= 1;
            c++;
        }
        while ((a & 1) == 0)
            a >>= 1;
        while ((b & 1) == 0)
            b >>= 1;

        if (a < b) {
            int t = a;
            a = b;
            b = t;
        }

//        while (a > 0 && b > 0) {
//            a -= b;
//            if (a < b) {
//                int t = a;
//                a = b;
//                b = t;
//            }
//        }
//        return a<<c;
        while ((a = (a - b) >> 1) != 0) {
            while ((a & 1) == 0) a >>= 1;
            if (a < b) {
                int t = a;
                a = b;
                b = t;
            }
        }
        return b << c;
    }
}
