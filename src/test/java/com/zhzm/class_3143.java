package com.zhzm;

import org.junit.Test;

import java.util.*;

public class class_3143 {
    @Test
    public void test() {
        System.out.println(maxPointsInsideSquare(new int[][]{{2,2}, {-1,-2}, {-4,4}, {-3,1}, {3,-3}}, "abdca"));
    }
    @Test
    public void test1() {
        System.out.println(maxPointsInsideSquare(new int[][]{{-1,-1}, {1,1}, {2,2}}, "ccd"));
    }
    @Test
    public void test2() {
        System.out.println(maxPointsInsideSquare(new int[][]{{-1,-1}}, "c"));
    }


    public int maxPointsInsideSquare(int[][] points, String s) {
        int maxRange = Integer.MAX_VALUE;
        int maxDistance = 0;
        Map<Integer, Set<Character>> pointMarkDistance = new HashMap<>();
        pointMarkDistance.put(-1, Collections.emptySet());
        for (int i = 0; i < points.length; i++) {
            int[] point = points[i];
            Character c = s.charAt(i);
            int distance = Math.max(Math.abs(point[0]), Math.abs(point[1]));
            Set<Character> markSet = pointMarkDistance.computeIfAbsent(
                    distance, k-> new HashSet<>());
            maxDistance = Math.max(distance, maxDistance);
            if(markSet.contains(c)) {
                maxRange = Math.min(distance, maxRange);
            } else {
                markSet.add(c);
            }
        }
        List<Integer> keyList = pointMarkDistance.keySet().stream().sorted().toList();
        maxRange = Math.min(maxRange, maxDistance + 1);
        Set<Character> res = pointMarkDistance.get(maxDistance);
        for (int i = 1; i < keyList.size(); i++) {
            int distance = keyList.get(i);
            if (distance >= maxRange) {
                res = pointMarkDistance.get(keyList.get(i - 1));
                break;
            }
            int ldSize = pointMarkDistance.get(keyList.get(i)).size();
            int rdSize = pointMarkDistance.get(keyList.get(i - 1)).size();
            pointMarkDistance.get(keyList.get(i)).addAll(pointMarkDistance.get(keyList.get(i - 1)));
            if(pointMarkDistance.get(keyList.get(i)).size() < ldSize + rdSize) {
                res = pointMarkDistance.get(keyList.get(i - 1));
                break;
            }
        }
        return res.size();
    }
}
