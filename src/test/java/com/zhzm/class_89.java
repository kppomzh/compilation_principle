package com.zhzm;

import java.util.Arrays;
import java.util.List;

public class class_89 {
    public List<Integer> grayCode(int n) {
        return Arrays.asList(getTrueNumber(n));
    }

    public Integer[] getTrueNumber(int range){
        if(range==1){
            return new Integer[]{0,1};
        } else {
            Integer[] upper=getTrueNumber(range-1);
            Integer[] res=new Integer[2*upper.length];
            System.arraycopy(upper,0,res,0,upper.length);
            for(int i=0;i<upper.length;i++){
                res[i+upper.length]=upper.length+upper[upper.length-1-i];
            }
            return res;
        }
    }
}
