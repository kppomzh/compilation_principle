package com.zhzm;

public class class_121 {
    public int maxProfit(int[] prices) {
        int minPrice=Integer.MAX_VALUE;
        int maxSpread=0;

        for(int i=0;i<prices.length;i++){
            if(prices[i]<minPrice){
                minPrice=prices[i];
            } else {
                maxSpread=Math.max(maxSpread,prices[i]-minPrice);
            }
        }
        return maxSpread;
    }
}
