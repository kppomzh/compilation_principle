package com.zhzm;

public class class_1658 {
    public static void main(String[] ar){
        class_1658 c1658=new class_1658();

        System.out.println(c1658.minOperations2(new int[]{5207,5594,477,6938,8010,7606,2356,6349,3970,751,5997,6114,9903,3859,6900,7722,2378,1996,8902,228,
                4461,90,7321,7893,4879,9987,1146,8177,1073,7254,5088,402,4266,6443,3084,1403,5357,2565,3470,3639,
                9468,8932,3119,5839,8008,2712,2735,825,4236,3703,2711,530,9630,1521,2174,5027,4833,3483,445,8300,
                3194,8784,279,3097,1491,9864,4992,6164,2043,5364,9192,9649,9944,7230,7224,585,3722,5628,4833,8379,
                3967,5649,2554,5828,4331,3547,7847,5433,3394,4968,9983,3540,9224,6216,9665,8070,31,3555,4198,2626,
                9553,9724,4503,1951,9980,3975,6025,8928,2952,911,3674,6620,3745,6548,4985,5206,5777,1908,6029,2322,
                2626,2188,5639},565610));
        System.out.println(c1658.minOperations2(new int[]{5207},5207));
        System.out.println(c1658.minOperations2(new int[]{5,2,3,1,1},5));
        System.out.println(c1658.minOperations2(new int[]{5,1,4,2,3},6));
    }

    public int minOperations(int[] nums, int x) {
        int res=0;
        int front=0,end=0;
        int fpoint=0,epoint=-1;
        for (int i = nums.length-1; i >= 0; i--) {
            res+=nums[i];
            if(res==x){
                end=res;
                res = Integer.MAX_VALUE;
                epoint=i;
                break;
            } else if(res<0||res>x){
                end=res-nums[i];
                res=Integer.MAX_VALUE;
                epoint=i+1;
                break;
            }
        }
        if(res!=Integer.MAX_VALUE)
            return -1;

        while(epoint<nums.length){
            if(front+end==x) {
                res = Math.min(res, fpoint + nums.length - epoint);
                front+=nums[fpoint];
                fpoint++;
                end-=nums[epoint];
                epoint++;
            } else if (front+end<x) {
                front+=nums[fpoint];
                fpoint++;
            } else if (front + end > x) {
                end-=nums[epoint];
                epoint++;
            }
        }
        while(fpoint<nums.length && front+end<x){
            front+=nums[fpoint];
            fpoint++;
        }
        if(front+end==x) {
            res = Math.min(res, fpoint + nums.length - epoint);
        }

        return res==Integer.MAX_VALUE?-1:res;
    }

    public int minOperations2(int[] nums, int x) {
        int res=Integer.MAX_VALUE;
        int front=0,end=0;
        int fpoint=0,epoint;
        for (epoint = nums.length-1; epoint >= 0; epoint--) {
            end+=nums[epoint];
            if(end>=x){
                break;
            }
        }

        int opoint=-1;
        while(epoint< nums.length || opoint != fpoint){
            boolean f=false,e=false;
            if(front+end==x){
                res = Math.min(res, fpoint + nums.length - epoint);
                f=true;e=true;
            } else if(front+end<x) {
                f=true;
            } else if(front+end>x) {
                e=true;
            }

            if(f){
                front+=nums[fpoint];
                fpoint++;
            }
            if(e&&epoint<nums.length){
                end-=nums[epoint];
                epoint++;
            }
            opoint = fpoint;
        }
        return res==Integer.MAX_VALUE?-1:res;
    }
}
