package com.zhzm;


import java.util.Arrays;

public class class_1599 {
    public static void main(String[] args) {
        class_1599 class1599=new class_1599();
        char[] ca=new char[]{'1','2','c'};
        Arrays.sort(ca);
        Arrays.equals(new char[]{'1','2','c'},ca);
//        Assert.assertEquals(2,
//                class1599.minOperationsMaxProfit(new int[]{3,4,0,0},5,1));
//        Assert.assertEquals(2,
//                class1599.minOperationsMaxProfit(new int[]{3,4,0,0,0},5,1));
//        Assert.assertEquals(5,
//                class1599.minOperationsMaxProfit(new int[]{3,4,0,5,1},5,1));
//        Assert.assertEquals(7,
//                class1599.minOperationsMaxProfit(new int[]{10,9,6},6,4));
    }
    public int minOperationsMaxProfit1(int[] customers, int boardingCost, int runningCost) {
        int people=0,res=-1,lostboard=1,maxCost=0,hisCost=0;
        for(int i=0;people>0||i<customers.length;i++){
            if(people==0 && customers[i]==0){
                lostboard++;
            } else {
                if(i<customers.length)
                    people+=customers[i];
                int pay= Math.min(people, 4);
                people-=pay;
                hisCost+=(pay*boardingCost-lostboard*runningCost);
                if(hisCost>maxCost){
                    maxCost=hisCost;
                    res=i+1;
                }
                lostboard=1;
            }
        }
        return res;
    }

    public int minOperationsMaxProfit(int[] customers, int boardingCost, int runningCost) {
        int remain = 0;
        int rotation = 0;
        int totalCustomer = 0;
        int length = customers.length;
        for (int i = 0; i < length; i++) {

            totalCustomer += customers[i];
            remain += customers[i];
            if (remain > 4) {
                remain -= 4;
            } else {
                remain = 0;
            }
            rotation++;
        }
        rotation += remain / 4;
        remain %= 4;
        if (remain * boardingCost > runningCost) {
            rotation++;
        }
        int amount = totalCustomer * boardingCost - rotation * runningCost;
        return amount <= 0 ? -1 : rotation;
    }
}
