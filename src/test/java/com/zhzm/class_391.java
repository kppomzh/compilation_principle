package com.zhzm;

import com.zhzm.structure.geometry.Point;
import com.zhzm.structure.geometry.Rectangle;
import com.zhzm.structure.geometry.Vector;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// 完美矩形
public class class_391 {

    @Test
    public void test() {
        Assert.assertTrue(isRectangleCover(new int[][]{{1, 1, 3, 3}, {3, 1, 4, 2}, {3, 2, 4, 4}, {1, 3, 2, 4}, {2, 3, 3, 4}}));
//        Assert.assertFalse(isRectangleCover(new int[][]{{1, 1, 2, 3}, {1, 3, 2, 4}, {3, 1, 4, 2}, {3, 2, 4, 4}}));
//        Assert.assertFalse(isRectangleCover(new int[][]{{1, 1, 3, 3}, {3, 1, 4, 2}, {1, 3, 2, 4}, {2, 2, 4, 4}}));
//        Assert.assertFalse(isRectangleCover(new int[][]{{1, 1, 3, 3}, {3, 1, 4, 2}, {3, 2, 4, 4}, {1, 3, 2, 4}, {2, 3, 3, 4},{2, 2, 3, 3}}));
//        Assert.assertFalse(isRectangleCover(new int[][]{{2,2,3,6}, {3,3,4,5}, {3,5,7,6}, {5,3,7,5}, {3,2,7,3}}));
//        Assert.assertFalse(isRectangleCover(new int[][]{{2,2,3,6}, {5,4,6,5}, {3,5,4,6}, {5,3,7,5}, {3,2,7,3}}));
//        Assert.assertTrue(isRectangleCover(new int[][]{{0, 1, 1,2}, {0,2,1,3}, {0,3,1,4}, {0,0,1,1}}));
    }

    /**
     * 判断给定的矩形是否可以完全覆盖一个大矩形。
     * 通过分析矩形的边向量来验证是否可能形成一个大矩形的边界。
     *
     * @param rectangles 二维数组，每个子数组表示一个矩形的左下角和右上角坐标[x1, y1, x2, y2]。
     * @return 如果可以完全覆盖大矩形返回true，否则返回false。
     */
    public boolean isRectangleCover(int[][] rectangles) {
        // 存储所有矩形对象
        List<Rectangle> rectangleList = new ArrayList<>();
        // 初始化大矩形的四个边界
        int maxX = 0;
        int maxY = 0;
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        // 遍历矩形数组，更新大矩形的边界和矩形列表
        for (int[] rectangle : rectangles) {
            minX = Math.min(minX, rectangle[0]);
            minY = Math.min(minY, rectangle[1]);
            maxX = Math.max(maxX, rectangle[2]);
            maxY = Math.max(maxY, rectangle[3]);
            Rectangle rectangleC = new Rectangle(new Point(rectangle[0], rectangle[1]), new Point(rectangle[2], rectangle[3]));
            rectangleList.add(rectangleC);
        }
        // 创建外层最大矩形对象
        Rectangle biggestR = new Rectangle(new Point(minX, minY), new Point(maxX, maxY));

        // 存储大矩形边界上的向量
        Map<Point, Vector> biggestEdgeVectors = new HashMap<>();
        // 存储其他非边界向量
        Map<Point, List<Vector>> otherVectors = new HashMap<>();
        // 遍历矩形列表，提取边向量
        for (Rectangle rectangle : rectangleList) {
            List<Vector> edgeVectors = rectangle.getEdgeVectors();
            for (Vector vector : edgeVectors) {
                Point start = vector.getStartPoint();
                // 判断是否为大矩形的边界向量
                if (biggestR.isEdgeVector(vector)) {
                    // 如果起点已存在边界向量，则不满足条件
                    if (biggestEdgeVectors.containsKey(start)) {
                        // 此时已经出现了重复的向量
                        return false;
                    } else {
                        biggestEdgeVectors.put(start, vector);
                    }
                } else {
                    // 存储非边界向量
                    List<Vector> vectors = otherVectors.computeIfAbsent(start, k -> new ArrayList<>());
                    vectors.add(vector);
                }
            }
        }

        // 从大矩形的从左下角开始计算外层最大矩形向量和
        Vector vector = biggestEdgeVectors.remove(new Point(minX, minY));
        if(vector == null)
            return false;
        while(vector != Vector.zero) {
            Vector tempAdd = biggestEdgeVectors.remove(vector.getEndPoint());
            if(tempAdd == null)
                return false;
            vector = vector.plus(tempAdd);
        }
        if(!biggestEdgeVectors.isEmpty()) {
            return false;
        }
        // 外层最大矩形处理完毕，最终向量为0向量，且没有多余向量

        // 处理内部向量
        while (!otherVectors.isEmpty()) {
            // 任意选择一个内部的顶点
            Point point = otherVectors.keySet().stream().findAny().get();
            // 获取以该顶点作为起点的向量
            Vector anyVector = otherVectors.get(point).remove(0);
            while (anyVector != Vector.zero) {
                // 到向量的终点
                Point end = anyVector.getEndPoint();
                // 继续寻找是否还有能够相加的向量
                List<Vector> toAdd = otherVectors.get(end);
                if(toAdd == null)
                    return false;
                boolean ril = false;
                // 尝试抵消其他向量，形成闭合
                for (int i = 0, toAddSize = toAdd.size(); i < toAddSize; i++) {
                    Vector tempAdd = toAdd.get(i);
                    // 仅允许抵消一条直线上的向量，正反向均可相加
                    if (anyVector.isInverse(tempAdd) || anyVector.getCrossProductInt(tempAdd) == 0) {
                        toAdd.remove(i);
                        // 抵消的向量从记录中移除
                        anyVector = anyVector.plus(tempAdd);
                        ril = true;
                        break;
                    }
                }
                // 对于已经没有向量出发的顶点，从记录中移除
                if(toAdd.isEmpty())
                    otherVectors.remove(end);
                if(!ril)
                    // 当前向量无法继续抵消
                    return false;
            }
            // 对于已经没有向量出发的顶点，从记录中移除
            if(otherVectors.get(point).isEmpty())
                otherVectors.remove(point);
        }
        // 如果所有向量都能互相抵消，说明可以完全覆盖大矩形
        return true;
    }


}
