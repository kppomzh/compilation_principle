package com.zhzm;

import org.junit.Test;

public class class_1437 {
    @Test
    public void test() {
        int[] nums = {1,0,0,0,1,0,0,1};
        int k = 2;
        System.out.println(kLengthApart(nums, k));
    }
    public boolean kLengthApart(int[] nums, int k) {
        boolean res = true;
        k = k+1;
        int lastIdx = -k;
        for(int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                if (i - lastIdx >= k) {
                    lastIdx = i;
                } else {
                    res = false;
                    break;
                }
            }
        }
        return res;
    }
}
