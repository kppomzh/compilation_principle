package com.zhzm;

public class class_112 {
    public static void main(String[] args) {
        class_112 class112=new class_112();
        class_297 class297=new class_297();
        TreeNode root=new TreeNode(1);
        {
            TreeNode tmp=new TreeNode(-2);
            root.left=tmp;
            tmp.left = new TreeNode(1);
            tmp = tmp.left;
            tmp.left=new TreeNode(-1);
            tmp.right=new TreeNode(2);
        }
        System.out.println(class112.hasPathSum(root,-1));
    }
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if(root==null)
            return false;
        else if(root.left==null&&root.right==null){
            return targetSum==root.val;
        } else {
            boolean res=false;
            if(root.left!=null)
                res|=hasPathSum(root.left,targetSum-root.val);
            if(root.right!=null)
                res|=hasPathSum(root.right,targetSum-root.val);
            return res;
        }
    }
}
