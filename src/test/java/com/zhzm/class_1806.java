package com.zhzm;

public class class_1806 {
    public static void main(String[] ar){
        class_1806 c1806=new class_1806();
        for (int i = 2; i <= 128; i+=2) {
            System.out.println(c1806.reinitializePermutation1(i));
        }

    }

    public int reinitializePermutation(int n) {
        int[] arr=new int[n];
        int res=1;
        for(int i=0;i<n;i+=2){
            arr[i] = i/2;
//            if(arr[i]!=i)
                res++;
        }
        arr[1] = n/2;
        for(int i=3;i<n;i+=2){
            arr[i] = (i-1)/2+n/2;
//            if(arr[i]!=i)
                res++;
        }

        for (int i = 0; i < n; i++) {
            System.out.print(arr[i]);
            System.out.print(',');
        }
        return res;
    }

    int reinitializePermutation1(int n) {
        int pos = 1, ans = 0;
        do{
            pos = pos%2!=0 ? n / 2 + (pos - 1) / 2 : pos / 2;
            ++ans;
        }while(pos != 1);
        return ans;
    }
}
