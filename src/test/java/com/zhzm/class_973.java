package com.zhzm;

// 最接近原点的 K 个点

import com.zhzm.structure.geometry.Point;

import java.util.ArrayList;
import java.util.List;

public class class_973 {
    public int[][] kClosest(int[][] points, int k) {
        List<Point> pointList = new ArrayList<>();
        pointList.add(new Point(points[0][0], points[0][1]));
        for (int i = 1; i < points.length; i++) {
            Point p = new Point(points[i][0], points[i][1]);
            pointList.add(binarySearchInsertPosition(pointList, p), p);
        }
        return pointList.subList(0, k).parallelStream().map(Point::toArray).toArray(a -> new int[points.length][]);
    }

    public static int binarySearchInsertPosition(List<Point> list, Point target) {
        int left = 0;
        int right = list.size() - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;
            int comp = list.get(mid).compareTo(target);
            if (comp == 0) {
                return mid; // 如果找到了目标值，返回其索引
            } else if (comp < 0) {
                left = mid + 1; // 目标值在mid的右侧
            } else {
                right = mid - 1; // 目标值在mid的左侧
            }
        }

        // 如果没找到目标值，left就是目标值应该插入的位置
        return left;
    }

        public static void main(String[] args) {
            String s = "String s='%s\\nSystem.out.println(s.formatted(s));'";
            System.out.println(s.formatted(s));
        }

}
