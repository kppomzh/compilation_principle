package com.zhzm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LCP_18 {
    public static void main(String[] args) {
        LCP_18 lcp18=new LCP_18();
        System.out.println(lcp18.breakfastNumber2(new int[]{10,20,5},new int[]{5,5,2},15));
    }
    public int breakfastNumber(int[] staple, int[] drinks, int x) {
        int res = 0;

        int[] staPriceMap = new int[x+1];
        for (int i = 0; i < staple.length; i++) {
            if(staple[i]<x) {
                staPriceMap[staple[i]]++;
            }
        }

        Arrays.sort(drinks);
        for (int i = 1; i < x+1; i++) {
            if(staPriceMap[i]>0) {
                int drinkPrice = x - i;
                int dCount = getLessDrinkPriceCouunt(drinks, drinkPrice);
                res += staPriceMap[i] * dCount;
                res %= 1000000007;
            }
        }
        return res;
    }
    public int breakfastNumber2(int[] staple, int[] drinks, int x) {
        int res = 0;

        int[] staPriceMap = new int[x+1];
        for (int i = 0; i < staple.length; i++) {
            if(staple[i]<x) {
                staPriceMap[staple[i]]++;
            }
        }

        for (int i = 2; i < staPriceMap.length; i++) {
            staPriceMap[i]+=staPriceMap[i-1];
        }
        for (int num : drinks) {
            if (x > num)
                res += staPriceMap[x-num];
            if (res > 1000000007)
                res -= 1000000007;
        }
        return res;
    }

    private int getLessDrinkPriceCouunt(int[] array, int limit) {
        int left = 0;
        int right = array.length - 1;
        int res = -1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (array[middle] > limit) {
                if (middle == 0 || array[middle - 1] <= limit) {
                    res = middle;
                    break;
                } else
                    right = middle - 1;
            } else { //if(array[middle]<limit)
                if (middle == array.length - 1 || array[middle + 1] > limit) {
                    res = middle + 1;
                    break;
                }
                else
                    left = middle + 1;
            }
        }
        return res;
    }
}
