package com.zhzm;

import com.zhzm.structure.map.ChainedForwardStar;
import com.zhzm.structure.object.MultiTreeNode;
import org.junit.Test;

import java.util.*;

public class class_1766 {
    @Test
    public void test() {
        int[] nums = {2,3,3,2};
        int[][] edges = {{0,1},{1,2},{1,3}};
        System.out.println(Arrays.toString(getCoprimes(nums, edges)));
    }

    @Test
    public void test1() {
        int[] nums = new int[100000];
        int[][] edges = new int[99999][2];
        nums[99999] = 6;
        for (int i = 0; i < edges.length; i++) {

        }
        System.out.println(Arrays.toString(getCoprimes(nums, edges)));
    }

    public int[] getCoprimes(int[] nums, int[][] edges) {
        ChainedForwardStar cfs = new ChainedForwardStar(edges.length + 1);
        for (int[] edge : edges) {
            cfs.addEdge(edge[0], edge[1]);
            cfs.addEdge(edge[1], edge[0]);
        }
        int[] res = new int[nums.length];
        dfs(new Stack<>(), res, cfs, 0, nums);
        return res;
    }

    private void dfs(Stack<Integer> parIdxStack, int[] res, ChainedForwardStar cfs, Integer nowIdx, int[] nums) {
        int nodeRes = -1;
        ListIterator<Integer> iterator = parIdxStack.listIterator(parIdxStack.size());
        while (iterator.hasPrevious()) {
            int parIdx = iterator.previous();
            int parVal = nums[parIdx];
            int val = nums[nowIdx];
            int a = Math.max(val, parVal);
            int b = Math.min(val, parVal);
            if(a % b == 0)
                continue;
            if(1 == gcd(a, b)) {
                nodeRes = parIdx;
                break;
            }
        }
        int parIdx = !parIdxStack.isEmpty() ? parIdxStack.peek() : -1;
        parIdxStack.push(nowIdx);
        for (Integer childIdx : cfs.getNextVal(parIdx, nowIdx)) {
            dfs(parIdxStack, res, cfs, childIdx, nums);
        }
        parIdxStack.pop();
        res[nowIdx] = nodeRes;
    }

    private void dfs(Stack<MultiTreeNode> stack, MultiTreeNode node, int[] res) {
        int nodeRes = -1;
        ListIterator<MultiTreeNode> iterator = stack.listIterator(stack.size());
        while (iterator.hasPrevious()) {
            MultiTreeNode par = iterator.previous();
            int parVal = par.getVal();
            int parIdx = par.getIdx();
            int a = Math.max(node.getVal(), parVal);
            int b = Math.min(node.getVal(), parVal);
            if(1 == gcd(a, b)) {
                nodeRes = parIdx;
                break;
            }
        }
        stack.push(node);
        for (MultiTreeNode child : node.getChild()) {
            dfs(stack, child, res);
        }
        stack.pop();
        res[node.getIdx()] = nodeRes;
    }

    private int gcd(int a, int b) { // a>b
        return b != 0 ? gcd(b, a % b) : a;
    }
}
