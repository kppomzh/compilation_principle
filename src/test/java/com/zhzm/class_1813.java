package com.zhzm;

public class class_1813 {
    public static void main(String[] args) {
        class_1813 class1813=new class_1813();
        System.out.println(class1813.areSentencesSimilar("My name is Haley","MyHaley"));
        System.out.println(class1813.areSentencesSimilar("My name is Haley","My Haley"));
        System.out.println(class1813.areSentencesSimilar("if","what if"));
        System.out.println(class1813.areSentencesSimilar("what","what if"));
        System.out.println(class1813.areSentencesSimilar("MyH abc aley","MyH aley"));

        System.out.println(class1813.areSentencesSimilar("M","MyH aley"));
        System.out.println(class1813.areSentencesSimilar("My","MyH aley"));
        System.out.println(class1813.areSentencesSimilar("of","of"));
        System.out.println(class1813.areSentencesSimilar("what if","what if s"));
    }
    public boolean areSentencesSimilar(String sentence1, String sentence2) {
        String[] s1=sentence1.split(" "),s2=sentence2.split(" "),
                source,target;
        if(s1.length>s2.length){
            source=s2;
            target=s1;
        } else {
            source=s1;
            target=s2;
        }

        if(source.length==1){
            return target[0].equals(source[0])||target[target.length-1].equals(source[0]);
        }
        int index=0;
        for (; index < source.length; index++) {
            if(!source[index].equals(target[index])) {
                index--;
                break;
            }
        }
        int lindex=source.length-1;
        for (int i=target.length-1; lindex > index ; lindex--,i--) {
            if(!source[lindex].equals(target[i]))
                break;
        }

        return lindex<=index;

    }
}
