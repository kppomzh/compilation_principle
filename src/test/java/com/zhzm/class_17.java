package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_17 {
    private static char[][] carray=new char[][]{{'a','b','c'},{'d','e','f'},{'g','h','i'},{'j','k','l'},{'m','n','o'},{'p','q','r','s'},{'t','u','v'},{'w','x','y','z'}};
    List<String> combinations;

    @Test
    public void test(){
        long start=System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            letterCombinations2("3759");
        }
        System.out.println((System.currentTimeMillis()-start)/1000d);
        start=System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            letterCombinations("3759");
        }
        System.out.println((System.currentTimeMillis()-start)/1000d);
    }

    public List<String> letterCombinations2(String digits) {
        combinations = new ArrayList<>();
        if (digits.isEmpty()) {
            return combinations;
        }
        linkStr(digits, 0, new StringBuffer());
        return combinations;
    }

    public void linkStr(String digits, int index, StringBuffer combination) {
        if (index == digits.length()) {
            combinations.add(combination.toString());
        } else {
            char digit = digits.charAt(index);
            char[] letters=carray[digit-50];
            int lettersCount = letters.length;
            for (int i = 0; i < lettersCount; i++) {
                combination.append(letters[i]);
                linkStr(digits, index + 1, combination);
                combination.deleteCharAt(index);
            }
        }
    }

    public List<String> letterCombinations(String digits) {
        List<String> combinations = new ArrayList<String>();
        if (digits.length() == 0) {
            return combinations;
        }
        Map<Character, String> phoneMap = new HashMap<Character, String>() {{
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }};
        backtrack(combinations, phoneMap, digits, 0, new StringBuffer());
        return combinations;
    }

    public void backtrack(List<String> combinations, Map<Character, String> phoneMap, String digits, int index, StringBuffer combination) {
        if (index == digits.length()) {
            combinations.add(combination.toString());
        } else {
            char digit = digits.charAt(index);
            String letters = phoneMap.get(digit);
            int lettersCount = letters.length();
            for (int i = 0; i < lettersCount; i++) {
                combination.append(letters.charAt(i));
                backtrack(combinations, phoneMap, digits, index + 1, combination);
                combination.deleteCharAt(index);
            }
        }
    }
}
