package com.zhzm;

import java.util.HashMap;
import java.util.Map;

public class class_1797 {
}

class AuthenticationManager {
    Map<String,Integer> tokenMap=new HashMap<>();
    int timeToLive;

    public AuthenticationManager(int timeToLive) {
        this.timeToLive=timeToLive;
    }

    public void generate(String tokenId, int currentTime) {
        tokenMap.put(tokenId,currentTime+timeToLive);
    }

    public void renew(String tokenId, int currentTime) {
        if(tokenMap.containsKey(tokenId)){
            int endTime=tokenMap.get(tokenId);
            if(endTime>currentTime){
                tokenMap.put(tokenId,currentTime+timeToLive);
            }
        }
    }

    public int countUnexpiredTokens(int currentTime) {
        int res=0;
        for (int endTime: tokenMap.values()){
            if(endTime>currentTime)
                res++;
        }
        return res;
    }
}
