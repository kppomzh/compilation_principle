package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class class_316 {

    @Test
    public void test() {
        System.out.println(removeDuplicateLetters("acbdcec"));
    }

    public String removeDuplicateLetters(String s) {
        Stack<Character> stack = new Stack<>();
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(set.contains(c)) {
                if(c > stack.peek()) {
                    Stack<Character> temp = new Stack<>();
                    while(true) {
                        char tc = stack.pop();
                        if(tc == c)
                            break;
                        temp.push(tc);
                    }
                    while (!temp.empty())
                        stack.push(temp.pop());
                    stack.push(c);
                }
            } else {
                set.add(c);
                stack.push(c);
            }
        }
        return Arrays.toString(stack.toArray());
    }
}
