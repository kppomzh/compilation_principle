package com.zhzm;

import java.util.Stack;

public class class_678 {
    public static void main(String[] args) {
//        String msg="(((((*(*********((*(((((****";
        String msg="(((((*(()((((*((**(((()()*)()()()*((((**)())*)*)))))))(())(()))())((*()()(((()((()*(())*(()**)()(())";
        class_678 class678=new class_678();
        System.out.println(class678.checkValidString(msg));
    }
    public boolean checkValidString(String s) {
        int leftbucket=0,star=0;
        Stack<Integer> stack=new Stack<>(),tmp=new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            switch(s.charAt(i)){
                case '(':
//                    leftbucket++;
                    stack.push(0);
                    break;
                case '*':
//                    star++;
                    stack.push(1);
                    break;
                case ')':
//                    if(leftbucket>0)
//                        leftbucket--;
//                    else if(star>0)
//                        star--;
//                    else
//                    return false;
                    if(stack.isEmpty())
                        return false;
                    else {
                        while (!stack.isEmpty() && stack.peek() == 1) {
                            tmp.push(stack.pop());
                        }
                        if (stack.isEmpty()) {
                            tmp.pop();
                        } else {
                            stack.pop();
                        }
                        stack.addAll(tmp);
                        tmp.removeAllElements();
                    }
            }
        }

        while (!stack.isEmpty()) {
            if(stack.peek() == 1)
                tmp.push(stack.pop());
            else if(!tmp.isEmpty()){
                stack.pop();
                tmp.pop();
            } else {
                return false;
            }
        }
        return true;
    }
}
