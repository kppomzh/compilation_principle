package com.zhzm;

import org.junit.Assert;
import org.junit.Test;

// 最长有效括号
public class class_32 {

    @Test
    public void test() {
        Assert.assertEquals(2, longestValidParentheses("(()"));
        Assert.assertEquals(4, longestValidParentheses(")()())"));
        Assert.assertEquals(0, longestValidParentheses(""));
        Assert.assertEquals(2, longestValidParentheses("()(()"));
    }

    public int longestValidParentheses(String s) {
        int left = 0;
        int right = 0;
        int leftInv = 0;
        int rightInv = 0;
        int res = 0;
        for (int i = 0, j = s.length()-1; i < s.length(); i++, j--) {
            if (s.charAt(i) == '(')
                left++;
            else {
                right++;
                if(left == right)
                    res = Math.max(left, res);
                else if(left<right) {
                    left = 0;
                    right = 0;
                }
            }

            if(s.charAt(j) == ')')
                rightInv++;
            else {
                leftInv++;
                if(rightInv == leftInv)
                    res = Math.max(rightInv, res);
                else if(rightInv<leftInv) {
                    leftInv = 0;
                    rightInv = 0;
                }
            }
        }
        return res<<1;
    }
}
