package com.zhzm;

public class class_2016 {
    public static void main(String[] args) {
        class_2016 class2016=new class_2016();
        System.out.println(class2016.maximumDifference2(new int[]{1,5,2,10}));
        System.out.println(class2016.maximumDifference2(new int[]{100,50,50,10}));
    }
    public int maximumDifference(int[] nums) {
        int res=0,tmpRes=0,min=Integer.MAX_VALUE,idx=0;
        boolean find=false;
        for (; idx < nums.length - 1; idx++) {
            if(nums[idx+1]-nums[idx]>0){
                res=nums[idx+1]-nums[idx];
                find=true;
                min=nums[idx];
                idx++;
                break;
            }
        }
        for (; idx < nums.length - 1; idx++) {
            if(nums[idx+1]-nums[idx]>0){
                tmpRes=nums[idx+1]-min;
            } else {
                res=Math.max(res,tmpRes);
                tmpRes=0;
                min=Math.min(min,nums[idx+1]);
            }
        }
        return find?Math.max(res,tmpRes):-1;
    }

    public int maximumDifference2(int[] nums) {
        int res=-1,tmpRes=-1,min=nums[0],idx=0;
        for (; idx < nums.length - 1; idx++) {
            if(nums[idx+1]-nums[idx]>0){
                tmpRes=nums[idx+1]-min;
            } else {
                res=Math.max(res,tmpRes);
                tmpRes=-1;
                min=Math.min(min,nums[idx+1]);
            }
        }
        return Math.max(res,tmpRes);
    }


}
