package com.zhzm;

import org.junit.Test;

public class class_45 {

    @Test
    public void test1() {
        System.out.println(jump1(new int[]{2,3,1,3,3,3,3,3}));
    }
    @Test
    public void test2() {
        System.out.println(jump(new int[]{2,3,1,1,4}));
    }
    @Test
    public void test3() {
        System.out.println(jump(new int[]{1,3,2}));
    }
    @Test
    public void test4() {
        System.out.println(jump(new int[]{3,2,1}));
    }
    @Test
    public void test5() {
        System.out.println(jump(new int[]{1,1,1,1,1}));
    }
    @Test
    public void test6() {
        System.out.println(jump(new int[]{1,2,1,1,1}));
    }
    @Test
    public void test7() {
        System.out.println(jump1(new int[]{0}));
    }

    public int jump(int[] nums) {
        int idx = 0;
        int res = nums.length==1?0:1;
        while(idx + nums[idx] < nums.length - 1) {
            int maxStepIdx = idx+1;
            for (int i = idx+2; i <= idx+nums[idx]; i++) {
                if(nums[i] >= nums[maxStepIdx] - (i-maxStepIdx)) {
                    maxStepIdx = i;
                }
            }
            idx = maxStepIdx;
            res++;
        }
        return res;
    }

    public int jump1(int[] nums) {
        int length = nums.length;
        int end = 0;
        int maxPosition = 0;
        int steps = 0;
        for (int i = 0; i < length - 1; i++) {
            maxPosition = Math.max(maxPosition, i + nums[i]);
            if (i == end) {
                end = maxPosition;
                steps++;
            }
        }
        return steps;
    }

//    作者：力扣官方题解
//    链接：https://leetcode.cn/problems/jump-game-ii/solutions/230241/tiao-yue-you-xi-ii-by-leetcode-solution/
//    来源：力扣（LeetCode）
//    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
}
