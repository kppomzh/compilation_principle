package com.zhzm;

import com.zhzm.structure.geometry.SimpleLine;
import com.zhzm.structure.geometry.XAxis;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

// 天际线问题
public class class_218 {
    @Test
    public void test() {
        System.out.println(getSkyline(new int[][]{{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}}));
        System.out.println(getSkyline(new int[][]{{0,2,3}, {2,5,3}}));
//        System.out.println(getSkyline(new int[][]{{3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {0,2147483647,2147483647}, {19, 24, 8}}));
    }

    public List<List<Integer>> getSkyline(int[][] buildings) {
        XAxis xAxis = new XAxis();
        SimpleLine[] buildingTop = new SimpleLine[buildings.length];
        for (int i = 0; i < buildings.length; i++) {
            buildingTop[i] = new SimpleLine(buildings[i][0], buildings[i][2], buildings[i][1], buildings[i][2]);
        }
        Arrays.sort(buildingTop, Comparator.comparingInt(SimpleLine::offAxis));
        for (int i = 0; i < buildingTop.length; i++) {
            xAxis.projectedLine(buildingTop[i]);
        }
        return xAxis.getLeftRiseTrailEdge();
    }
}
