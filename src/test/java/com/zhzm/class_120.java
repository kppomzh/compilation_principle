package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// 120. 三角形最小路径和
public class class_120 {
    @Test
    public void test1() {
        List<List<Integer>> triangle = List.of(
                new ArrayList<>(List.of(2)),
                new ArrayList<>(List.of(3,4)),
                new ArrayList<>(List.of(6,5,7)),
                new ArrayList<>(List.of(4,1,8,3)));
        System.out.println(minimumTotal(triangle));
    }
    @Test
    public void test2() {
        List<List<Integer>> triangle = List.of(
                new ArrayList<>(List.of(-1)),
                new ArrayList<>(List.of(3,4)),
                new ArrayList<>(List.of(1,-1,-3)));
        System.out.println(minimumTotal(triangle));
    }
    @Test
    public void test3() {
        List<List<Integer>> triangle = List.of(
                new ArrayList<>(List.of(-7))
                ,new ArrayList<>(List.of(-2,1))
               ,new ArrayList<>(List.of(-5,-5,9))
                ,new ArrayList<>(List.of(-4,-5,4,4))
                ,new ArrayList<>(List.of(-6,-6,2,-1,-5))
                ,new ArrayList<>(List.of(3,7,8,-3,7,-9))
        );
        System.out.println(minimumTotal(triangle));
    }

    private int minimumTotal(List<List<Integer>> triangle) {
        int[] cache = new int[triangle.size()];
        cache[0] = triangle.get(0).get(0);
        for (int i = 1; i < triangle.size(); i++) {
            List<Integer> line = triangle.get(i);
            int temp = cache[0];
            cache[0] += line.get(0);
            for (int j = 1; j < line.size() - 1; j++) {
                int step = Math.min(temp, cache[j]) + line.get(j);
                temp = cache[j];
                cache[j] = step;
            }
            cache[i] = line.get(i) + temp;
        }
        return Arrays.stream(cache).reduce(Integer::min).getAsInt();
    }

    public int minimumTotal3(List<List<Integer>> triangle) {
        for (int i = 1; i < triangle.size(); i++) {
            List<Integer> lastLine = triangle.get(i-1);
            List<Integer> line = triangle.get(i);
            int bef = lastLine.get(0);
            line.set(0, bef+line.get(0));
            for (int j = 1; j < line.size() - 1; j++) {
                int now = lastLine.get(j);
                line.set(j, Math.min(now, bef)+line.get(j));
                bef = now;
            }
            line.set(line.size() - 1, bef+line.get(line.size() - 1));
        }
        int res = triangle.get(triangle.size() - 1).get(0);
        for (Integer i : triangle.get(triangle.size() - 1)) {
            res = Math.min(res, i);
        }
        return res;
    }
}
