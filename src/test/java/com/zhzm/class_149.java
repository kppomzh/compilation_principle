package com.zhzm;

import com.zhzm.structure.geometry.EquationLine;
import com.zhzm.structure.geometry.Point;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class class_149 {
    @Test
    public void test1() {
        int[][] points = {{1, 1}, {3, 2}, {5, 3}, {4, 1}, {2, 3}, {1, 4}};
        System.out.println(maxPoints(points));
    }

    @Test
    public void test2() {
        int[][] points = {{3, 3}, {5, 3}, {-3, 3}};
        System.out.println(maxPoints(points));
    }

    public int maxPoints(int[][] points) {
        if (points.length < 3)
            return points.length;
        List<Point> pointList = new ArrayList<>(IntStream.range(0, points.length).parallel().boxed().
                map(i -> points[i]).map(arr -> new Point(arr[0], arr[1])).toList());
        Map<EquationLine, Integer> count = new HashMap<>();
        while (!pointList.isEmpty()) {
            Point p = pointList.removeFirst();
            pointList.forEach(n -> {
                Integer num = count.computeIfAbsent(EquationLine.getLine(n, p), k -> 0);
                count.put(EquationLine.getLine(n, p), num + 1);
            });
        }
        System.out.println(count.entrySet());
        return (int) Math.sqrt(2 * count.values().parallelStream().max(Integer::compareTo).orElse(0)) + 1;
    }
}
