package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_442 {

    @Test
    public void test() {
        List<Integer> res = findDuplicates(new int[]{4,3,2,7,8,2,3,1});
        System.out.println(res);
    }

    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> res = new ArrayList<>();
        for(int i : nums) {
            int idx = Math.abs(i)-1;
            if (nums[idx] > 0) {
                nums[idx] = -nums[idx];
            } else {
                res.add(idx + 1);
            }
        }
        return res;
    }

    public List<Integer> findDuplicates1(int[] nums) {
        Map<Integer, Integer> mapRes = new HashMap<>();
        for(int i : nums) {
            mapRes.put(i, mapRes.getOrDefault(i, 0) + 1);
        }
        return mapRes.entrySet().stream().filter(item -> item.getValue() == 2).map(Map.Entry::getKey).toList();
    }
}
