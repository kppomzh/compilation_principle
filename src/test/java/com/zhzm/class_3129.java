package com.zhzm;

// 找出所有稳定的二进制数组 I
public class class_3129 {
    public int numberOfStableArrays(int zero, int one, int limit) {
        int k = zero + one;
        int minSize = k / (limit + 1);
        if(zero < minSize || one < minSize)
            return 0;
        int patchSize = k - minSize;
//        int subPatch = patchSize - minSize;
        int res = getComb(patchSize);
//        for (int i = minSize; i < patchSize; i++) {
//            res += (i + 1 - (k % (i + 1)));
//        }
        return res;
    }

    public int getComb(int n) {
        int[] res = new int[n];
        int sum = 1;
        res[0] = 1;
        for (int i = 1; i < n; i++) {
            res[i] = (n - i) * res[i - 1] / i;
            sum += res[i];
        }
        return sum;
    }

    public int cnk(int n, int k) {
        k = Math.min(k, n-k);
        int res;
        for (int i = 0; i < n; i++) {

        }
        return 0;
    }
}
