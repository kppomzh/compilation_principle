package com.zhzm;

import java.time.chrono.MinguoEra;

public class class_1625 {
    public String findLexSmallestString(String s, int a, int b) {
        StringBuilder sb=new StringBuilder();
        sb.append(s);

        return sb.toString();
    }

    private int add(StringBuilder sb,int add){
        int minIdx=-1,min=Integer.MAX_VALUE;
        for (int i = 1; i < sb.length(); i+=2) {
            char c=sb.charAt(i);
            c+=add;
            if(c>57)
                c-=10;
            if(c<min){
                min=c;
                minIdx=i;
            }
            sb.setCharAt(i,c);
        }
        return minIdx;
    }

    private void loop(StringBuilder sb,int loop){
        String prefix=sb.substring(0,loop);
        sb.delete(0,loop).append(prefix);
    }

    private int findMin(StringBuilder sb,int loop){
        int minIdx=0,min=sb.charAt(0);
        for (int i = loop; i < sb.length(); i+=loop) {
            if(min>sb.charAt(i)){
                min=sb.charAt(i);
                minIdx=i;
            }
        }
        return minIdx/loop;
    }
}
