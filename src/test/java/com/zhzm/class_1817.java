package com.zhzm;

import java.util.*;

public class class_1817 {
    public static void main(String[] args) {
        class_1817 class1817=new class_1817();
        class1817.findingUsersActiveMinutes2(new int[][]{{0,5},{1,2},{0,2},{0,5},{1,3}},5);
    }
    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        int[] res=new int[k];
        Arrays.sort(logs, Comparator.comparingInt(o->o[0]));

        int key=logs[0][0];
        Set<Integer> map=new HashSet<>();
        for (int i = 0; i < logs.length; i++) {
            if(logs[i][0]!=key){
                res[map.size()-1]++;

                key=logs[i][0];
                map.clear();
                map=new HashSet<>();
            }
            map.add(logs[i][1]);
        }
        res[map.size()-1]++;

        return res;
    }

    public int[] findingUsersActiveMinutes2(int[][] logs, int k) {
        int[] res=new int[k];
//        Arrays.sort(logs, Comparator.comparingInt(o->o[0]));

//        int key=logs[0][0];
        Map<Integer,Set<Integer>> map=new HashMap<>();
        for (int i = 0; i < logs.length; i++) {
            Set<Integer> toAppend;
            if(map.containsKey(logs[i][0]))
                toAppend=map.get(logs[i][0]);
            else {
                toAppend=new HashSet<>();
                map.put(logs[i][0],toAppend);
            }
            toAppend.add(logs[i][1]);
        }

        for (Set<Integer> toAppend:map.values())
            res[toAppend.size()-1]++;

        return res;
    }
}
