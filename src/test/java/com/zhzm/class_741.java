package com.zhzm;

import java.util.*;

public class class_741 {
    public int cherryPickup(int[][] grid) {
        return move(grid, 0, 0);
    }

    private int move(int[][] grid, int startX, int startY) {
        if(grid[startX][startY] == -1) {
            return 0;
        }
        int xRes = move(grid, startX + 1, startY);
        if(xRes > 0) {
            
        } else {
            int yRes = move(grid, startX, startY + 1);
        }
        return xRes;
    }

    public int cherryPickup1(int[][] grid) {
        Map<Boolean, Set<block>> initBlocks = initAllBlocks(grid);
        Set<block> allBlocks = initBlocks.get(true);
        Set<block> edgeBlocks = initBlocks.get(false);
        Set<block> obsBlocks = new HashSet<>();

        return 0;
    }

    /* 构建所有地块 */
    private Map<Boolean, Set<block>> initAllBlocks(int[][] grid) {
        Set<block> allBlocks = new HashSet<>();
        Set<block> edgeBlocks = new HashSet<>();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                block b = block.build(i, j);
                allBlocks.add(b);
                if (i == 0 || j==0 || i == grid.length - 1 || j == grid.length - 1)
                    edgeBlocks.add(b);
            }
        }
        return Map.of(true, allBlocks, false, edgeBlocks);
    }

    /* 构建初始的边缘地块 */
    private Set<block> initEdgeBlocks(Set<block> allBlocks) {
        return Collections.EMPTY_SET;
    }

    static class block {
        private int x;
        private int y;

        private block(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static block build(int x, int y) {
            return new block(x, y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof block)
                return ((block)obj).x == this.x && ((block)obj).y == this.y;
            else
                return false;
        }
    }
}
