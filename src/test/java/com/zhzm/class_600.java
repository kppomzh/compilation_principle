package com.zhzm;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class class_600 {
    @Test
    public void test() {
//        for (int i = 1; i < 16; i += 1) {
//            System.out.println("i: " + i + ",res: " + findIntegers(i));
//        }
//        System.out.println("i: 511 ,res: " + findIntegers(511));
//        System.out.println("i: 255 ,res: " + findIntegers(255));
//        System.out.println("i: 63 ,res: " + findIntegers(63));
//        System.out.println("i: 35 ,res: " + findIntegers(35));
//        System.out.println("i: 62 ,res: " + findIntegers(62));
        System.out.println("i: 511 ,res: " + findIntegers(1000000000));
//        System.out.println("i: 511 ,res1: " + findIntegers1(1000000000));
    }

    private static Map<Integer, Integer> cache = new HashMap<>();

    static {
        cache.put(0, 1);
        cache.put(1, 2);
    }

    public int findIntegers(int n) {
        if (cache.containsKey(n))
            return cache.get(n);
        int size = n + 1;
        int mix = (int) (Math.log(size) / Math.log(2));
        int res;
        int subSize = 1 << mix;
        if (subSize != size) {
            res = findIntegers(subSize - 1);
            if(size - subSize > (subSize >> 1))
                res += findIntegers((subSize >> 1) - 1);
            else
                res += findIntegers(size - subSize - 1);
        } else {
            subSize = subSize >> 1;
            res = findIntegers(subSize - 1);
            res += findIntegers((subSize >> 1) - 1);
        }
        cache.put(n, res);
        return res;
    }

    public int findIntegers1(int n) {
        if (cache.containsKey(n + 1)) {
            return cache.get(n + 1);
        }
        int x = 2;
        int res;
        while (x < n + 1) {
            int tr = cache.get(x) + cache.get(x >> 1);
            x <<= 1;
            cache.put(x, tr);
        }
        return cache.get(n + 1);
    }

    public int findIntegers2(int n) {
        if (n == 1) {
            return 1;
        }
        double mix = (int) Math.floor(Math.log(n) / Math.log(2));
        double halfCount = Math.pow(2, mix - 1);
        int res = (int) (halfCount + 1);
        if (n >= halfCount * 3 - 1) {
            res += (int) halfCount;
        } else {
            res += (int) (n - halfCount * 2 + 1);
        }
        return res;
    }

}
