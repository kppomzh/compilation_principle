package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

// 2423. 删除字符使频率相同
public class class_2423 {

    @Test
    public void test() {
        equalFrequency("abc");
    }

    public boolean equalFrequency(String word) {
        int[] charArray = new int[26];
        HashSet<Integer> constant = new HashSet<>();
        for(int i=0; i<word.length(); i++) {
            charArray[word.charAt(i)-97]++;
            constant.add(word.charAt(i)-97);
        }
        Arrays.sort(charArray);
        for (int min = 0, max = charArray.length - 1; min < max; min++, max--) {
            int temp = charArray[min];
            charArray[min] = charArray[max];
            charArray[max] = temp;
        }
        int normal = charArray[0];
        int trans = charArray[1];
        for(int i=2; i<26; i++) {
            if(charArray[i] == 0)
                break;
            else if(normal == trans) {
                trans = charArray[i];
            } else {

            }
        }
        System.out.println(Arrays.toString(charArray));
        return trans == 1 || normal - trans == 1;
    }
}
