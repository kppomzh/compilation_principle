package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

// tag 快速幂
// 双模幂运算
public class class_2961 {
    @Test
    public void testQP() {
        System.out.println(quickPower(2, 3));
    }

    @Test
    public void test1() {
        System.out.println(getGoodIndices(new int[][]{{32,2,1000,100}}, 16));
    }

    @Test
    public void test2() {
        System.out.println(getGoodIndices(new int[][]{{2, 3, 3, 10}, {3, 3, 3, 1}, {6, 1, 1, 4}}, 2));
    }

    @Test
    public void test3() {
        System.out.println(getGoodIndices(new int[][]{{1,3,3,2}}, 0));
    }

    @Test
    public void test4() {
        System.out.println(getGoodIndices(new int[][]{{37,1,12,52}}, 1));
    }

    public List<Integer> getGoodIndices(int[][] variables, int target) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < variables.length; i++) {
            if(variables[i][0] % 10 == 0 && target == 0) {
                res.add(i);
                continue;
            }
            if(target == quickPower(quickPower(variables[i][0], variables[i][1]) % 10, variables[i][2]) % variables[i][3])
                res.add(i);
        }
        return res;
    }

    public int quickPower(int x, int n) {
        if(n == 0)
            return 1;
        int loop = 0;
        boolean[] p = new boolean[31];
        while (n > 0) {
            p[loop] = n % 2 == 1;
            n = n >> 1;
            loop++;
        }
        int res = 1;
        int tempX = x;
        for (int i = 0; i < loop; i++) {
            if(p[i]) {
                res *= tempX;
            }
            tempX *= tempX;
        }
        return res;
    }
}
