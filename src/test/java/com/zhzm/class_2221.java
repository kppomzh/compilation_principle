package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

// 数组的三角和
public class class_2221 {

    @Test
    public void test() {
        long time1 = 0l, time2 = 0l;
        for (int i = 0; i < 500; i++) {
            int[] nums = getNums(160+i);
            long start = System.currentTimeMillis();
            triangularSum(nums);
            time1 += System.currentTimeMillis() - start;

            start = System.currentTimeMillis();
            new Solution().triangularSum(nums);
            time2 += System.currentTimeMillis() - start;
        }
        System.out.println(time1);
        System.out.println(time2);
    }

    private int[] getNums(int n) {
        int[] res = new int[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            res[i] = Math.abs(r.nextInt()) % 10;
        }
//        System.out.println(Arrays.toString(res));
        return res;
    }

    /**
 * 计算一个数组的三角和。
 * 三角和是通过不断对数组进行特定规则的转换，直到数组长度小于26，然后将最终数组按照特定规则转换后求和。
 * 此方法的核心在于对数组的多次转换，每次转换将数组中连续的26个元素（或剩余元素）根据特定算法进行更新。
 *
 * @param nums 输入的整数数组。
 * @return 返回数组的三角和。
 */
public int triangularSum(int[] nums) {
    // 初始化数组长度
    int length = nums.length;

    // 当数组长度大于等于26时，进行转换操作
    while(length >= 26) {
        // 对数组中连续的26个元素（或剩余元素）进行转换
        for (int i = 0; i < length - 25; i++) {
            // 转换函数，具体实现是对数组中元素进行某种计算操作，并取结果的个位数
            nums[i] = simpleConv26(nums, i) % 10;
        }

        // 更新数组长度，减少已经处理过的25个元素
        length -= 25;
    }

    // 对最终长度小于26的数组进行转换后求和
    return simpleConvN(nums, length);
}


    /**
     * 计算给定数组的简单转换值。
     * 该方法通过与一个特定计算结果组合数组相乘，然后将乘积求和，最后取个位数，来计算给定数组的简单转换值。
     * 这种转换的具体算法细节由getComb方法生成的组合数组决定。
     *
     * @param nums 原始数组，其元素将与组合数组对应元素相乘并求和。
     * @param length 原始数组的长度，用于指示nums数组的大小。
     * @return 计算得到的简单转换值，为求和结果的个位数。
     */
    public int simpleConvN(int[] nums, int length) {
        // 获取特定长度的组合数组，该数组用于与原始数组元素相乘。
        int[] comb = getComb(length);
        // 初始化结果变量，用于累加乘积。
        int res = 0;
        // 遍历原始数组，将每个元素与对应位置的组合数组元素相乘，并累加到结果中。
        for(int i = 0; i < length; i++)
            res += nums[i] * (comb[i] % 10);
        // 取结果的个位数作为最终的转换值。
        return res % 10;
    }

    /**
     * 根据杨辉三角第25行的特定系数，对输入数组中的元素执行加权求和操作。
     *
     * 此函数利用了杨辉三角中第25行的系数作为权重，对数组中特定位置的元素进行加权求和。
     * 具体来说，它使用了以下系数：
     * - \( C(25, 1) \) 和 \( C(25, 24) \) 的值 25，
     * - \( C(25, 8) \) 和 \( C(25, 17) \) 的值 1081575，
     * - \( C(25, 9) \) 和 \( C(25, 16) \) 的值 2042975。
     *
     * 这些系数用于对数组中位于 start、start+1、start+8、start+9、start+16、start+17、start+24 和 start+25 位置的元素进行加权。
     *
     * @param nums 输入的整数数组。
     * @param start 数组中开始计算的索引位置。
     * @return 基于杨辉三角第25行系数的加权求和结果。
     */
public int simpleConv26(int[] nums, int start) {
    return nums[start]                   // 使用 \( C(25, 0) \) 和 \( C(25, 25) \) 的隐含系数 1
           + nums[start + 25]            // 使用 \( C(25, 0) \) 和 \( C(25, 25) \) 的隐含系数 1
           + 25 * (nums[start + 1] + nums[start + 24])   // 使用 \( C(25, 1) \) 和 \( C(25, 24) \)
           + 1081575 * (nums[start + 8] + nums[start + 17]) // 使用 \( C(25, 8) \) 和 \( C(25, 17) \)
           + 2042975 * (nums[start + 9] + nums[start + 16]); // 使用 \( C(25, 9) \) 和 \( C(25, 16) \)
}


    @Test
    public void testComb() {
        for (int i = 2; i <= 30; i++) {
            System.out.println(Arrays.toString(getComb(i)));
        }
    }
    /**
     * 根据给定的整数n计算组合数列，并返回一个包含从C(n, 0)到C(n, n)所有组合数的数组。
     * 组合数C(n, k) = n! / [k! * (n-k)!]，表示从n个不同元素中选取k个元素的组合方式数量。
     *
     * @param n 需要计算组合数的总元素数量，必须是非负整数。
     * @return 包含从C(n, 0)到C(n, n)所有组合数的数组。
     */
    public int[] getComb(int n) {
        // 初始化结果数组，长度为n，用于存储各个组合数的值
        int[] res = new int[n];

        // 因为C(n, 0)总是等于1，所以直接设置结果数组的第一个元素为1
        res[0] = 1;

        // 使用递推公式C(n, i) = C(n, i-1) * (n-i) / i来计算每个i对应的组合数，并存储在结果数组中
        for (int i = 1; i < n; i++) {
            res[i] = (n - i) * res[i-1] / i;
        }

        // 返回计算得到的结果数组
        return res;
    }


}

class Solution {
    static int N = 1000;
    static int[][] c = new int[N + 1][N + 1];

    public int triangularSum(int[] nums) {

        if(c[1][0] == 0){
            for (int n = 1; n <= N; ++n){
                for (int r = 0; r < n; ++r)
                    c[n][r] = r == 0 ? 1 : (c[n - 1][r - 1] + c[n - 1][r]) % 10;
            }
        }

        int n = nums.length;
        int ans = 0;
        for(int i = 0; i < n; ++i)
            ans += nums[i] * c[n][i] % 10;
        return ans % 10;
    }
}
class Solution2 {
    static int[][] c = new int[1001][1000];

    static {
        for (int n = 1; n <= 1000; n++) {
            // c[n] = new int[n];
            c[n][0] = 1;
            for (int r = 1; r < n - 1; r++)
                c[n][r] = (c[n - 1][r - 1] + c[n - 1][r]) % 10 ;
            c[n][n - 1] = 1;
        }
    }

    public int triangularSum(int[] nums) {
        int res = 0;
        for(int i = 0; i < nums.length; i++)
            res += (nums[i] * c[nums.length][i]) % 10;
        return res % 10;
    }
}