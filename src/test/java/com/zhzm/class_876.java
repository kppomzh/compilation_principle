package com.zhzm;

import com.zhzm.structure.object.ListNode;

// 链表的中间结点
public class class_876 {
    public ListNode middleNode(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while(fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }
}
