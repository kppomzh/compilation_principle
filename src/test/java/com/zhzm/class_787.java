package com.zhzm;

import java.util.*;

public class class_787 {
    public static void main(String[] args) {
        class_787 class787=new class_787();

//        System.out.println(class787.findCheapestPrice(3,new int[][]{{0, 1, 100}, {1, 2, 100},{0, 2, 500}},0 ,2 ,1));
//        System.out.println(class787.findCheapestPrice(5,new int[][]{{4,1,1},{1,2,3},{0,3,2},{0,4,10},{3,1,1},{1,4,3}},2,1,1 ));
        System.out.println(class787.findCheapestPrice(5,new int[][]{{4,2,1},{3,1,2},{1,4,1},{1,2,5},{0,1,5},{0,3,2}},0,2,2 ));
//        System.out.println(class787.findCheapestPrice(4,new int[][]{{0,1,1},{0,2,5},{1,2,1},{2,3,1}},0,3,1 ));
    }

    private int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {
        int distance=Integer.MAX_VALUE;
        int[] visitedDistance=new int[n+1];

        Arrays.fill(visitedDistance,Integer.MAX_VALUE);
        visitedDistance[src]=0;


        for (int i = 0; i <= k; i++) {
            int[] stepDistance=new int[n+1];
            Arrays.fill(stepDistance,Integer.MAX_VALUE);
            for (int[] flight:flights) {
                int from = flight[0];
                if(visitedDistance[from] != Integer.MAX_VALUE) {
                    int to = flight[1];
                    int srctodistance = visitedDistance[from] + flight[2];
                    stepDistance[to] = Math.min(stepDistance[to], srctodistance);
                }
            }
            visitedDistance=stepDistance;
            distance=Math.min(distance,visitedDistance[dst]);
        }

        return distance==Integer.MAX_VALUE?-1:distance;
    }

    public int findCheapestPrice2(int n, int[][] flights, int src, int dst, int k) {
        List<int[]>[] flightMap=new List[n];
//        Map<Integer, Integer> visitedDistance=new HashMap<>(); //the distance from src to map.key
        int[] visitedDistance=new int[n+1];
        List<int[]> nextFlights;
        for (int i = 0; i < n; i++) {
            flightMap[i]=new ArrayList<>();
        }

        for (int i = 0; i < flights.length; i++) {
            int from=flights[i][0];
            flightMap[from].add(flights[i]);
        }

        nextFlights=flightMap[src];
        flightMap[src]=Collections.emptyList();
        visitedDistance[src]=0;
        visitedDistance[dst]=-1;

        for (int i = 0; i <= k; i++) {
            if(!nextFlights.isEmpty()){
                List<int[]> tmpNextFlights=new ArrayList<>();
                Set<Integer> tmpNextCity=new HashSet<>();
//                Map<Integer, Integer> stepDistance=new HashMap<>();
                int[] stepDistance=new int[n+1];
                Arrays.fill(stepDistance,Integer.MAX_VALUE);

                for (int[] flight : nextFlights) {
                    int from = flight[0];
                    int to = flight[1];
                    int srctodistance = visitedDistance[from] + flight[2];
                    if (visitedDistance[to]!=0) {
                        int oldDis = Math.min(stepDistance[to],visitedDistance[to]);
                        if (srctodistance < oldDis)
                            stepDistance[to]=srctodistance;
                    } else {
                        stepDistance[to]=srctodistance;
                    }

                    if(!tmpNextCity.contains(to)){
                        tmpNextCity.add(to);
                        tmpNextFlights.addAll(flightMap[to]);
                    }
                }
                for (int j = 1; j < n + 1; j++) {
                    if(stepDistance[j]!=Integer.MAX_VALUE){
                        visitedDistance[j]=stepDistance[j];
                    }
                }
//                visitedDistance.putAll(stepDistance);
                nextFlights=tmpNextFlights;
            } else
                break;
        }

        return visitedDistance[dst];
    }
}
