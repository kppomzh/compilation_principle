package com.zhzm;

import java.util.concurrent.Executors;

public class rectest {
    public static void main(String[] args) {
        rectest rectest=new rectest();
        System.out.println(rectest.solutionAns("abc(qwe)def(asd)opq"));
        System.out.println(rectest.solution("abc(qwe)def(asd)opq"));
    }

    public String solutionAns(String inputString) {
        StringBuilder str = new StringBuilder(inputString);
        int start, end;
        while(str.indexOf("(") != -1){
            start = str.lastIndexOf("(");
            end = str.indexOf(")", start);
            str.replace(start, end + 1, new StringBuilder(str.substring(start+1, end)).reverse().toString());
        }
        return str.toString();
    }

    //Spring Boot技术栈的代码题测试用例有问题，当一段字符串中平行出现多个括号的情况下，例如："abc(qwe)def(asd)opq"，我的提交代码应该会出现问题，但是目前并没有被检测出。
    public String solution(String inputString) {
        StringBuilder sb=new StringBuilder();
        int start=0;

        for(int i=0;i<inputString.length();i++){
            if(inputString.charAt(i)=='('){
                sb.append(inputString.substring(start,i));
                int j=inputString.length()-1;
                for(;j>i;j--){
                    if(inputString.charAt(j)==')'){
                        sb.append(resolvedStr(inputString.substring(i+1,j)));
                        break;
                    }
                }
                i=j;
                start=j+1;
            }
        }
        sb.append(inputString.substring(start));

        return sb.toString();
    }

    private String resolvedStr(String inputString){
        StringBuilder sb=new StringBuilder();
        for(int i=inputString.length()-1;i>=0;i--){
            if(inputString.charAt(i)==')'){
                int j=0;
                for(;j<i;j++){
                    if(inputString.charAt(j)=='('){
                        sb.append(solution(inputString.substring(j+1,i)));
                        break;
                    }
                }
                i=j;
            } else
                sb.append(inputString.charAt(i));
        }

        return sb.toString();
    }
}
