package com.zhzm;

import java.util.*;

public class class_1851 {
    public static void main(String[] ar) {
        class_1851 c1851 = new class_1851();
        for (int res : c1851.minInterval(new int[][]{{1, 4}, {2, 4}, {3, 6}, {4, 4}}, new int[]{2, 3, 4, 5})) {
            System.out.println(res);
        }
    }

    public int[] minInterval(int[][] intervals, int[] queries) {
        int[] res = new int[queries.length];
        Arrays.fill(res, -1);

        int[][] quesort = new int[queries.length][2];
        for (int i = 0; i < queries.length; i++) {
            quesort[i][0] = queries[i];
            quesort[i][1] = i;
        }
        Arrays.sort(quesort, Comparator.comparingInt(o -> o[0]));
        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));

        PriorityQueue<int[]> queue = new PriorityQueue<>(Comparator.comparingInt(o -> (o[1] - o[0])));

        for (int i = 0, index = 0; i < queries.length; i++) {
            while(index < intervals.length && quesort[i][0] >= intervals[index][0]){
                queue.offer(intervals[index]);
                index++;
            }
            while(!queue.isEmpty() && queue.peek()[1] < quesort[i][0]) {
                queue.poll();
            }
            if(!queue.isEmpty()) {
                int[] t = queue.peek();
                res[quesort[i][1]] = t[1] - t[0] + 1;
            }
        }


        return res;
    }

    public int[] minInterval2(int[][] intervals, int[] queries) {
        int[] res = new int[queries.length];
        Arrays.fill(res, -1);

        Map<int[], Integer> lmap = new HashMap<>();
        Map<Integer, int[]> local = new HashMap<>();
        for (int i = 0; i < intervals.length; i++) {
            int length = intervals[i][1] - intervals[i][0] + 1;
            lmap.put(intervals[i], length);
            for (int j = intervals[i][0]; j <= intervals[i][1]; j++) {
                if (local.containsKey(j)) {
                    if (length < lmap.get(local.get(j)))
                        local.put(j, intervals[i]);
                } else {
                    local.put(j, intervals[i]);
                }
            }
        }

        for (int i = 0; i < queries.length; i++) {
            res[i] = local.containsKey(queries[i]) ? lmap.get(local.get(queries[i])) : -1;
        }

        return res;
    }

    public int[] minInterval3(int[][] intervals, int[] queries) {
        int[] res = new int[queries.length];
        Arrays.fill(res, -1);

        Map<Integer, Integer> local = new LinkedHashMap<>();
        PriorityQueue<Integer> queue=new PriorityQueue<>();
        for (int i = 0; i < queries.length; i++) {
            local.put(queries[i], -1);
            queue.add(queries[i]);
        }
        localArray<Integer> la=new localArray<>(queue);

        int idx=0;
        for (Integer i:local.values()){
            res[idx]=i;
            idx++;
        }
        return res;
    }

    class localArray<T>{
        private T[] array;
        public localArray(PriorityQueue<T> query){
            array= (T[]) query.toArray();
        }

        public T[] findRange(int left,int right){
            int llocal=0,rlocal=0;

            return Arrays.copyOfRange(array,llocal,rlocal);
        }
    }


}
