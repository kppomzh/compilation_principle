package com.zhzm;

public class class_217 {
    public boolean containsDuplicate(int[] nums) {

        int[] numAn = new int[nums.length];
        int n;
        for (int i = 0; i < nums.length; i++) {
            n = nums[i] % nums.length;
            if (n < 0)
                n += nums.length;
            numAn[n]++;
            if (numAn[n] > 1) {
                for (int j = 0; j < i; j++) {
                    if (nums[i] == nums[j])
                        return true;
                }
            }
        }
        return false;

    }
}
