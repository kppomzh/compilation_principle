package com.zhzm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.IntStream;

public class class_3211 {
    @Test
    public void test() {
//        System.out.println(validStrings(3));
//        System.out.println(validStrings(4));
        System.out.println(validStrings(5));
    }

    private static final String[] single0 = {"1"};
    private static final String[] single1 = {"1", "0"};
    private static final String[] multi0 = {"10", "11"};
    private static final String[] multi1 = {"10", "11", "01"};

    public List<String> validStrings(int n) {
        char[] preStr = new char[n];
        List<String> res = new ArrayList<>();

        preStr[0] = '1';
        dfs(res, preStr, 1);
        preStr[0] = '0';
        dfs(res, preStr, 1);

        return res;
    }

    private void dfs(List<String> res, char[] preStr, int idx) {
        if(idx == preStr.length) {
            res.add(new String(preStr));
            return;
        }
        if(preStr[idx-1] == '1') {
            preStr[idx] = '0';
            dfs(res, preStr, idx+1);
        }
        preStr[idx] = '1';
        dfs(res, preStr, idx+1);
    }

    public List<String> validStrings1(int n) {
        if(n == 1)
            return Arrays.stream(single1).toList();
        else if(n == 2)
            return Arrays.stream(multi1).toList();
        else {
            List<String> res = new ArrayList<>();
            Stack<String> stack = new Stack<>();
            for (String nextS : multi1) {
                stack.push(nextS);
                dfs1(res, stack, n - 2);
                stack.pop();
            }
            return res;
        }
    }

    private void dfs1(List<String> res, Stack<String> stack, int length) {
        if(length == 1) {
            res.addAll(build(stack, stack.peek().charAt(1)=='0'?single0:single1));
        } else if(length == 2) {
            res.addAll(build(stack, stack.peek().charAt(1)=='0'?multi0:multi1));
        } else {
            String[] nextStr = stack.peek().charAt(1)=='0'?multi0:multi1;
            for (String nextS : nextStr) {
                stack.push(nextS);
                dfs1(res, stack, length - 2);
                stack.pop();
            }
        }
    }

    public List<String> build(Stack<String> stack, String[] last) {
        return IntStream.range(0, last.length).boxed().parallel().map(i -> last[i]).map(s -> {
            StringBuilder sb = new StringBuilder();
            stack.forEach(sb::append);
            sb.append(s);
            return sb.toString();
        }).toList();
    }
}
