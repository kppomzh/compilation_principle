package com.zhzm;

import java.util.LinkedList;
import java.util.Queue;

public class class_225 {
}
class MyStack {
    Queue<Integer> q1,q2;
    Integer top;

    public MyStack() {
        q1=new LinkedList<>();
        q2=new LinkedList<>();
        top=null;
    }

    public void push(int x) {
        top=x;
        q2.add(x);
        q2.addAll(q1);
        Queue<Integer> tmp=q1;
        q1=q2;
        q2=tmp;
        q2.clear();
    }

    public int pop() {
        int res=q1.poll();
        top=q1.peek();
        return res;
    }

    public int top() {
        return top;
    }

    public boolean empty() {
        return q1.isEmpty();
    }
}
