package com.zhzm;

public class class_2325 {
    public static void main(String[] args) {
        class_2325 class2325=new class_2325();
        System.out.println(class2325.decodeMessage("vbxcnmuzijfdkdladhgpqiopoqieowpjmzlkmlzijgiutyhsanncxvnmbuiuiuweyruiewgtzlpaweptuoe","huvcixhzuirhiduzvhfd"));
    }
    public String decodeMessage(String key, String message) {
        char[] map=new char[26];
        boolean[] has=new boolean[26];
        int mapCount=26;

        int idx=0,insertIdx=0;
        while(mapCount>0){
            char c=key.charAt(idx);
            int hasIdx=c-97;
            if(c>=97 && c<=122&&!has[hasIdx]){
                has[hasIdx]=true;
                map[hasIdx]=(char)(insertIdx+97);
                insertIdx++;
                mapCount--;
            }
            idx++;
        }

        char[] res=new char[message.length()];
        for(int i=0;i<message.length();i++){
            char c=message.charAt(i);
            if(c>=97 && c<=122){
                res[i]=map[c-97];
            } else {
                res[i]=c;
            }
        }
        return new String(res);
    }
}
