package com.zhzm;

import org.junit.Test;

public class class_275 {

    @Test
    public void test1 () {
        System.out.println(hIndex(new int[]{100}));
        System.out.println(hIndex(new int[]{100,100}));
        System.out.println(hIndex(new int[]{1,1,1,1,1,1,100}));
    }

    public int hIndex(int[] citations) {
        int left = 0;
        int right = citations.length - 1;
        int middle = (left + right) / 2;
        int pageNum = citations.length - middle; // 论文数量
        int hNum = 0;

        while(middle > left && middle < right) {
            hNum = citations[middle];
            if(pageNum > hNum)
                left = middle;
            else if(pageNum == hNum)
                break;
            else
                right = middle;
            middle = (left + right) / 2;
            pageNum = citations.length - middle;
        }
        return hNum == 0 ? pageNum : hNum;
    }
}
