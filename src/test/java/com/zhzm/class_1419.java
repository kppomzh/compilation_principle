package com.zhzm;

import java.util.HashMap;
import java.util.Map;

public class class_1419 {
    public static void main(String[] args) {
        class_1419 class1419 = new class_1419();
        System.out.println(class1419.minNumberOfFrogs("croakcroakcroakcroak"));
        System.out.println(class1419.minNumberOfFrogs("crcoakroak"));
        System.out.println(class1419.minNumberOfFrogs("croakcrook"));
        System.out.println(class1419.minNumberOfFrogs("croakcrcocarkoraokakcroakcroak"));
    }

    public int minNumberOfFrogs(String croakOfFrogs) {
        int res = 0, inMid = 0;
        int[] stateCount = new int[4];
        for (int i = 0; i < croakOfFrogs.length(); i++) {
            char c = croakOfFrogs.charAt(i);
            switch (c) {
                case 'c' -> {
                    stateCount[0]++;
                    inMid++;
                    if (inMid >= res) {
                        res = inMid;
                    }
                }
                case 'r' -> {
                    if (stateCount[0] > 0) {
                        stateCount[0]--;
                        stateCount[1]++;
                    } else {
                        return -1;
                    }
                }
                case 'o' -> {
                    if (stateCount[1] > 0) {
                        stateCount[1]--;
                        stateCount[2]++;
                    } else {
                        return -1;
                    }
                }
                case 'a' -> {
                    if (stateCount[2] > 0) {
                        stateCount[2]--;
                        stateCount[3]++;
                    } else {
                        return -1;
                    }
                }
                case 'k' -> {
                    if (stateCount[3] > 0) {
                        stateCount[3]--;
                        inMid--;
                    } else {
                        return -1;
                    }
                }
                default -> {
                    ;
                }
            }
        }

        return res == 0 || inMid > 0 ? -1 : res;
    }
}
