package com.zhzm;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class class_352 {


    @Test
    public void test() {
        SummaryRanges summaryRanges = new SummaryRanges();
        summaryRanges.addNum(4);
        summaryRanges.addNum(6);
        summaryRanges.addNum(8);
        summaryRanges.addNum(7);
        summaryRanges.addNum(7);
        summaryRanges.addNum(5);
        System.out.println(Arrays.deepToString(summaryRanges.getIntervals()));
    }

    class SummaryRanges {
        private Set<Integer> leftSite, rightSite, added;

        public SummaryRanges() {
            leftSite = new HashSet<>();
            rightSite = new HashSet<>();
            added = new HashSet<>();
        }

        public void addNum(int value) {
            if (added.contains(value))
                return;
            added.add(value);
            boolean joinLeft = leftSite.contains(value + 1);
            boolean joinRight = rightSite.contains(value - 1);
            if (joinLeft && joinRight) {
                leftSite.remove(value + 1);
                rightSite.remove(value - 1);
            } else if (joinLeft) {
                leftSite.remove(value + 1);
                leftSite.add(value);
            } else if (joinRight) {
                rightSite.remove(value - 1);
                rightSite.add(value);
            } else {
                leftSite.add(value);
                rightSite.add(value);
            }
        }

        public int[][] getIntervals() {
            int[][] res = new int[leftSite.size()][2];
            Integer[] leftArray = leftSite.toArray(new Integer[0]);
            Integer[] rightArray = rightSite.toArray(new Integer[0]);
            Arrays.sort(leftArray);
            Arrays.sort(rightArray);
            for (int i = 0; i < leftArray.length; i++) {
                res[i][0] = leftArray[i];
                res[i][1] = rightArray[i];
            }
            return res;
        }
    }
}
