package com.zhzm;

public class class_463 {
    public int islandPerimeter(int[][] grid) {
        int height = grid.length;
        int width = grid[0].length;
        int[][] kernel = new int[][]{{0,1,0},{1,0,1},{0,1,0}};
        int[][] upperBoard = new int[height+2][width+2];
        for (int i = 0; i < height; i++) {
            System.arraycopy(grid[i], 0, upperBoard[i+1], 1, width);
        }
        int res = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(grid[i][j] == 1)
                    res += 4 - countKernel(upperBoard, kernel, i, j);
            }
        }
        return res;
    }

    private int countKernel(int[][] upperBoard, int[][] kernel, int height, int width) {
        int count = 0;
        for (int i = 0; i < kernel.length; i++) {
            for (int j = 0; j < kernel.length; j++) {
                count += upperBoard[height + i][width + j] * kernel[i][j];
            }
        }
        return count;
    }
}
