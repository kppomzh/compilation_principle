package com.zhzm;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class class_645 {
    public int[] findErrorNums1(int[] nums) {
        int res = -1;
        Set<Integer> fill = new HashSet<>();
        Set<Integer> exists = new HashSet<>();
        for(int i = 0; i < nums.length; i++) {
            fill.add(i+1);
            if (!exists.contains(nums[i])) {
                exists.add(nums[i]);
            } else {
                res = nums[i];
            }
        }
        fill.removeAll(exists);
        return new int[]{res, fill.stream().findFirst().orElse(-1)};
    }

    public int[] findErrorNums(int[] nums) {
        int[] map = new int[nums.length + 1];
        for(int i = 0; i < nums.length; i++) {
            map[nums[i]]++;
        }
        int[] res = new int[2];
        for(int i = 1; i < map.length; i++) {
            if(map[i] == 1)
                continue;
            else if(map[i] == 2)
                res[0] = i;
            else
                res[1] = i;
        }
        return res;
    }
    @Test
    public void test() {
        int[] nums = {1,2,2,4};
        int[] res = findErrorNumsBit(nums);
        System.out.println(res[0] + " " + res[1]);
    }

    public int[] findErrorNumsBit(int[] nums) {
        int xor = 0;
        for(int i = 0; i < nums.length; i++) {
            xor^=nums[i];
            xor^=i;
        }
        int low = xor & (-xor);
        int[] res = new int[]{xor, low};
        return res;
    }
}
