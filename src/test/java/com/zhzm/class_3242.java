package com.zhzm;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class class_3242 {
    @Test
    public void test() {
        int[][] grid = new int[][]{{0,1,2},{3,4,5},{6,7,8}};
        NeighborSum ns = new NeighborSum(grid);
        System.out.println(ns.adjacentSum(8));
        System.out.println(ns.diagonalSum(8));
    }
    @Test
    public void test1() {
        int[][] grid = new int[][]{{1,2,0,3},{4,7,15,6},{8,9,10,11},{12,13,14,5}};
        NeighborSum ns = new NeighborSum(grid);
        System.out.println(ns.adjacentSum(15));
        System.out.println(ns.diagonalSum(9));
    }

}

class NeighborSum {
    private static final int[][] adjacentKernel = new int[][]{{0,1,0},{1,0,1},{0,1,0}};
    private static final int[][] diagonalKernel = new int[][]{{1,0,1},{0,0,0},{1,0,1}};

    int[][] board;
    int width;
    int height;
    Map<Integer, Integer> contIdx;

    public NeighborSum(int[][] grid) {
        height = grid.length;
        width = grid[0].length;
        contIdx = new HashMap<>();
        this.board = new int[height+2][width+2];
        for (int i = 0; i < height; i++) {
            System.arraycopy(grid[i], 0, board[i+1], 1, width);
            for (int j = 0; j < width; j++) {
                contIdx.put(grid[i][j], i*width+j);
            }
        }
    }

    public int adjacentSum(int value) {
        int[] idx = getIdx(value);
        return countKernel(board, adjacentKernel, idx[0], idx[1]);
    }

    public int diagonalSum(int value) {
        int[] idx = getIdx(value);
        return countKernel(board, diagonalKernel, idx[0], idx[1]);
    }

    private int[] getIdx(int value) {
        int idx = contIdx.get(value);
        int list = idx % width;
        int line = idx / width;
        return new int[]{line, list};
    }

    private int countKernel(int[][] upperBoard, int[][] kernel, int height, int width) {
        int count = 0;
        for (int i = 0; i < kernel.length; i++) {
            for (int j = 0; j < kernel.length; j++) {
                count += upperBoard[height + i][width + j] * kernel[i][j];
            }
        }
        return count;
    }
}
