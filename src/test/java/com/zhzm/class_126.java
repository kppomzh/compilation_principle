package com.zhzm;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// 126. 单词接龙Ⅱ
public class class_126 {

    @Test
    public void test1() {
        String beginWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");
        List<List<String>> res = findLadders(beginWord, endWord, wordList);
        System.out.println(res);
    }

    @Test
    public void test2() {
        String beginWord = "a";
        String endWord = "c";
        List<String> wordList = Arrays.asList("b", beginWord, endWord);
        List<List<String>> res = findLadders(beginWord, endWord, wordList);
        System.out.println(res);
    }

    @Test
    public void test3() {
        String beginWord = "red";
        String endWord = "tax";
        List<String> wordList = Arrays.asList("ted", "tex", "red", "tax", "tad", "den", "rex", "pee");
        List<List<String>> res = findLadders(beginWord, endWord, wordList);
        System.out.println(res);
    }

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        Set<String> targets = new HashSet<>(wordList);
        targets.add(beginWord);
        Map<String, Set<String>> levelMap;
        if (!targets.contains(endWord))
            return Collections.emptyList();
        else
            levelMap = targets.parallelStream().collect(Collectors.toMap(String::valueOf, k->Collections.synchronizedSet(new HashSet<>())));

        Set<String> beforeSet = Set.of(beginWord);
        while (!(beforeSet.isEmpty() || beforeSet.contains(endWord))) {
            targets.removeAll(beforeSet);
            beforeSet = beforeSet.parallelStream().map(source -> strCharDiffMap(source, targets)).
                    peek(nMap -> nMap.forEach((key, value) -> levelMap.get(key).add(value))).
                    flatMap(nMap -> nMap.keySet().stream()).collect(Collectors.toSet());
        }

        if (!beforeSet.contains(endWord))
            return Collections.emptyList();
        List<List<String>> res = new ArrayList<>();
        dfs(Set.of(endWord), new Stack<>(), levelMap, res);
        return res;
    }

    private void dfs(Set<String> afterNext, Stack<String> temp, Map<String, Set<String>> levelMap, List<List<String>> res) {
        if (afterNext.isEmpty()) {
            res.add(List.copyOf(temp.reversed()));
        } else {
            afterNext.forEach(ks -> {
                temp.push(ks);
                dfs(levelMap.get(ks), temp, levelMap, res);
                temp.pop();
            });
        }
    }


    public List<List<String>> findLadders1(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordSet = new HashSet<>(wordList);
        if (!wordSet.contains(endWord))
            return Collections.emptyList();

        Map<String, List<List<String>>> beginMap = new HashMap<>();
        Map<String, List<List<String>>> endMap = new HashMap<>();
        List<String> linkColl = Collections.emptyList();

        beginMap.put(beginWord, List.of(List.of(beginWord)));
        endMap.put(endWord, List.of(List.of(endWord)));
        while (!beginMap.isEmpty()) {
            Set<String> bbset = beginMap.keySet();
            Set<String> beset = endMap.keySet();

            wordSet.removeAll(beginMap.keySet());
            beginMap = buildNextWord(wordSet, beginMap);
            if (!(linkColl = beginMap.keySet().stream().filter(beset::contains).toList()).isEmpty()) {
                break;
            }
            wordSet.removeAll(endMap.keySet());
            endMap = buildNextWord(wordSet, endMap);
            if (!(linkColl = endMap.keySet().stream().filter(bbset::contains).toList()).isEmpty()) {
                break;
            }
            if (!(linkColl = endMap.keySet().stream().filter(beginMap.keySet()::contains).toList()).isEmpty()) {
                break;
            }
        }
        final Map<String, List<List<String>>> finalBeginMap = beginMap;
        final Map<String, List<List<String>>> finalEndMap = endMap;
        return linkColl.parallelStream().flatMap(ls ->
                finalBeginMap.get(ls).parallelStream().peek(List::removeLast).
                        flatMap(blt -> finalEndMap.get(ls).stream().
                                map(elt -> Stream.concat(blt.stream(), elt.reversed().stream()).toList()))).toList();
    }

    /**
     * 构建下一个单词的逻辑
     * 该方法用于根据当前单词和已知单词集合，构建下一个单词的可能性，并更新结束单词映射
     *
     * @param wordSet 所有可能的单词集合，用于查找下一个单词
     * @param map     维护每个单词可能的结束单词列表的映射
     */
    private Map<String, List<List<String>>> buildNextWord(Set<String> wordSet, Map<String, List<List<String>>> map) {
        Map<String, List<List<String>>> res = new HashMap<>();
        map.forEach((ks, lists) -> {
            Set<String> ksNext = strCharDiff(ks, wordSet);
            ksNext.forEach(ksn -> lists.stream().map(ArrayList::new).
                    peek(nList -> nList.add(ksn)).
                    forEach(nList -> res.computeIfAbsent(ksn, k -> new ArrayList<>()).add(nList)));
        });
        return res;
    }

    /**
     * 该方法用于找出与给定字符串恰好有一个字符不同的目标字符串集合中的字符串
     *
     * @param source  给定的源字符串
     * @param targets 目标字符串集合，从中筛选出与源字符串满足条件的字符串
     * @return 返回一个包含与源字符串恰好有一个字符不同的目标字符串集合
     */
    private Set<String> strCharDiff(String source, Set<String> targets) {
        return targets.parallelStream().filter(target ->
                IntStream.range(0, source.length()).filter(i -> source.charAt(i) != target.charAt(i)).count() == 1).
                collect(Collectors.toSet());
    }

    private Map<String, String> strCharDiffMap(String source, Set<String> targets) {
        return targets.stream().filter(target ->
                IntStream.range(0, source.length()).filter(i -> source.charAt(i) != target.charAt(i)).count() == 1).
                collect(Collectors.toMap(String::valueOf, s -> source));
    }
}

class WordMsg {
    int level = -1;
    Set<String> parents;
    String word;

    public static WordMsg get(String word) {
        WordMsg w = new WordMsg();
        w.word = word;
        w.parents = new HashSet<>();
        return w;
    }

    public void set(WordMsg w) {
        this.level = w.level + 1;
        this.parents.add(w.word);
    }
}
