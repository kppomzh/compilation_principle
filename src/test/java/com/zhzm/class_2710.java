package com.zhzm;

public class class_2710 {
    public String removeTrailingZeros(String num) {
        int idx = Integer.MAX_VALUE;
        for (int i = num.length() - 1; i >= 0; i--) {
            if(num.charAt(i) != 48) {
                idx = i + 1;
                break;
            }
        }
        return idx == num.length() ? num : num.substring(0, idx);
    }
}
