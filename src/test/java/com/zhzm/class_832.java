package com.zhzm;

import java.util.stream.IntStream;

public class class_832 {
    public int[][] flipAndInvertImage(int[][] image) {
        // 使用顺序流处理小规模任务
        IntStream stream = image.length > 64 ? IntStream.range(0, image.length).parallel()
                : IntStream.range(0, image.length);// 超过 100 行才并行

        stream.forEach(idx -> {
            int[] arr = image[idx];
            int left = 0, right = arr.length - 1;
            while (left <= right) {
                // 翻转并取反
                if (left == right) {
                    // 中间元素只取反
                    arr[left] = 1 ^ arr[left];
                } else {
                    int temp = arr[left];
                    arr[left] = 1 ^ arr[right];
                    arr[right] = 1 ^ temp;
                }
                left++;
                right--;
            }
        });

        return image;
    }
}
