package com.zhzm;

import org.junit.Test;

// Z字变换
public class class_6 {

    @Test
    public void test() {
        System.out.println(this.convert("387t43ynft45789wt5748w96t5w47ty54987ftny5f478tny547389tyn5478n9t45", 4));
    }

    public String convert(String s, int numRows) {
        // 特殊情况处理
        if (s.length() < numRows || numRows == 1) return s;

        int jump1 = (numRows - 1) << 1;
        int jump2 = 0;
        char[] resultChars = new char[s.length()];
        int currentCharIndex = 0;
        int currentRow = 0;
        boolean useJump1 = true;

        for (int loop = 0; loop < s.length(); loop++) {
            resultChars[loop] = s.charAt(currentCharIndex);

            currentCharIndex += useJump1 ? jump1 :jump2;
            useJump1 = jump1 != 0 && (jump2 == 0 || !useJump1);

            if (currentCharIndex >= s.length()) {
                currentRow++;
                currentCharIndex = currentRow;
                jump1 -= 2;
                jump2 += 2;
                useJump1 = jump1 != 0;
            }
        }
        return new String(resultChars);
    }

}