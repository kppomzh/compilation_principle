package com.zhzm;

import java.util.HashMap;
import java.util.Map;

public class class_1713 {
    public static void main(String[] args) {
        class_1713 class1713=new class_1713();
        System.out.println(class1713.minOperations(new int[]{6,4,8,1,3,2},new int[]{4,7,6,2,3,2,8,6,1}));
    }

    Map<Integer, tnode> entrance;
    public int minOperations(int[] target, int[] arr) {
        int res= target.length;
        entrance=new HashMap<>();

        tnode tmpnode=null;
        for (int i = 0; i < target.length; i++) {
            tnode made=new tnode(target[i]);
            made.before=tmpnode;
            tmpnode=made;
            entrance.put(target[i],made);
        }

        for (int i = 0; i < arr.length; i++) {
            if(entrance.containsKey(arr[i])){
                tmpnode=entrance.get(arr[i]);
                if(tmpnode.before==null||tmpnode.before.point==null){
                    tpoint p=new tpoint();
                    p.point=tmpnode;
                    tmpnode.point=p;
                } else {
                    tmpnode.point= tmpnode.before.point;
                    tmpnode.point.step++;
                }
            }
        }


        return res;
    }

    private tpoint copyMaxPoint(tnode node){
        tpoint res= node.point;
        if(res==null){
            res=new tpoint();
            res.step=0;
        }
        while(node.before!=null){
            node=node.before;

            if(node.point!=null){
                res=res.step<node.point.step? node.point : res;
            }
        }
        res.step++;
        return res;
    }
}

class tnode{
    tnode before=null;
    int is;
    tpoint point=null;

    tnode(int is){
        this.is=is;
    }
}

class tpoint{
    tnode point=null;
    int step;
}
