package com.zhzm;

import java.util.*;

// 先序遍历 中左右
public class class_144 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();
            TreeNode cursor = root;
            while (cursor != null) {
                res.add(cursor.val);
                if(cursor.right != null)
                    stack.push(cursor.right);
                if(cursor.left != null)
                    cursor = cursor.left;
                else if(!stack.isEmpty())
                    cursor = stack.pop();
                else
                    cursor = null;
            }
        }
        return res;
    }
}
