package com.zhzm;

import java.util.*;

public class class_1356 {
    public static void main(String[] args) {
        class_1356 class1356=new class_1356();
        class1356.sortByBits(new int[]{1024,512,256,128,64,32,16,8,4,2,1});
    }
    public int[] sortByBits(int[] arr) {
        int[] res=new int[arr.length];
        Map<Integer, PriorityQueue<Integer>> map=new HashMap<>();
        for(int num:arr){
            int size=getOneSize(num);
            if(!map.containsKey(size)){
                map.put(size,new PriorityQueue<>());
            }
            map.get(size).add(num);
        }

        Integer[] sortKey=map.keySet().toArray(new Integer[0]);
        Arrays.sort(sortKey);
        int idx=0;
        for(int key:sortKey){
            for(int value:map.get(key)){
                res[idx]=value;
                idx++;
            }
            PriorityQueue<Integer> tmp=map.get(key);
            while(!tmp.isEmpty()){
                res[idx]=tmp.poll();
                idx++;
            }
        }

        return res;
    }

    private int getOneSize(int num){
        if(num==0)
            return 0;
        else {
            int res=1;
            while(num>1){
                if(num%2==0)
                    num>>=1;
                else{
                    res++;
                    num--;
                }
            }
            return res;
        }
    }
}
