package com.zhzm;

import java.util.*;

public class class_49 {
    public static void main(String[] args) {
        class_49 class49=new class_49();
        class49.groupAnagrams(new String[]{"eat", "ate"});
    }
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<StringKey,List<String>> res=new HashMap<>();

        for(int i=0;i< strs.length;i++){
            StringKey sk=new StringKey(strs[i].toCharArray());
            res.putIfAbsent(sk,new ArrayList<>());
            res.get(sk).add(strs[i]);
        }
        return List.copyOf(res.values());
    }

    class StringKey{
        private char[] key;
        private int hashCode;
        public StringKey(char[] toSort){
            key=toSort;
            Arrays.sort(key);
            hashCode=0;
            for (int i = 0; i < key.length; i++) {
                hashCode+=key[i];
                hashCode%=1000000007;
            }
        }

        public int hashCode(){
            return hashCode;
        }

        public boolean equals(Object o){
            if(o==null)
                return false;
            else if(o instanceof StringKey){
                return Arrays.equals(key,((StringKey) o).key);
            } else
                return false;
        }
    }
}
