package com.zhzm;

import org.junit.Test;

public class class_33 {
    @Test
    public void test(){
//        System.out.println(c33.search(new int[]{4,5,7,8,1,2,3},1));
        System.out.println(search(new int[]{4,5,7,8,1,2,3},2));
//        System.out.println(c33.search(new int[]{1,3},0));
    }

    public int search(int[] nums, int target) {
        if(nums.length<5){
            for (int i = 0; i < nums.length; i++) {
                if(nums[i]==target){
                    return i;
                }
            }
            return -1;
        }

        if(target==nums[0])
            return 0;
        if(target==nums[nums.length-1])
            return nums.length-1;
        int left=0,right= nums.length-1;
        int index=-1;
        if(target>nums[0]){
            while(left<right){
                index= (left+right)/2==index?index+1:(left+right)/2;
                if(nums[index]<nums[0]||nums[index]>target) {
                    right=index-1;
                } else if(nums[index]<target) {
                    left=index+1;
                } else {
                    return index;
                }
            }
        } else if(target<nums[nums.length-1]){
            while(left<right){
                index= (left+right)/2==index?1:(left+right)/2;
                if(nums[index]>nums[nums.length-1]||nums[index]<target) {
                    left=index+1;
                } else if(nums[index]>target) {
                    right=index-1;
                } else {
                    return index;
                }
            }
        }
        if(left<nums.length)
            return target==nums[left]?left:-1;
        else
            return -1;
    }
}
