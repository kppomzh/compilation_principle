package com.zhzm;

import org.junit.Test;

public class class_661 {
    private static final int[][] kernel = new int[][]{{1,1,1}, {1,1,1}, {1,1,1}};

    @Test
    public void test() {
        int[][] img = new int[][]{{1,1,1},{1,0,1},{1,1,1}};
        imageSmoother(img);
    }


    public int[][] imageSmoother(int[][] img) {
        int height = img.length;
        int width = img[0].length;
        int[][] upperBoard = new int[height+2][width+2];
        for (int i = 0; i < height; i++) {
            System.arraycopy(img[i], 0, upperBoard[i+1], 1, width);
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int count = countKernel(upperBoard, i, j);
                img[i][j] = count / getDiv(i,j,height,width);
            }
        }
        return img;
    }

    private int countKernel(int[][] upperBoard, int height, int width) {
        int count = 0;
        for (int i = 0; i < kernel.length; i++) {
            for (int j = 0; j < kernel.length; j++) {
                count += upperBoard[height + i][width + j] * kernel[i][j];
            }
        }
        return count;
    }

    private int getDiv(int i, int j, int height, int width) {
        boolean up = i == 0;
        boolean down = i == height-1;
        boolean left = j == 0;
        boolean right = j == width-1;
        int res = 9;
        if(up && down) {
            res -= 6;
            if (left)
                res -= 1;
            if (right)
                res -= 1;
        }else if(up || down) {
            res-=3;
            if (left)
                res -= 2;
            if (right)
                res -= 2;
        }
        else {
            if (left)
                res -= 3;
            if (right)
                res -= 3;
        }
        return res;
    }
}
