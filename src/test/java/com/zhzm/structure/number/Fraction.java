package com.zhzm.structure.number;

public class Fraction {
    protected final int molecule;
    protected final int denominator;

    public Fraction(int molecule, int denominator) {
        this.denominator = denominator;
        this.molecule = molecule;
    }

    @Override
    public String toString() {
        return  molecule +"/"+ denominator;
    }
}
