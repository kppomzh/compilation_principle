package com.zhzm.structure.number;

import java.util.List;

public class FarleyFraction extends Fraction {
    private Fraction left;
    private Fraction right;

    public FarleyFraction(Fraction left, Fraction right) {
        super(left.molecule + right.molecule, left.denominator + right.denominator);
        this.left = left;
        this.right = right;
    }

    public FarleyFraction getLeftFarleySum(List<FarleyFraction> nextFloor, int maxDenominator) {
        if(left.denominator + this.denominator <= maxDenominator) {
            FarleyFraction next = new FarleyFraction(left, this);
            nextFloor.add(next);
            return next;
        } else return null;
    }

    public FarleyFraction getRightFarleySum(List<FarleyFraction> nextFloor, int maxDenominator) {
        if(right.denominator + this.denominator <= maxDenominator) {
            FarleyFraction next = new FarleyFraction(this, right);
            nextFloor.add(next);
            return next;
        } else return null;
    }
}
