package com.zhzm.structure.number;

public class IntegerPlus extends Number {
    private long inte;
    @Override
    public int intValue() {
        return (int) inte;
    }

    @Override
    public long longValue() {
        return inte;
    }

    @Override
    public float floatValue() {
        return (float) inte;
    }

    @Override
    public double doubleValue() {
        return (double) inte;
    }


}
