package com.zhzm.structure.geometry;

import com.zhzm.structure.object.MinHeap;

import java.util.Comparator;

public class KnnPoint extends Point implements Comparator<KnnPoint> {
    private final MinHeap<Line> heap;

    public KnnPoint(int x, int y, int size) {
        super(x, y);
        heap = new MinHeap<>(size);
    }

//    public boolean addKnn(KnnPoint p) {
//        return heap.add(p);
//    }

    @Override
    public int compare(KnnPoint o1, KnnPoint o2) {
        return 0;
    }
}
