package com.zhzm.structure.geometry;

// 垂直于平面坐标轴的线段
public abstract class Line implements Comparable<Line> {


    protected Point start;
    protected Point end;
    protected int xMax, xMin, yMax, yMin;

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
        this.xMax = Math.max(start.x, end.x);
        this.xMin = Math.min(start.x, end.x);
        this.yMax = Math.max(start.y, end.y);
        this.yMin = Math.min(start.y, end.y);
    }

    public Line(int x1, int y1, int x2, int y2) {
        this.xMax = Math.max(x1, x2);
        this.xMin = Math.min(x1, x2);
        this.yMax = Math.max(y1, y2);
        this.yMin = Math.min(y1, y2);
    }

    abstract boolean isOnLine(Point point);


    abstract boolean isOnLine(Line line);
    abstract Point getStartPoint();

    abstract Point getEndPoint() ;

    /**
     * 判断两条线段是否相交。
     *
     * @param line 待比较的线段对象。
     * @return 如果线段相交则返回true，否则返回false。
     */
    abstract boolean isIntersect(Line line);

    public double getLength() {
        return Math.abs(Math.sqrt(start.getEuclideanDistancePow(end)));
    }

    @Override
    public int compareTo(Line o) {
        return 0;
    }

    @Override
    public String toString() {
        return String.format("startPoint: %s,endPoint: %s", getStartPoint(), getEndPoint());
    }
}
