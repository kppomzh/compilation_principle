package com.zhzm.structure.geometry;

import java.util.Objects;

public class Point implements Comparable<Point> {
    // 原点
    public static final Point ORIGIN = new Point(0, 0);

    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    /**
     * 计算并返回两点之间的曼哈顿距离
     * 用于快速比较两点间距离关系
     *
     * @param point 第二个点的坐标数组
     * @return 返回两个点之间的曼哈顿距离。
     */
    public int getManhattanDistance(Point point) {
        return Math.abs(x - point.x) + Math.abs(y - point.y);
    }

    /**
     * 计算与另一个点的欧几里得距离。
     *
     * @param point 另一个点，用于计算与本点的距离。
     * @return 返回本点与参数点之间的欧几里得距离。
     */
    public int getEuclideanDistancePow(Point point) {
        return (x - point.x) * (x - point.x) + (y - point.y) * (y - point.y);
    }

    public int[] toArray() {
        return new int[]{x, y};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point point)) return false;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point = (" + x +
                ", " + y +
                ')';
    }
    @Override
    public int compareTo(Point o) {
        return Integer.compare(x * x + y * y, o.x * o.x + o.y * o.y);
    }
}
