package com.zhzm.structure.geometry;

import java.util.*;

/*
* 投影图形到x坐标轴
*/
public class XAxis implements Axis{
    int xMax = Integer.MIN_VALUE;
    int xMin = Integer.MAX_VALUE;
    // 投影点集合，当计算投影时只使用keySet，value值仅仅是为了方便取点而保存的y值
    // key:x, value:y
    Map<Integer, Integer> projectionPoints = new HashMap<>();


    @Override
    public void projectedLine(Line line) {
        xMax = Math.max(xMax, line.xMax);
        xMin = Math.min(xMin, line.xMin);
        int rightInnerY = -1;
        for (int i = line.xMin; i < line.xMax ; i++) {
            if (!projectionPoints.containsKey(i)) {
                continue;
            }
            rightInnerY = projectionPoints.remove(i);
        }
        projectionPoints.put(line.xMin, line.yMax);
        if(line.xMax == xMax || rightInnerY == -1)
            projectionPoints.put(line.xMax, 0);
        else{
            projectionPoints.put(line.xMax, rightInnerY);
        }
    }

    public List<List<Integer>> getLeftRiseTrailEdge() {
        List<List<Integer>> res = new ArrayList<>();
        if(xMax < xMin) {
            return res;
        }
        Integer[] keyArray = projectionPoints.keySet().toArray(new Integer[0]);
        Arrays.sort(keyArray);
        res.add(List.of(xMin, projectionPoints.get(xMin)));
        for (int j = 1; j < keyArray.length; j++) {
            int i = keyArray[j];
            if (projectionPoints.containsKey(i)) {
                if (Objects.equals(projectionPoints.get(i), projectionPoints.getOrDefault(keyArray[j - 1], -1)))
                    continue;
                res.add(List.of(i, projectionPoints.get(i)));
            }
        }
        return res;
    }

    public List<List<Integer>> getLeftRiseTrailEdge1() {
        List<List<Integer>> res = new ArrayList<>();
        if(xMax < xMin) {
            return res;
        }
        int lastHeight = projectionPoints.get(xMin);
        res.add(List.of(xMin, lastHeight));
        for (int i = xMin + 1; i <= xMax; i++) {
            int height = projectionPoints.getOrDefault(i, -1);
            if(height > lastHeight) {
                res.add(List.of(i, height));
            } else if(height < lastHeight) {
                res.add(List.of(i - 1, height));
            } else
                continue;
            lastHeight = height;
        }
        res.add(List.of(xMax, 0));
        return res;
    }

    public List<Point> getRisingEdgeTopPoints() {
        List<Point> res = new ArrayList<>();
        if(xMax < xMin) {
            return res;
        }
        int lastHeight = projectionPoints.get(xMin);
        res.add(new Point(xMin, lastHeight));
        for (int i = xMin + 1; i <= xMax; i++) {
            int height = projectionPoints.getOrDefault(i, 0);
            if(height > lastHeight) {
                res.add(new Point(i, lastHeight));
            }
            lastHeight = height;
        }
        return res;
    }

    @Override
    public void projectedRectangle(Rectangle rectangle) {

    }
}
