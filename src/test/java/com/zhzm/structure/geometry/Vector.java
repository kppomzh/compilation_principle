package com.zhzm.structure.geometry;

import java.util.Objects;

/*
 * 向量类
 */
public class Vector extends Line {
    public static final Vector zero = new Vector(Point.ORIGIN, Point.ORIGIN);

    protected int yDelta;
    protected int xDelta;

    /**
     * 构造一个向量对象。
     *
     * @param startX 起始点的 x 坐标
     * @param startY 起始点的 y 坐标
     * @param endX   终点的 x 坐标
     * @param endY   终点的 y 坐标
     * @throws IllegalArgumentException 如果起始点和终点的坐标相同，则抛出此异常。
     */
    private Vector(int startX, int startY, int endX, int endY) {
        this(new Point(startX, startY), new Point(endX, endY));
    }

    private Vector(Point start, Point end) {
        super(start, end);
        yDelta = end.y - start.y;
        xDelta = end.x - start.x;
    }

    public static Vector getVector(int startX, int startY, int endX, int endY) {
        if (startX == endX && startY == endY) {
            return zero;
        }
        return new Vector(startX, startY, endX, endY);
    }
    public static Vector getVector(Point start, Point end) {
        if (Objects.equals(start, end)) {
            return zero;
        }
        return new Vector(start, end);
    }

    public Point getStartPoint() {
        return start;
    }

    public Point getEndPoint() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector vector)) return false;
        return Objects.equals(start, vector.start) && Objects.equals(end, vector.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    /**
     * 计算向量 的长度的平方。
     * 该方法通过欧几里得空间中两点间距离的公式，计算出线段的长度的平方，而无需进行开方运算。
     * 适用于只需要比较线段长度关系，而不必实际获取长度值的场景，可以节省计算资源。
     *
     * @return 线段长度的平方，为非负整数。
     */
    public int getLengthSquare() {
        return xDelta * xDelta + yDelta * yDelta;
    }

    /**
     * 获取当前向量的反向向量。
     * 本方法通过交换起始点和结束点的坐标来实现向量的反向。
     *
     * @return Vector 返回一个新的向量，该向量为当前向量的反向。
     */
    public Vector getInverse() {
        return getVector(getEndPoint(), getStartPoint());
    }

    /**
     * 判断两个向量是否互为反向量。
     * 本方法检查传入的向量是否与其当前对象所表示的向量在起点和终点上完全对调，
     * 即如果两个向量的起点和终点相互对应，则视为互为反向量。
     *
     * @param vector 待比较的向量对象。
     * @return 如果传入的向量与当前向量互为反向量，则返回true；否则返回false。
     */
    public boolean isInverse(Vector vector) {
        return Objects.equals(start, vector.end) && Objects.equals(end, vector.start);
    }

    /**
     * 计算两个向量的和。
     * 本方法旨在通过考虑两种特定情况来简化向量加法的计算：
     * 1. 当第二个向量的起点与第一个向量的终点重合时，直接将第二个向量的终点作为结果的终点。
     * 2. 当第二个向量的起点与第一个向量的起点重合时，计算两个向量的终点差值，并以此作为结果的终点。
     * 在其他情况下，本方法不执行加法操作并返回null，表示无法进行加法计算。
     *
     * @param vector 第二个向量，用于与第一个向量相加。
     * @return 新的向量，表示两个向量的和。如果无法进行加法计算，则返回null。
     */
    public Vector plus(Vector vector) {
        if (Objects.equals(vector.start, start))
            return getVector(start.x, start.y, end.x + vector.xDelta, end.y + vector.yDelta);
        else if (Objects.equals(vector.start, end))
            return getVector(getStartPoint(), vector.getEndPoint());
        else
            return null;
    }

    /**
     * 计算两个向量的差向量。
     * 本函数尝试以两种方式计算差向量：当给定向量的起点与当前向量的终点重合，或者给定向量的终点与当前向量的起点重合时。
     * 如果没有重合点，则说明无法计算差向量，返回null。
     *
     * @param vector 输入的向量，用于计算与当前向量的差。
     * @return 返回计算得到的差向量，或者null（如果无法计算）。
     */
    public Vector minus(Vector vector) {
        if (Objects.equals(vector.start, start))
            return getVector(end, vector.end);
        else if (Objects.equals(vector.start, end))
            return getVector(start, vector.start);
        else
            return null;
    }

    public int getDotProduct(Vector vector) {
        return xDelta * vector.xDelta + yDelta * vector.yDelta;
    }

    public int getCrossProductInt(Vector vector) {
        return xDelta * vector.yDelta - vector.xDelta * yDelta;
    }

    /**
     * 计算当前向量与指定向量之间的夹角。
     * 使用点积和长度的平方来计算两个向量的夹角余弦值，进而得到夹角的弧度值。
     *
     * @param vector 指定的向量，用于计算与当前向量的夹角。
     * @return 返回两个向量之间的夹角，以弧度表示。
     */
    public double getAngle(Vector vector) {
        int dotProduct = getDotProduct(vector);
        double lengthProduct = Math.sqrt(getLengthSquare()) * Math.sqrt(vector.getLengthSquare());
        return Math.acos(dotProduct / lengthProduct);
    }

    @Override
    public boolean isOnLine(Point p) {
        if(p.x > xMax || p.x < xMin || p.y > yMax || p.y < yMin)
            return false;
        int tempXD = start.x - p.x;
        int tempYD = start.y - p.y;
        return tempXD * yDelta == xDelta * tempYD;
    }

    @Override
    boolean isOnLine(Line line) {
        return isOnLine(line.getStartPoint()) && isOnLine(line.getEndPoint());
    }

    /**
     * 判断两个线段是否相交。
     *
     * @param vector 表示另一个线段的向量对象。
     * @return 如果线段相交返回true，否则返回false。
     */
    public boolean isIntersect(Line vector) {

        if (xMax < vector.xMin || yMin < vector.yMin || vector.xMax < xMin || vector.yMax < yMin) {
            return false;
//        }
//        if ((((startX - vector.startX) * vector.yDelta - (startY - vector.startY) * vector.xDelta) *
//                ((endX - vector.startX) * vector.yDelta - (endY - vector.startY) * vector.xDelta)) > 0 ||
//                (((vector.startX - startX) * yDelta - xDelta * (vector.startY - startY)) *
//                        ((vector.endX - startX) * yDelta - xDelta * (vector.endY - startY))) > 0) {
//            return false;
        }
        return true;
    }
}
