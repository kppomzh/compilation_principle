package com.zhzm.structure.geometry;

public interface Axis {
    void projectedLine(Line line);

    void projectedRectangle(Rectangle rectangle);
}
