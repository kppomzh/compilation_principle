package com.zhzm.structure.geometry;

import java.util.Objects;

public class DoublePoint {
    // 原点
    public static final DoublePoint ORIGIN = new DoublePoint(0, 0);

    public final double x;
    public final double y;

    public DoublePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }


    /**
     * 计算并返回两点之间的曼哈顿距离
     * 用于快速比较两点间距离关系
     *
     * @param point 第二个点的坐标数组
     * @return 返回两个点之间的曼哈顿距离。
     */
    public double getManhattanDistance(DoublePoint point) {
        return Math.abs(x - point.x) + Math.abs(y - point.y);
    }

    /**
     * 计算与另一个点的欧几里得距离。
     *
     * @param point 另一个点，用于计算与本点的距离。
     * @return 返回本点与参数点之间的欧几里得距离。
     */
    public double getEuclideanDistance(DoublePoint point) {
        return Math.sqrt(Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2));
    }

    public static DoublePoint getMidPoint(DoublePoint point1, DoublePoint point2) {
        return new DoublePoint((point1.x + point2.x) / 2, (point1.y + point2.y) / 2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoublePoint point)) return false;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
