package com.zhzm.structure.geometry;

import java.util.List;

/*
* 矩形类，用于表示矩形。
* 矩形正交于坐标轴
*/
public class Rectangle {

    private final Point rightUp;
    private final Point leftDown;
    private final Point rightDown;
    private final Point leftUp;
    private final Vector left, right, top, bottom;

    public Rectangle(Point leftDown, Point rightUp) {
        this.rightUp = rightUp;
        this.leftDown = leftDown;
        this.rightDown = new Point(rightUp.x, leftDown.y);
        this.leftUp = new Point(leftDown.x, rightUp.y);
        left = Vector.getVector(leftDown, leftUp);
        right = Vector.getVector(leftUp, rightUp);
        top = Vector.getVector(rightUp, rightDown);
        bottom = Vector.getVector(rightDown, leftDown);
    }

    public int getLeftX() {
        return leftDown.x;
    }

    public int getRightX() {
        return rightUp.x;
    }

    public int getTopY() {
        return rightUp.y;
    }

    public int getBottomY() {
        return leftDown.y;
    }

    public int getSize() {
        return (rightUp.x - leftDown.x) * (rightUp.y - leftDown.y);
    }

    /**
     * 判断一个点是否为矩形的边缘点。
     * 通过比较点的x坐标和y坐标与矩形左下角和右上角的坐标，来确定点是否位于矩形的边缘。
     *
     * @param point 待判断的点，其坐标由x和y属性定义。
     * @return 如果点位于矩形的边缘，则返回true；否则返回false。
     */
    public boolean isEdgePoint(Point point) {
        return ((point.x == leftDown.x || point.x == rightUp.x) && point.y >= leftDown.y && point.y <= rightUp.y)||
                ((point.y == leftDown.y || point.y == rightUp.y) && point.x >= leftDown.x && point.x <= rightUp.x);
    }

    /**
     * 判断向量是否为边缘向量。
     * 边缘向量定义为：其起点和终点都是边缘点的向量。
     * 本函数通过检查向量的起点和终点是否均为边缘点来确定向量是否为边缘向量。
     *
     * @param vector 待检查的向量。
     * @return 如果向量的起点和终点都是边缘点，则返回true；否则返回false。
     */
    public boolean isEdgeVector(Vector vector) {
        return left.isOnLine(vector) || top.isOnLine(vector) || right.isOnLine(vector) || bottom.isOnLine(vector);
    }

    /**
     * 获取边界向量列表。
     * 从左下角开始顺时针遍历矩形四边
     */
    public List<Vector> getEdgeVectors() {
        return List.of(left, top, right, bottom);
    }

    // @Test
    public void testEdgePoint() {
        Rectangle r = new Rectangle(new Point(1,1), new Point(4,4));
        Point p1 = new Point(3,1);
        Point p2 = new Point(1,1);

        System.out.println(r.isEdgeVector(Vector.getVector(p1, p2)));
    }
}
