package com.zhzm.structure.geometry;

public interface Equation {
    default double getY(double x) {
        return getY((int)x);
    }

    int getY(int x);

    default double getX(double y) {
        return getX((int)y);
    }

    int getX(int y);
}
