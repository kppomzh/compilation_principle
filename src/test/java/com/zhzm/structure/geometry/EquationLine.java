package com.zhzm.structure.geometry;

/**
 * 该类用于表示和解决二元一次方程
 * y = ax + b
 */
public class EquationLine implements Equation {
    private final double a;
    private final double b;

    public EquationLine(Line line) {
        if (line.start.x == line.end.x) {
            throw new IllegalArgumentException("The two points are on the same line.");
        }
        a = ((double) line.start.y - line.end.y) / (line.start.x - line.end.x);
        b = line.end.y - a * line.end.x;
    }

    public EquationLine(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int getY(int x) {
        return (int) Math.round(getY((double) x));
    }

    @Override
    public double getY(double x) {
        return a * x + b;
    }

    @Override
    public double getX(double y) {
        return (y - b) / a;
    }

    @Override
    public int getX(int y) {
        return (int) Math.round(getX(((double) y)));
    }
}
