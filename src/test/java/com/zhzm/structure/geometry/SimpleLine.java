package com.zhzm.structure.geometry;

public class SimpleLine extends Line {
    public SimpleLine(Point start, Point end) {
        super(start, end);
    }

    public SimpleLine(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    public int offAxis() {
        if(xMax == xMin)
            return xMin;
        if(yMax == yMin)
            return yMin;
        return Math.min(xMin, yMin);
    }

    @Override
    boolean isOnLine(Point point) {
        return false;
    }

    @Override
    boolean isOnLine(Line line) {
        return false;
    }

    @Override
    Point getStartPoint() {
        return null;
    }

    @Override
    Point getEndPoint() {
        return null;
    }

    /**
     * 判断两条线段是否相交。
     *
     * @param line 待比较的线段对象。
     * @return 如果线段相交则返回true，否则返回false。
     */
    @Override
    boolean isIntersect(Line line) {
        return false;
    }
}
