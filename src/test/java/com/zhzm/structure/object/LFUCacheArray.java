package com.zhzm.structure.object;

public class LFUCacheArray {
    public static class Node {
        public int key, val, freq;
        public Node pre, next;

        public Node() {
        }

        public Node(int k, int v) {
            this.key = k;
            this.val = v;
            this.freq = 1;
        }
    }

    public int capacity, size, minFreq;
    Node[] map, freqmap;

    public LFUCacheArray(int capacity) {
        this.capacity = capacity;
        this.size = 0;
        this.minFreq = 1;
        this.map = new Node[100010];
        this.freqmap = new Node[20010];
    }

    public int get(int key) {
        Node node = map[key];
        if (node == null) {
            return -1;
        }
        //更新频次
        update(key);
        return node.val;
    }

    public void put(int key, int val) {
        Node node = map[key];
        if (node == null) {
            if (capacity == 0) {
                return;
            }
            // 过载则先删除
            if (++size > capacity) {
                //最小频次的node链表的头
                Node head = freqmap[minFreq];
                //循环双向链表，直接定位到链表尾部，即最久未被访问的节点
                Node del_node = head.pre;
                remove(del_node);
                //map移除
                map[del_node.key] = null;
                // 自己指向自己，且为最小的节点，则删除
                if (head == head.next) {
                    freqmap[minFreq] = null;
                }
                size--;
            }
            node = new Node(key, val);
            map[key] = node;
            //1频次的节点不存在
            if (freqmap[1] == null) {
                create(1);
            }
            add(freqmap[1], node);
            //记录minFreq
            minFreq = 1;
        } else {
            node.val = val;
            update(key);
        }
    }

    public void update(int key) {
        Node node = map[key];
        //当前的freqMap位置的链表删掉自己
        remove(node);
        int freq = node.freq;
        // 自己指向自己，表示只有一个节点
        if (freqmap[freq] == freqmap[freq].next) {
            // 删除自己
            freqmap[freq] = null;
            // 若需要删除的节点是最小点，则最小频率加1
            if (freq == minFreq) {
                ++minFreq;
            }
        }
        // node频率加加
        freq = ++node.freq;
        if (freqmap[freq] == null) {
            create(freq);
        }
        add(freqmap[freq], node);
    }

    public void remove(Node node) {
        node.pre.next = node.next;
        node.next.pre = node.pre;
    }

    //头插法添加进对应频次的链表
    public void add(Node head, Node node) {
        node.next = head.next;
        head.next.pre = node;
        head.next = node;
        node.pre = head;
    }

    //freqMap对应频次创建头节点并保存
    public void create(int freq) {
        Node head = new Node();
        head.pre = head;
        head.next = head;
        freqmap[freq] = head;
    }
}
