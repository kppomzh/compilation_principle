package com.zhzm.structure.object;

import java.util.HashMap;

//高频列表，会将频率最高的元素排列在前，如果两个元素频率相同，则按照统计顺序排列（即先达到这个频率的在前）
public class FreqMaxList {
    private HashMap<Integer,BidListNode> map;
    private final BidListNode headNode;
    private BidListNode tailNode;

    public FreqMaxList(){
        map=new HashMap<>();
        headNode=new BidListNode(Integer.MAX_VALUE);
        tailNode=headNode;
    }

    public void add(Integer num){
        if(map.containsKey(num)){
            BidListNode now=map.get(num);
            now.val++;
            BidListNode check=now.front;
            if(now.val > check.val) {
                check = check.front;
                while (now.val > check.val) {
                    check = check.front;
                }
                now.front.next = now.next;
                if(now==tailNode){
                    tailNode=now.front;
                } else {
                    now.next.front=now.front;
                }

                now.next = check.next;
                check.next.front = now;

                check.next = now;
                now.front = check;
            }
        } else {
            BidListNode temp=new BidListNode(1);
            temp.key=num;
            temp.front=tailNode;
            tailNode.next=temp;
            tailNode=temp;
            map.put(num,temp);
        }
    }

    public Object pollTopKey(){
        BidListNode topNode=headNode.next;

        headNode.next=topNode.next;
//        if()
//        headNode.next.front=headNode;

        return topNode.key;
    }
}
