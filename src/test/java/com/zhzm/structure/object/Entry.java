package com.zhzm.structure.object;

import java.util.Objects;

//key/value节点
public class Entry<K,V> {
    public K key;
    public V value;

    public Entry(K key, V value){
        this.key=key;
        this.value=value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entry<?, ?> entry)) return false;
        return Objects.equals(key, entry.key) && Objects.equals(value, entry.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}
