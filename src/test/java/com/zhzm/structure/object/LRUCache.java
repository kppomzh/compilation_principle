package com.zhzm.structure.object;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
    private Map<Integer,DLinkedNode> orderList;
    int maxSize;

    DLinkedNode firstNode,lastNode;

    public LRUCache(int capacity) {
        maxSize=capacity;
        orderList =new HashMap<>();
    }

    public int get(int key) {
        if(orderList.containsKey(key)){
            refreshOrder(key);
            return orderList.get(key).value;
        }
        return -1;
    }

    public void put(int key, int value) {
        if(orderList.containsKey(key)){
            refreshOrder(key);
            orderList.get(key).value=value;
        } else {
            if (orderList.size() == maxSize) {
                int firstKey=firstNode.key;
                firstNode=firstNode.next;
                orderList.remove(firstKey).remove();
            }
            DLinkedNode nlast=new DLinkedNode(key,value);
            orderList.put(key,nlast);
            nlast.addToLast(lastNode);
            lastNode=nlast;
            if(firstNode==null){
                firstNode=nlast;
            }
        }
    }

    private void refreshOrder(int key){
        DLinkedNode nlast=orderList.get(key);

        if(nlast!=lastNode){
            if(nlast==firstNode){
                firstNode=firstNode.next;
            }
            nlast.remove();
            nlast.addToLast(lastNode);
            lastNode=nlast;
        }
    }
}
