package com.zhzm.structure.object;

import java.util.Comparator;
import java.util.PriorityQueue;

//  最大堆
public class MaxHeap<T extends Comparable<T>> extends Heap<T> {

    public MaxHeap() {
        heap = new PriorityQueue<>(Comparator.reverseOrder());
    }

    public MaxHeap(int size) {
        heap = new PriorityQueue<>(size, Comparator.reverseOrder());
    }

    public MaxHeap(Comparator<T> comparator) {
        heap = new PriorityQueue<>(comparator.reversed());
    }

}
