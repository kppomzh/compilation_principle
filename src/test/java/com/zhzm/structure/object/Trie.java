package com.zhzm.structure.object;

// 字典树，支持a-zA-Z
public class Trie {
    Trie[] charMap = new Trie[52];
    boolean isEnd = false;
    String word;

    public void insert(String word) {
        Trie node = this;
        for (int idx = 0; idx < word.length(); idx++) {
            char c = word.charAt(idx);
            int mapIdx = c >= 97 ? c - 71 : c - 65;
            if (node.charMap[mapIdx] == null) {
                node.charMap[mapIdx] = new Trie();
            }
            node = node.charMap[mapIdx];
        }
        node.isEnd = true;
        node.word = word;
    }

    public boolean search(String word) {
        Trie node = this;
        for (int idx = 0; idx < word.length(); idx++) {
            char c = word.charAt(idx);
            int mapIdx = c >= 97 ? c - 71 : c - 65;
            if (node.charMap[mapIdx] == null) {
                return false;
            }
            node = node.charMap[mapIdx];
        }
        return node.isEnd;
    }

    public boolean startsWith(String word) {
        Trie node = this;
        for (int idx = 0; idx < word.length(); idx++) {
            char c = word.charAt(idx);
            int mapIdx = c >= 97 ? c - 71 : c - 65;
            if (node.charMap[mapIdx] == null) {
                return false;
            }
            node = node.charMap[mapIdx];
        }
        return true;
    }

    public boolean searchMagic(int idx, String word) {
        if(idx == word.length()) {
            return isEnd && !this.word.equals(word);
        }
        char c = word.charAt(idx);
        int mapIdx = c >= 97 ? c - 71 : c - 65;
        boolean res = false;
        if (charMap[mapIdx] != null) {
            res = charMap[mapIdx].searchMagic(idx + 1, word);
        }
        for (int i = 0; !res && i < charMap.length; i++) {
            if (i == mapIdx) {
                continue;
            }
            String subStr = word.substring(idx + 1);
            if (charMap[i] != null) {
                res = charMap[i].search(subStr);
            }
        }
        return res;
    }
}
