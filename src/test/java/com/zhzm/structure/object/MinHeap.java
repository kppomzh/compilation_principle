package com.zhzm.structure.object;

import java.util.Comparator;
import java.util.PriorityQueue;

//  最小堆
public class MinHeap<T extends Comparable<T>> extends Heap<T> {

    public MinHeap() {
        heap = new PriorityQueue<>();
    }

    public MinHeap(int size) {
        heap = new PriorityQueue<>(size);
    }

    public MinHeap(Comparator<T> comparator) {
        heap = new PriorityQueue<>(comparator);
    }

}
