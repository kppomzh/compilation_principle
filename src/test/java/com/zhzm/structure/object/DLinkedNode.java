package com.zhzm.structure.object;

//双向链表节点
public class DLinkedNode {
    public int key,value;
    public DLinkedNode prev,next;

    public DLinkedNode(){}

    public DLinkedNode(int key,int value){
        this.key = key;
        this.value = value;
    }

    public DLinkedNode addToLast(DLinkedNode last){
        if(last!=null)
            last.next=this;
        this.prev=last;

        return this;
    }

    public DLinkedNode remove(){
        if(this.prev!=null){
            this.prev.next=this.next;
        }
        if(this.next!=null){
            this.next.prev=this.prev;
        }

        this.prev=null;
        this.next=null;
        return this;
    }
}
