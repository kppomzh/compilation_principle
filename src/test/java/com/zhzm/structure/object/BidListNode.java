package com.zhzm.structure.object;

public class BidListNode {
    public int val;
    public BidListNode next;
    public BidListNode front;
    public Integer key;

    public BidListNode() {
    }

    public BidListNode(int val) {
        this.val = val;
    }

    public BidListNode(int val, BidListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return String.valueOf(val);
//        return val + ((next==null)?"": ","+next);
    }
}
