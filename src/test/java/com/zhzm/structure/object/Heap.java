package com.zhzm.structure.object;

import java.util.Collection;
import java.util.PriorityQueue;

public abstract class Heap<T extends Comparable<T>> {

    protected PriorityQueue<T> heap;
    public boolean add(T t) {
        return heap.add(t);
    }

    public boolean hasNext() {
        return !heap.isEmpty();
    }

    public boolean isEmpty() {
        return heap.isEmpty();
    }

    public int size() {
        return heap.size();
    }

    public T getTop() {
        return heap.peek();
    }

    public T removeTop() {
        return heap.poll();
    }

    public void addAll(Collection<T> coll) {
        heap.addAll(coll);
    }
    public void addAll(Heap<T> maxHeap) {
        heap.addAll(maxHeap.heap);
    }
}
