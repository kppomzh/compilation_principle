package com.zhzm.structure.object;

import java.util.ArrayList;

//定长列表
public class FixLengthQuere extends ArrayList<Integer> {
    private int fixLength,oLength;
    public FixLengthQuere(int length){
        super(length+1);
        fixLength=length;
    }

    public int added(Integer e){
        int res=0;
        if(size()+1>fixLength){
            res=remove(0);
            oLength-=res;
        }
        oLength+=e;
        super.add(e);
        return res;
    }

    public int getoLength(){
        return oLength;
    }

    public boolean isFull(){
        return size()==fixLength;
    }
}
