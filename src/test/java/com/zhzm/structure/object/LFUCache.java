package com.zhzm.structure.object;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class LFUCache {
    private HashMap<Integer,Integer> valueMap,freq;
    private HashMap<Integer, LinkedHashSet<Integer>> freqToKeys;
    private int minFreq=0,maxFreq=0;
    // 记录 LFU 缓存的最大容量
    private int cap;

    public LFUCache(int capacity) {
        valueMap =new HashMap<>();
        freq=new HashMap<>();
        freqToKeys=new HashMap<>();
        cap=capacity;
    }

    public int get(int key) {
        if(freq.containsKey(key)){
            increaseFreq(key);
            return valueMap.get(key);
        }
        return -1;
    }

    public void put(int key, int value) {
        if(cap>0){
            valueMap.put(key, value);
            if(freq.containsKey(key)){
                increaseFreq(key);
            } else {
                removeUseLessKey();
                freq.put(key, 1);

                freqToKeys.putIfAbsent(1, new LinkedHashSet<>());
                freqToKeys.get(1).add(key);
                this.minFreq = 1;
            }
        }

    }

    private void removeUseLessKey(){
        if(freq.size()+1>cap){
            int toRemove=freqToKeys.get(minFreq).iterator().next();
            freqToKeys.get(minFreq).remove(toRemove);
            freq.remove(toRemove);
        }
    }

    private void increaseFreq(int key) {
        int oldFreq = freq.get(key);
        freq.put(key,oldFreq+1);
        freqToKeys.get(oldFreq).remove(key);
        if(!freqToKeys.containsKey(oldFreq + 1))
            freqToKeys.put(oldFreq + 1, new LinkedHashSet<>());
        freqToKeys.get(oldFreq+1).add(key);

        if(minFreq==oldFreq && freqToKeys.get(minFreq).isEmpty()){
            minFreq++;
        } else if(oldFreq==maxFreq){
            maxFreq++;
        }
    }

    public int getFreqDiff(){
        return maxFreq-minFreq;
    }
}
