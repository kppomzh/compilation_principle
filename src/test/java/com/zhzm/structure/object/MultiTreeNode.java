package com.zhzm.structure.object;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
* 双向多叉树
*/
public class MultiTreeNode {
    private int idx;
    private int val;

    private final List<MultiTreeNode> child;

    private MultiTreeNode parent;

    public MultiTreeNode() {
        parent = null;
        child = new ArrayList<>();
    }
    public MultiTreeNode(int idx, int val) {
        this();
        this.idx = idx;
        this.val = val;
    }

    public MultiTreeNode(int val, MultiTreeNode parent) {
        child = new ArrayList<>();
        this.val = val;
        this.parent = parent;
    }

    public List<MultiTreeNode> getChild() {
        return child;
    }

    public boolean addChild(MultiTreeNode node) {
        return child.add(node);
    }

    public MultiTreeNode getParent() {
        return parent;
    }

    public void setParent(MultiTreeNode parent) {
        this.parent = parent;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MultiTreeNode that)) return false;
        return idx == that.idx && val == that.val && Objects.equals(child, that.child) && Objects.equals(parent, that.parent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx, val, child, parent);
    }
}
