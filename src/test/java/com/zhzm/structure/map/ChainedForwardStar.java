package com.zhzm.structure.map;

import org.junit.Test;

import java.util.*;

// 链式前向星
public class ChainedForwardStar {
    int n; // 顶点数量
    int m; // 边数量
    int[] head; // 每个顶点的第一条出边索引
    int[] edge; // 边的目标节点
    int[] next; // 下一条边的索引
    int cnt; // 当前边的数量

    public ChainedForwardStar(int n) {
        this.n = n;
        this.m = 0;
        this.head = new int[n];
        Arrays.fill(head, -1); // 初始化为-1
        this.edge = new int[2 * n]; // 假设最多每个顶点有一条边到另一个顶点
        this.next = new int[2 * n];
        this.cnt = 0;
    }

    public void addEdge(int u, int v) {
        next[cnt] = head[u];
        edge[cnt] = v;
        head[u] = cnt;
        cnt++;
        m++;
    }

    public Set<Integer> getNextVal(Integer parent, int point) {
        Set<Integer> res = new HashSet<>();
        for (int j = head[point]; j != -1; j = next[j]) {
            res.add(edge[j]);
        }
        res.remove(parent);
        return res;
    }

    private void printAdjList() {
        for (int i = 0; i < n; i++) {
            System.out.print("Vertex " + i + ": ");
            for (int j = head[i]; j != -1; j = next[j]) {
                System.out.print(edge[j] + " ");
            }
            System.out.println();
        }
    }
}
