package com.zhzm;

public class class_304 {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}};
        NumMatrix numMatrix = new NumMatrix(matrix);
        System.out.println(numMatrix.sumRegion(2, 1, 4, 3));
        System.out.println(numMatrix.sumRegion(1, 1, 2, 2));
        System.out.println(numMatrix.sumRegion(1, 2, 2, 4));
    }
}

class NumMatrix {
    private int[][] matrixl;

    public NumMatrix(int[][] matrix) {
        int height = matrix.length;
        int width = matrix[0].length;
        matrixl = new int[height + 1][width + 1];
        for (int i = 1; i < height + 1; i++) {
            for (int j = 1; j < width + 1; j++) {
                matrixl[i][j] = matrix[i - 1][j - 1] + matrixl[i][j - 1] + matrixl[i - 1][j] - matrixl[i - 1][j - 1];
            }
        }
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        return matrixl[row2 + 1][col2 + 1] - matrixl[row2 + 1][col1] - matrixl[row1][col2 + 1] + matrixl[row1][col1];
    }
}