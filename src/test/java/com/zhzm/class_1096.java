package com.zhzm;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class class_1096 {
    public List<String> braceExpansionII(String expression) {
        Set<String> res=new HashSet<>();
        StringBuilder sb=new StringBuilder();

        for (int i = 0; i < expression.length(); i++) {
            if(expression.charAt(i)=='{'){
                Set<String> set=getBucketString(expression,i);

            } else if(expression.charAt(i)==',') {
                res.add(sb.toString());
                sb.delete(0,sb.length());
            } else {
                sb.append(expression.charAt(i));
            }
        }

        return List.copyOf(res);
    }

    private Set<String> getBucketString(String expression,int index){
        Set<String> res=new HashSet<>();
        StringBuilder sb=new StringBuilder();

        for (int i = index; i < expression.length(); i++) {
            if(expression.charAt(i)=='{'){
                Set<String> set=getBucketString(expression,i);

            } else if(expression.charAt(i)=='}'){
                res.add(sb.toString());
                break;
            } else if(expression.charAt(i)==',') {
                res.add(sb.toString());
                sb.delete(0,sb.length());
            } else {
                sb.append(expression.charAt(i));
            }
        }
        return res;
    }
}
