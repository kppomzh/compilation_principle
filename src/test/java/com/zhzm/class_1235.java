package com.zhzm;

import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

// 1235. 规划兼职工作
public class class_1235 {

    @Test
    public void test1235() {
        int[] start = {15,44,15,47,11,18,5,41,38,25,19,25};
        int[] end = {33,48,20,49,37,22,32,48,39,37,38,40};
        int[] profits = {18,19,16,1,5,12,17,7,19,9,18,9};
        System.out.println(this.jobScheduling(start, end, profits));
    }

    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        List<Integer> sortedIdx = sortedTime(startTime, endTime);
        List<Profit> profits = Profit.builder(startTime, endTime, profit, sortedIdx);
        int res = 0;
        int maxTime = Arrays.stream(endTime).max().orElseThrow();
        for (int i = 0; i < sortedIdx.size(); i++) {
            boolean[] times = new boolean[maxTime+1];
            int dynaRes = dynamicCount(i, profits, times);
            res = Math.max(res, dynaRes);
        }
        return res;
    }

    private int dynamicCount(int idx, List<Profit> profits, boolean[] times) {
        if(idx >= profits.size())
            return 0;
        int subStage = 0;
        if(checkTimeTrue(profits.get(idx), times)) {
            IntStream.range(profits.get(idx).startTime, profits.get(idx).endTime).forEach(j -> times[j] = true);
            subStage = Math.max(subStage, dynamicCount(idx+1, profits, Arrays.copyOf(times, times.length)));
            subStage += profits.get(idx).profit;
        } else {
            subStage = dynamicCount(idx+1, profits, Arrays.copyOf(times, times.length));
        }
        return subStage;
    }

    private boolean checkTimeTrue(Profit profit, boolean[] times) {
        boolean checkFalse = true;
        for(int i = profit.startTime; i < profit.endTime; i++) {
            if(times[i]) {
                checkFalse = false;
            }
        }
        return checkFalse;
    }

    private List<Integer> sortedTime(int[] start, int[] end) {
        List<Integer> res = new ArrayList<>();
        Set<Integer> exclude = new HashSet<>();
        for (int i = 0; i < start.length; i++) {
            int largeIdx = -1;
            int large = -1;
            for (int j = 0; j < start.length; j++) {
                if (!exclude.contains(j) && end[j] - start[j] > large) {
                    largeIdx = j;
                    large = end[j] - start[j];
                }
            }
            res.add(largeIdx);
            exclude.add(largeIdx);
        }
        return res;
    }

    private List<Integer> sortedProfit(int[] profit) {
        List<Integer> res = new ArrayList<>();
        Set<Integer> exclude = new HashSet<>();
        while (res.size() < profit.length) {
            int largeIdx = -1;
            int large = -1;
            for (int j = 0; j < profit.length; j++) {
                if (!exclude.contains(j) && profit[j] > large) {
                    largeIdx = j;
                    large = profit[j];
                }
            }
            res.add(largeIdx);
            exclude.add(largeIdx);
        }
        return res;
    }

    static class Profit {
        int profit;
        int startTime;
        int endTime;

        static List<Profit> builder(int[] startTime, int[] endTime, int[] profit, List<Integer> sortedIdx) {
            List<Profit> res = new ArrayList<>();
            for (Integer sort : sortedIdx) {
                Profit prf = new Profit();
                prf.profit = profit[sort];
                prf.startTime = startTime[sort];
                prf.endTime = endTime[sort];
                res.add(prf);
            }
            return res;
        }
    }
}
