package com.zhzm;

import org.junit.Test;

import java.util.List;
import java.util.Stack;

public class class_738 {
    @Test
    public void test() {
        System.out.println(monotoneIncreasingDigits(321809));
    }

    public int monotoneIncreasingDigits(int n) {
        int mod = 111111111;
        int res = 0;
        for (int i = 0; i < 9 && mod > 0; i++) {
            while(res + mod > n) {
                mod /= 10;
            }
            res += mod;
        }
        return res;
    }

    public int monotoneIncreasingDigits1(int n) {
        if(n == 0)
            return 0;
        int res = dig(n);
        int iter = dig(res);
        while(res != iter) {
            res = iter;
            iter = dig(res);
        }
        return res;
    }

    private int dig(int n) {
        Stack<Integer> nArray = new Stack<>();
        int t = n;
        while(t != 0) {
            nArray.push(t%10);
            t/=10;
        }
        int top = nArray.pop();
        int res = top;
        while (!nArray.isEmpty()) {
            int cont = nArray.pop();
            if(top > cont) {
                res *= Math.pow(10, nArray.size()+1);
                res--;
                break;
            } else {
                top = cont;
            }
            res *= 10;
            res += top;
        }
        return res;
    }
}
