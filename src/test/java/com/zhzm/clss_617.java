package com.zhzm;

import java.util.Stack;

public class clss_617 {
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if(root1 == null)
            return root2;
        else if(root2 == null)
            return root1;
        TreeNode nRoot = new TreeNode(root1.val + root2.val);
        TreeNode left = mergeTrees(root1.left, root2.left);
        TreeNode right = mergeTrees(root1.right, root2.right);
        nRoot.left = left;
        nRoot.right = right;
        return nRoot;
    }
}
