package com.id;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.checksum.CRC16;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.MD5;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

public enum IdEnum {
    CRC_32_ID {
        @Override
        protected void getId(byte[] bytes, Map<String, String> res) {
            res.put(this.name(), Long.toHexString(IoUtil.checksumCRC32(new ByteArrayInputStream(bytes))));
            if(this.getNext() != null)
                this.getNext().getId(bytes, res);
        }
    },
    CRC_16_ID {
        @Override
        protected void getId(byte[] bytes, Map<String, String> res) {
            CRC16 crc16 = new CRC16();
            crc16.update(bytes,0,bytes.length);
            res.put(this.name(), Long.toHexString(crc16.getValue()));
            if(this.getNext() != null)
                this.getNext().getId(bytes, res);
        }
    },
    SHA_1_ID {
        @Override
        protected void getId(byte[] bytes, Map<String, String> res) {
            res.put(this.name(), SecureUtil.sha1(new ByteArrayInputStream(bytes)));
            if(this.getNext() != null)
                this.getNext().getId(bytes, res);
        }
    },
    SHA_256_ID {
        @Override
        protected void getId(byte[] bytes, Map<String, String> res) {
            res.put(this.name(), SecureUtil.sha256(new ByteArrayInputStream(bytes)));
            if(this.getNext() != null)
                this.getNext().getId(bytes, res);
        }
    },
//    SHA_512_ID {
//        @Override
//        protected void getId(byte[] bytes, Map<String, String> res) {
//            return SecureUtil.sha(new ByteArrayInputStream(bytes));
//        }
//    },
    MD5_ID {
        @Override
        protected void getId(byte[] bytes, Map<String, String> res) {
            res.put(this.name(), MD5.create().digestHex16(bytes));
            if(this.getNext() != null)
                this.getNext().getId(bytes, res);
        }
    };

    private IdEnum next;
    private static final IdEnum head;

    static {
        IdEnum[] ids = IdEnum.values();
        head = ids[0];
        IdEnum curr = head;
        for (int i = 1; i < ids.length; i++) {
            curr.next = ids[i];
            curr = curr.next;
        }
    }

    protected IdEnum getNext() {
        return next;
    }

    public static Map<String, String> getHashIdMap(byte[] bytes) {
        Map<String, String> res = new HashMap<>();
        head.getId(bytes, res);
        return res;
    }

    protected abstract void getId(byte[] bytes, Map<String, String> res);
}
