# 最简分数

给你一个整数 `n` ，请你返回所有 0 到 1 之间（不包括 0 和 1）满足分母小于等于 `n` 的 **最简** 分数 。分数可以以 **任意** 顺序返回。

## 涉及的概念与定义

### 算术基本定理

每个大于1的整数都能分解成有限个质数的乘积，且分解方式唯一。

这条定理虽然证明有些难度，但是可以说得到这个结论是显然的。

### 欧几里得算法（辗转相除法）

递归实现

```java
public int gcd(int a, int b) { // a>b
    return b != 0 ? gcd(b, a % b) : a;
}
```

递推实现

```java
public int gcd(int a, int b) {
    int copyA = Math.max(a, b);
    int copyB = Math.min(a, b);
    while(copyB != 0) {
        int temp = copyA;
        copyA = copyB;
        copyB = temp % copyB;
    }
    return copyA;
}
```



### 裴蜀定理

考虑整数x，y以及他们的最大公约数d，有如下关系：

1. x，y的某个线性组合等于d，即必然存在a，b两个整数，使得$$ax+by=d$$
2. 对于任意整数m，n与x，y组成的线性组合，一定满足$$mx+ny$$是d的整数倍

裴蜀定理可以由辗转相除法的逆向过程得到。(注：一般用辗转相除法的时候我们会忽略掉商，只关注余数。但是逆向推导裴蜀定理的时候需要稍微关注一下商，不能完全无视)

### 福特圆

假设有两个最简分数$$\frac{a}{b}$$与$$\frac{c}{d}$$，a、b；c、d互质，让它们分别在$$p(\frac{a}{b},\frac{1}{2*b^{2}})$$ 和 $$q(\frac{c}{d},\frac{1}{2*d^{2}})$$ 两点做半径为坐标y值的圆。

那么两圆相切等价于圆心距等于半径和，可以推导出下面的等式：
$$
(\frac{a}{b} - \frac{c}{d})^{2} + (\frac{1}{2*b^{2}} - \frac{1}{2*d^{2}})^{2} = (\frac{1}{2*d^{2}} + \frac{1}{2*b^{2}})^{2}
$$

$$
(\frac{a}{b} - \frac{c}{d})^{2} = 4 * \frac{1}{2*d^{2}} * \frac{1}{2*b^{2}}
$$

$$
(ad-cb)^{2} = 1
$$

因此这两个圆相切时，可以得到a、b、c、d的关系，一系列满足该关系的圆的集合称为福特圆：
$$
\lvert{ad-cb}\rvert = 1
$$

### 法里中项（法里和）

对于任意两个能满足福特圆关系的最简分数$$\frac{a}{b}$$与$$\frac{c}{d}$$，我们可以进行一个特殊的分数加法操作。

一般做分数加法时都要先通分再相加，但是这一次我们放弃通分，直接用分子加分子、分母加分母来求值。

如此可以得到法里中项：$\frac{a+c}{b+d}$

我们需要验证如下四个问题：

1.  $$ \frac{a}{b} < \frac{a+c}{b+d} < \frac{c}{d}$$（这样才能证明这三个圆之间不重叠）
2.  $$\frac{a+c}{b+d}$$与另外两个最简分数的福特圆相切（该条是由两个问题合并起来的）
3.  $$\frac{a+c}{b+d}$$是最简分数，即a+c与b+d互质

第1个问题证明是最简单的，可以将分数转换到平面直角坐标系中，将分数$\frac{a}{b}$视为从原点到点(a,b)的向量，那么原分数就是该向量所在直线的斜率，根据向量加法的概念，显然有如下关系：

![](.\pic\向量证明法里中项性质1.png)

注意这里的向量OC与OB斜率分别是2/5与1/3，并且$2*3-1*5 = 1$

第2、3个问题，新的圆与两个旧圆相切，意味着$\lvert{a(b+d)-b(a+c)}\rvert = 1$ 或者 $\lvert{c(b+d)-d(a+c)}\rvert = 1$

化简两个式子可得：$\lvert{ad-bc}\rvert = 1$  或  $\lvert{cb-ad}\rvert = 1$ ，这在事实上与上节福特圆的性质相同，因此新的圆和两个旧圆分别相切。

第4个问题，由裴蜀定理，x、y两个整数互质的条件是$\exists \{a,b\} \in N,ax+by=1$，因此设m、n两数为整数，若a+c与b+d互质，则$m(a+c)+n(b+d)=1$。此时我们其实可以不用求解m、n的具体值，因为上面第2、3两问的解答已经给出了这个二元方程的两个解：$(m,n)=(-b,a)$或$(m,n)=(-d,c)$。如此四个问题均得以证明，也就论证了法里中项相加方式的可行性。

## 推导思路

事实上从上面的各个概念的介绍当中，对最简分数的所有需要证明需要推导的要素都已经被介绍完了，唯独缺少的就是初始条件，或者说第一推动力。

只需要从两个不算分数的分数开始，就能令人信服的推导出所有的最简分数，这两个分数是$\frac{0}{1}$、$\frac{1}{1}$。

根据法里中项的求和方式，对于任意的最简分数p/q，我们都可以通过二分查找来逼近；逆向考虑这个问题，对于任意的最简分数p/q，一定有一个更大的最简分数m/n和更小的最简分数p-m/q-n使其能够向上回溯，直到回溯到1/2。因此法里中项是能够覆盖所有最简分数的。

多说一点，对于限制分母大小的情形，都能用法里中项来找无理数的最佳有理逼近，对此有一些逼近定理可以证明，在此不详细描述。

以下是实现代码，截稿时，消耗时间仅有官方解法的3/5：

```java
    public List<String> simplifiedFractions(int n) {
        return fillFarleyList(new ArrayList<>(), 0, 1, 1, 1, n);
    }
    private List<String> fillFarleyList(List<String> fractionList, int lm, int ld, int rm, int rd, int level) {
        if(ld + rd > level) return fractionList;
        fractionList.add((lm + rm) + "/" + (ld + rd));
        fillFarleyList(fractionList, lm, ld, lm + rm, ld + rd, level);
        return fillFarleyList(fractionList, lm + rm, ld + rd, rm, rd, level);
    }
```

